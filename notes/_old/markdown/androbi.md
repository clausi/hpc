## androbi.com (course platform on AWS)

* Access: `ssh username@androbi.com`
* Users: alumNN , NN=01..20
* Usually just one "core", and 1G memory, so limited capacity
* During classes with more cores and memory
* gcc 5.4, gfortran, Ubuntu 16
* intel compilers available: 
  `source /opt/intel/bin/iccvars.sh -arch intel64 -platform linux`
` 
 
    * C: icc
    * C++: icpc
    * Fortan ifort
