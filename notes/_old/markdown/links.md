### Development Tools

* [markdown](http://daringfireball.net/projects/markdown/)
  [md files at bitbucket](https://bitbucket.org/tutorials/markdowndemo)
* [Intel Academico](https://software.intel.com/en-us/intel-education-offerings#pid-2460-93)
* [Intel® Math Kernel Library (Intel® MKL)](https://software.intel.com/en-us/intel-mkl)
* [MVAPICH](http://mvapich.cse.ohio-state.edu/)
* [OpenMP](https://www.lrz.de/services/software/parallel/openmp/)
  [OpenMP on NUMA](https://cterboven.files.wordpress.com/2012/09/openmp_on_ccnuma_architectures_-_2010.pdf)


### Courses

* [Intro Lawrence Livermore Lab](https://computing.llnl.gov/tutorials/parallel_comp/)
* [Designing and Builing Parallel Programs](http://www.mcs.anl.gov/~itf/dbpp/)


### Benchmarks

* [Stream - Memory Bandwidth](http://www.cs.virginia.edu/stream/)
* [Roy Longbottom's PC Benchmark Collection](http://www.roylongbottom.org.uk/)
* [Meassuring time on Intel](http://blog.regehr.org/archives/330)


### Optimization

* [Link collection AVX/SSE](http://www.walkingrandomly.com/?p=3378)

### Xeon Phi

* [Getting Started Guide](https://software.intel.com/sites/default/files/managed/26/d6/Intel_Xeon_Phi_Quick_Start_Developers_Guide-MPSS-3.4.pdf)


### Associations
* [Open fabrics](https://www.openfabrics.org/)
* [Infiniband roadmap](http://www.infinibandta.org/content/pages.php?pg=technology_overview)


### Hardware (Communications)

* [Infiniband performance Mellanox](http://www.mellanox.com/page/performance_infiniband)
* [Mellanox refurbished](http://www.mellanoxstore.com/categories/certified-refurbished.html)


### Programming

* [Arrays in C99](http://www.drdobbs.com/the-new-cwhy-variable-length-arrays/184401444)
* [Dr. Dobbs Programming Intel Phi](http://www.drdobbs.com/parallel/programming-intels-xeon-phi-a-jumpstart/240144160?pgno=1)
* [CUDA intro](http://devblogs.nvidia.com/parallelforall/easy-introduction-cuda-c-and-c/)
