## Cluster CICA

* [Hardware description](http://grupo.us.es/geterus/index.php?option=com_content&view=article&id=130&Itemid=100&lang=en)
* [Cluster report tool](http://luke.cica.es/ganglia/?m=cpu_report&r=hour&s=by%2520name&c=FIS-ATOM&h=&sh=1&hc=4)
* Login node: `democrito.cica.es`
* Development node: `scadm01`
* Activate gcc 4.8: `scl enable devtoolset-2 bash`
* Activate gcc 4.9: `scl enable devtoolset-3 bash`
* Activate gcc 5.2: `scl enable devtoolset-4 bash`

Activate mpi: add the following lines to your `/home/user/.bashrc file`

    # User specific aliases and functions
    if [ "$(hostname -s)" != "democrito" ]; then
      module load openmpi-x86_64
      alias mpirun_="mpirun --prefix /usr/lib64/openmpi --mca btl_tcp_if_include virbr1"
    fi


__General information and queue system__

* How to [use the cluster](https://info-hpc.cica.es/) (spanish)
* How to [send jobs to the queue](https://info-hpc.cica.es/wiki/SGEtutorial) (spanish)
