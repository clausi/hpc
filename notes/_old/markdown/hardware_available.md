## Currently available hardware

The following is a _selection_ of hardware for HPC available 
at this moment. 

### Intel Xeon processors

* 2015, see [microway](https://www.microway.com/knowledge-center-articles/detailed-specifications-intel-xeon-e5-2600v3-haswell-ep-processors/)
* 2016, see [Intel](http://ark.intel.com/products/family/93797/Intel-Xeon-Processor-E7-v4-Family#@Server)

__CPUs for 2 socket mainboards__

![Cores](../img/Xeon_E5_2600v3_cores.png)

![Peak performance](../img/Xeon_E5_2600v3_peak.png)

Example for an entry level server:

* Intel® Xeon® Processor E5-2670 v3 - 1589$
* Server + memory (2x32Gb) + 1HDD + 2 CPU  - 5300$

__CPUs for 4 socket mainboards__

* Comparable to E5-2670: E5-4650v3: 3800$
* For more info, [see here](https://www.microway.com/knowledge-center-articles/detailed-specifications-intel-xeon-e5-4600-v3-haswell-ep-processors/)

More detailed information [about Intel CPUs](http://ark.intel.com/#@Processors) 

### Intel Xeon Phi coprocessors (2015)

![Xeon Phi](../img/xeon_phi.png)


* Xeon Phi coprocessor 7120 - 3800$
* 1 teraflops peak performance. 61 cores, 8GB memory.
* Special prices 150€(!) for passively cooled units (31S1P).
* Runs same code as processors.

### Intel Xeon Phi processors (2016)

![Knights Landing](../img/knights_landing.jpg)

* Knights Landing: [coprocessors & processors](http://www.nextplatform.com/2016/06/20/intel-knights-landing-yields-big-bang-buck-jump/)
* AVX512
* 3,5 Tflops, 6200$ (model 7290)



### NVIDIA GPUs 2015

![NVIDIA Tesla K40](../img/NVIDIA_Tesla_K40_small.png)

* Tesla K40: peak double performance 1.43-1.66 tflops
* 2880 Cuda cores 
* Price: 3200$
* Special [coding on GPU](http://devblogs.nvidia.com/parallelforall/easy-introduction-cuda-c-and-c/)


### NVIDIA Tesla 2016

* 21 TFlops FP16 performance, 5.3 TFlops double precision
* NVLink for HPC

More info, [see NVIDIA](http://www.nvidia.com/object/tesla-p100.html)