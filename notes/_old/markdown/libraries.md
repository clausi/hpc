## Libraries

### Linear Algebra

##### BLAS

The BLAS (Basic Linear Algebra Subprograms) are routines that provide 
standard building blocks for performing basic vector and matrix operations

* Level 1 BLAS: vector-vector
* Level 2 BLAS: vector-matrix
* Level 3 BLAS: matrix-matrix 

See [Netlib](http://www.netlib.org/blas/) for the "original".

##### CBLAS

CBLAS is not a standalone library, but an interface to the FORTRAN BLAS 
routines. It can be found at [Netlib](http://www.netlib.org/blas/#_cblas),
most BLAS implementation come with a CBLAS interface. 

##### LAPACK

LAPACK provides routines for solving systems of simultaneous linear 
equations, least-squares solutions of linear systems of equations, 
eigenvalue problems, and singular value problems. LAPACK uses BLAS routines
extensively, so LAPACK preformance is closely related to BLAS performance.

See [Netlib](http://www.netlib.org/lapack/) for the "original".

##### OpenBLAS

Highly optimized implementation of BLAS and LAPACK for different 
arquitectures, freely available. The best 
BLAS for the "poor" scientist. Includes LAPACK and CBLAS interface.

* [Website](http://www.openblas.net/)
* [Manual](https://github.com/xianyi/OpenBLAS/wiki/User-Manual)
* [Wiki](https://github.com/xianyi/OpenBLAS/wiki)

##### Intel MKL

Intel MKL (Math Kernel Library) is a highly optimized library for Intel 
processors. Includes BLAS, LAPACK, sparse and parallel versions, partial
differential equations, FFT and vector mathematical functions. The best
library out there for Intel, only available with Intel compiler 
products which start at 699$. Free versions for students, educators and
researchers [here](https://software.intel.com/en-us/qualify-for-free-software).

[More Info](https://software.intel.com/en-us/intel-mkl)

##### AMD ACML and ACL

AMD ACML (AMD Core Math Library) is AMD's answer to Intel's MKL. AMD
has transitioned from propietary to open source, so these libraries
are available for free. BLAS, LAPACK, FFT and RNG.

* [ACML](http://developer.amd.com/tools-and-sdks/archive/amd-core-math-library-acml/)
* [ACL (OpenCL)](http://developer.amd.com/tools-and-sdks/opencl-zone/acl-amd-compute-libraries/)

