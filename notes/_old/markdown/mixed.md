## Mixed MPI - OpenMP programming

Please see examples in `src/mpi/stencil`, especially `3dmpi4.c`

Use option `-x` to propagate `OMP_NUM_THREADS` to environments in which
mpi processes are started:

    export OMP_NUM_THREADS=4
    mpirun -np 4 -x OMP_NUM_THREADS -hostfile hosts.txt 3dmpi4
