### MPI - Game of life

Domain: orthogonal grid of square cells. Usually considered infinte, we
will use periodic boundary conditions in this example.

![gameoflife1](../img/gameoflife1.png)

Rules (see [wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)):

* Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
* Any live cell with two or three live neighbours lives on to the next generation.
* Any live cell with more than three live neighbours dies, as if by overpopulation.
* Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

Example:


![life1](../img/life1.gif)

A (not optimized) implementation in C would be

    void evolve(int nx, int ny) {
      for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
          int n = 0;
          for (int i1 = i - 1; i1 <= i + 1; i1++) {
            for (int j1 = j - 1; j1 <= j + 1; j1++) {
              if (univ[(i1 + nx) % nx][(j1 + ny) % ny]) {
                n++;
              }
            }
          }
          if (univ[i][j]) n--;
          new[i][j] = (n == 3 || (n == 2 && univ[i][j]));
        }
      }
      for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
          univ[i][j] = new[i][j];
        }
      }
    }

For the periodic boundary conditions we can use 

    (i + nx) % nx

or using "shadow cells"

![domain1](../img/shadow0.png)

A simple domain decomposition with shadow cells might be

![domain1](../img/shadow1.png)

