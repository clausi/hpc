### Notebook Tecra Z50 [i7-4600U](http://ark.intel.com/products/76616/Intel-Core-i7-4600U-Processor-4M-Cache-up-to-3_30-GHz)

    CPU                |  Core i7-4600U (Haswell, AVX2)   
    Cores              |  2
    Threads            |  4
    CPU-Freq           |  2.1 GHZ - 3.3GHz
    RAM                |  8GB
    Mem-BW             |  25,6GB/s
    L1                 |  32KB
    L2                 |  256KB
    L3                 |  4MB
    DP FLOPS/cycle     |  16 (two 4-wide FMA3)
    SP FLOPS/cycle     |  32 (two 8-wide FMA3)
   
    stream1            |  8.9 GB/s
    streamn            |  8.8 GB/s
    flopstriad1        |  16.4 / 32.8 GFlops/s
    flopstriadn        |  26.6 / 53.8 GFlops/s

### scadm01 [AMD Opteron 6344](http://www.amd.com/en-gb/products/server/opteron/6000/6300#)

    CPU                |  AMD Opteron 6344 (Abu Dhabi)   
    Cores              |  2 x 12
    CPU-Freq           |  2.6 GHZ - 3.2 GHz
    RAM                |  256 GB
    L1                 |  16KB
    L2                 |  2MB
    L3                 |  6MB x 4
    DP FLOPS/cycle     |  8 (4-wide FMA4)
    SP FLOPS/cycle     |  16 (8-wide FMA4)

### Instance t2.micro on AWS

    CPU                | Intel(R) Xeon(R) CPU E5-2676 v3 @ 2.40GHz
    Cores              | 1
    Threads            | 1
    Cpu GHz            | 2.4
    cache size         | 30720 KB
    RAM                | 1 GB
    Bench:stream1      | 8.9 GB/s
    Bench:flopstriad1  | 24.02 GFlops/s

### Xeon Phi on MTL  
    CPU                |  Intel Xeon Phi 7120P
    Cores              |  61 
    Threads            |  122/244
    Cpu GHZ            |  1.238
    flopstriad1        |  19.5 GFlops/s (float) 9.7 GFlops/s (double)
    flopstriadn        |  2200 GFlops/s (float) 1004 GFlops/s (double)
