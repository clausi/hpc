_Introducción a la computación de alto rendimiento (HPC) con OpenMP y MPI_

El curso proporcionará los conocimientos básicos para iniciarse en el tema de la 
computación paralela. Se tratarán los siguientes temas

*    Descripción breve del hardware más común de sistemas actuales. Intel Xeon y Core (i7 etc), Intel Xeon Phi. No se trataran GPUs (procesadores gráficos).
*    Optimización de programas “secuenciales” (SIMD)
*    Programación para memoria compartida con OpenMP
*    Programación con MPI (paso de mensajes)

Este programa es muy amplio y por lo tanto sólo puede entenderse como una introducción a cada tema, proporcionando 
una base para un aprendizaje autónomo posterior. Será necesario el conocimiento de un lenguaje de 
programación (FORTRAN o C) para poder seguir los ejemplos.