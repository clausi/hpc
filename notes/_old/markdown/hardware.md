## Hardware

* [Currently available hardware](hardware_available.md)
* [Hardware available for this course](hardware_used.md)
    * [Xeon 2600v3](xeon2600v3.md)
    * [AMD 6200/6300](amd6200.md)
    * [AVX theoretical peak Flops](is_flops.md)

![Von_Neumann](../img/von_neumann.png)


##### Haswell 8 core

![Haswell_8](../img/haswell_die.jpg)

##### Bulldozer 8 core

![Bulldozer_8](../img/Bulldozer3.jpg)


##### Xeon Phi

![Phi_Ring](../img/xeonphi_ring.jpg)

[See Intel site](https://software.intel.com/en-us/articles/intel-xeon-phi-coprocessor-codename-knights-corner)



### Obtain information about our hardware

##### Windows:

* Windows: Start -> Right-click on "Computer" -> Properties
* Program [cpu-z](http://www.cpuid.com/softwares/cpu-z.html)

![Win System](../img/sistema_tecra_z50_small.png)

![CPU-Z](../img/cpuz_tecra_z50.png)

##### Linux: 

* `cat /proc/cpuinfo`
* `hwloc-ls` or `lstopo`
* `numactl --hardware`

Example:

    processor       : 3
    vendor_id       : GenuineIntel
    cpu family      : 6
    model           : 69
    model name      : Intel(R) Core(TM) i7-4600U CPU @ 2.10GHz
    stepping        : 1
    cpu MHz         : 2693.000
    cache size      : 4096 KB
    physical id     : 0
    siblings        : 4
    core id         : 1
    cpu cores       : 2
    .... more info ....

![lstopo](../img/lstopo_tecra_z50.png)
