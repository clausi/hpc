## Introduction

### HPC is an essential tool for science and industry

In this section we will show a few examples where HPC is employed.
This list is by no means exhaustive!

##### Galaxy formation and evolution

![Simulation](../img/blowout.png)

* [EAGLE](http://icc.dur.ac.uk/Eagle/index.php)
* [Video at youtube](https://www.youtube.com/watch?v=-awYNyznf58)

##### The Grid - CERN

![Links at CERN grid](../img/cern_grid600.jpg)

* Video [Grid at CERN](http://insidehpc.com/2013/10/cern-uses-grid-global-supercomputer/)

##### Weather forecasts (El hombre del tiempo)

![Mariano Medina, primer "Hombre del tiempo"](../img/mariomedina.jpg)

* Supercomputation at AEMET (spanish)[AEMET](http://www.aemet.es/en/idi/tic/supercomputacion) 
* [Acquisition of a new system](http://insidehpc.com/2015/02/spanish-meteorological-procures-168-teraflop-bull-supercomputer)
* HPC in [weather forecasts](http://insidehpc.com/2014/07/raining-hpc-next-gen-supercomputing-will-shape-weather-forecasts)

##### Formula 1

![CFD in Formula 1](../img/aero_fluid_flow_f1_car_small.jpg)

* [Aerodynamics of a F1](http://www.formula1-dictionary.net/aerodynamics_of_f1.html)
* CFD and wind tunnel testing has now been limited by rules. 30 units per week, 1 unit = 1 hour of wind tunnel or 1 teraflop.

##### Car industry, product design
![Car crash](../img/crash1.jpg)

[//]: # (Use of HPC in [car industry](http://www.automotiveitnews.org/articles/share/993346/)

* RADIOSS [comercial application](http://www.altairhyperworks.com/product/RADIOSS)
* Hall of Fame de [Ansys](http://www.ansys.com/Other/Hall-of-Fame/Archive)
* advertising brochure [HPC at Ansys](http://www.solidcad.co.za/WebsiteAssets/pdf/brochures/ansys-hpc-brochure.pdf)

##### Artificial intelligence, deep learning

![Neural network](../img/neural_network.png)

* Clarifai [demo](https://www.clarifai.com/demo)
* Caffe [demo](http://demo.caffe.berkeleyvision.org/)
* Neural Networks and Deep Learning [Online book](http://neuralnetworksanddeeplearning.com)
* Reactive, a startup about Kanji recognition [Bloomberg](https://www.bloomberg.com/news/articles/2016-04-13/artificial-intelligence-s-next-phase-sooner-and-more-accessible-for-everyone)
* AI and [social implications](https://www.top500.org/news/white-house-ponders-the-ramifications-of-ai/)
* Ethics [board](http://www.dailymail.co.uk/sciencetech/article-3812740/The-justice-league-AI-Tech-giants-reveal-artificial-intelligence-ethics-board-Apple-NOT-it.html)

##### Etc., etc....



### Moore 's law

Paper 1965: [Electronics, pp. 114–117, April 19, 1965](http://www.cs.utexas.edu/~fussell/courses/cs352h/papers/moore.pdf) 

__Synopsis__: The number of components (on a 
single wafer) determinates the cost/component. For a given technology
in the production process (state of the art) there is a minimum of this
cost for a given component density. The component density where
this minimum is located increases exponentially with time. So cost
drives integration. The initial 
prediction was for the component density to double each 
year (extrapolation 1965-1975)

Later revised to x 2 each two years 

Performance doubles every 18 months.

2015 Moor's law celebrated it's 50th birthday with remarkable success:

![Moor's Law](../img/Moores_Law_small.png)

__But ...__


[2005 - The Free Lunch Is Over](http://www.gotw.ca/publications/concurrency-ddj.htm)
![2005 - Free Lunch is over](../img/free_lunch_small.png)

    Applications will increasingly need to be concurrent if they want 
    to fully exploit continuing exponential CPU throughput gains

    Efficiency and performance optimization will get more, not less, important

Reads about Memory Wall, Power Wall, ILP (Instruction Level Parallelism) wall:

* [Dennard scaling](https://en.wikipedia.org/wiki/Dennard_scaling)
* [The future of computers](http://www.edn.com/design/systems-design/4368705/The-future-of-computers--Part-1-Multicore-and-the-Memory-Wall)
* [Post-Dennard Scaling and the final Years of Moore's Law](https://www.hs-augsburg.de/Binaries/Binary20963/PostDennard.pdf)
* [1994 - Memory wall](http://www.di.unisa.it/~vitsca/SC-2011/DesignPrinciplesMulticoreProcessors/Wulf1995.pdf)

### Top 500

Ranking of the fastest (Linpack-fastest) supercomputers in the world: 
[http://www.top500.org/](http://www.top500.org/)

June 2015:

![Top10](../img/top500_june2015.png)

June 2016:

![Top10](../img/top500_june2016.png)

![Performance](../img/top500_perform.png)

Number of Cores vs Rank
![Cores vs Rank](../img/cores_vs_rank.png)

Sunway TaihuLight
![TaihuLight](../img/sunway_taihulight.jpg)
![TaihuLight data](../img/taihulight_data.png)

* [Sunway report](../pdf/sunway-report-2016.pdf)

### What will we probably see the next years?

__High interest in HPC:__

* April 2015: USA blocks export of Xeon Phi technology to China [PC-World](http://www.pcworld.com/article/2908692/us-blocks-intel-from-selling-xeon-chips-to-chinese-supercomputer-projects.html)
* 15 th July 2015: China announces [homegrown DSP](http://www.nextplatform.com/2015/07/15/china-intercepts-u-s-intel-restrictions-with-homegrown-supercomputer-chips/) as component in Thianhe-2A
* 29 th July 2015: Obama issues Executive Order establishing the National 
  Strategic Computing Initiative (NSCI) 
  [White House](https://www.whitehouse.gov/blog/2015/07/29/advancing-us-leadership-high-performance-computing).
  Next decade: exaflops and exabytes (numerical análisis meets big data)
* June 2016 [New chinese chip in #1 computer](http://www.nextplatform.com/2016/06/20/look-inside-chinas-chart-topping-new-supercomputer/)
* January 2017 [Trump Administration Preps Plan for Huge Cuts to Department of Energy](https://www.top500.org/news/trump-administration-preps-plan-for-huge-cuts-to-department-of-energy/)
* March 2017 [EU Ratchets up the Race to Exascale Computing](https://www.hpcwire.com/2017/03/29/eu-ratchets-race-exascale-computing/)

__Technological developments:__

* GPU or Xeon Phi type accelerators + CPU. Higher level of integration.
* 3D Memory (like [Hybrid Memory Cube](http://www.micron.com/products/hybrid-memory-cube)), close to CPUs
* [Integrate processing with memory](https://www.top500.org/news/hpe-unveils-new-prototype-of-memory-driven-computer/)

__Computing Infrastructures:__

* Computing clouds
  * AWS - [Amazon Web Services](https://aws.amazon.com/?nc1=h_ls)
  * [Science foundation clouds](http://www.cloudlab.us/index.php)

