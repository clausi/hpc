## Xeon 2600v3

* [Haswell review](http://anandtech.com/show/8423/intel-xeon-e5-version-3-up-to-18-haswell-ep-cores-)
* [2600v3 description](http://www.pcper.com/reviews/Processors/Intel-Xeon-E5-2600-v3-Processor-Overview-Haswell-EP-18-Cores)
* [2 vs 4 socket system](https://www.microway.com/hpc-tech-tips/intel-xeon-e5-4600v3-cpu-review/)
* [E5 vs E7 in 4 socket systems](https://software.intel.com/en-us/blogs/2014/01/28/memory-latencies-on-intel-xeon-processor-e5-4600-and-e7-4800-product-families)

