## Theoretical peak flops/cycle various instruction sets

From [stackoverflow](http://stackoverflow.com/questions/15655835/flops-per-cycle-for-sandy-bridge-and-haswell-sse2-avx-avx2)

For Haswell, [see also](../pdf/Theoretical_Peak_FLOPS_per_instruction_s.pdf)

Intel Core 2 and Nehalem:

* 4 DP FLOPs/cycle: 2-wide SSE2 addition + 2-wide SSE2 multiplication
* 8 SP FLOPs/cycle: 4-wide SSE addition + 4-wide SSE multiplication

Intel Sandy Bridge/Ivy Bridge:

* 8 DP FLOPs/cycle: 4-wide AVX addition + 4-wide AVX multiplication
*  16 SP FLOPs/cycle: 8-wide AVX addition + 8-wide AVX multiplication

Intel Haswell/Broadwell:

* 16 DP FLOPs/cycle: two 4-wide FMA (fused multiply-add) instructions
* 32 SP FLOPs/cycle: two 8-wide FMA (fused multiply-add) instructions

AMD K10:

* 4 DP FLOPs/cycle: 2-wide SSE2 addition + 2-wide SSE2 multiplication
* 8 SP FLOPs/cycle: 4-wide SSE addition + 4-wide SSE multiplication

AMD Bulldozer/Piledriver/Steamroller, per module (two cores):

* 8 DP FLOPs/cycle: 4-wide FMA
* 16 SP FLOPs/cycle: 8-wide FMA

Intel MIC (Xeon Phi), per core (supports 4 hyperthreads):

* 16 DP FLOPs/cycle: 8-wide FMA every cycle
* 32 SP FLOPs/cycle: 16-wide FMA every cycle

Intel MIC (Xeon Phi), per thread:

* 8 DP FLOPs/cycle: 8-wide FMA every other cycle
* 16 SP FLOPs/cycle: 16-wide FMA every other cycle

