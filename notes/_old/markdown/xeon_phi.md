#Intel Xeon Phi Coprocessor High-Performance Programming
*Jim Jeffers
*James Reinders
Elsevier

Claves
* Exponer? paralelismo
* minimizar comunicaciones (maximizar localismo)

# Capitulo 1 - Introducción

Fig. 1.1 Clock speed - years

Fig. 1.2 Core count - years

Fig. 1.3 Vector width - years

Más rapido -> Más paralelo (Moore continua)

Hardware: > 50 cores, varios threads/core, 512bits SIMD

Ninja gap: "Can Traditional Programming Bridge the Ninja Performance Gap for Parrallel Computing Applications?", Satish et al. 2012

Para Xeon Phi: 

* Primero asegurar un optimo rendimiento en Xeon.
* Scaling > 100