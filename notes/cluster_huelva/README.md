# Notas Cluster Huelva

## Acceso

    ssh -X -p PORT user@tyrell.uhu.es

Notas:

* `-X` permite usar el entorno gráfico local para aplicaciones gráficas   ejecutadas en el servidor (por ejemplo ver la ventana del `gedit`)
* `-p` identifica el número del puerto de ssh

## Configuración de clave

Linux: En el **cliente** (mi máquina local) teclear `ssh-keygen` en un shell
y usar los valores por defecto.

En el  **servidor** añadir su clave **publica** como generada en el paso anterior
(normalmente en `~/.ssh/id_rsa.pub` en el **cliente**) al fichero
`~/.ssh/authorized_keys` del **servidor**. Ahora puede realizar una conexión ssh
sin introducir la clave.

## Intel compilers

* Intel compilers: `module load intel201X` where X = 7,8,9.
* Available modules: `module spider`

## Jupyter Notebook

* login into login0 node
* load necessary modules
* on login0:  `jupyter notebook --no-browser --port=8889`
* on localhost: `ssh -p PORT -N -f -L localhost:8888:localhost:8889 username@HOST`


## SLURM

Configuration in /etc/slurm/slurm.conf

    Nodename=c01 Sockets=2 CoresPerSocket=12 ThreadsPerCore=1 RealMemory=382000
    Nodename=c[02-10] Sockets=2 CoresPerSocket=12 ThreadsPerCore=1 RealMemory=191000
    Nodename=c[11-14] Sockets=1 CoresPerSocket=64 ThreadsPerCore=4 RealMemory=96000 Feature=knl

CPU's, cores, tasks et. al

* -N, --nodes -> "nodos" (c01, c02, ..)
* -n, --ntasks -> "tareas de MPI" -> number of processors
* --cpus-per-task -> number of cores for each task

Examples

* srun -n 1 --cpus-per-task=4  -> un "programa/tarea" con 4 cores
* sinfo -> available machines and allocated resources
* squeue -> current jobs
* salloc -c8 --nodelist  # make reservation  
* export | grep SLURM -> environment for SLURM
* srun program
* scancel 7895
* sstat, sview

Mapping with icc

* export KMP_AFFINITY=verbose,compact -> 0-A0 1-A1 
* export KMP_AFFINITY=verbose,scatter -> 0-A0 1-B0 

## KNL

* `-march=knl`
* run on KNL

    srun -n1 --cpus-per-task=64 --threads-per-core=4 --hint=compute_bound --cpu-bind=v --partition=knl flopstriadn
    cpu-bind-cores=UNK  - c11, task  0  0 [213702]: mask 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff set
    Initializing for 8 byte floating point
    Starting Compute on 256 threads
    GFlops =   3276.800, Secs =      1.649, GFlops per sec =   1987.333

## Documentos PDF

* <a href="Introduccion_cluster_UHU.pdf" target="_blank">Introducción</a>
* <a href="07_SLURM_test.pdf" target="_blank">Ejemplos de uso SLURM</a>
* <a href="07_SLURM_Tutorial_Admin.pdf" target="_blank">SLURM tutorial admin</a>