# Fortran notes

## Manual

<a href="Fortran95-Manual.pdf" target="_blank">Fortran 95 Manual</a>

## Style

<a href="Fortran_Style.pdf" target="_blank">Fortran Coding Standards and Style</a>

## Vectorization

* <a href="Pub69214.pdf" target="_blank">Effective Vectorization with OpenMP 4.5</a>
* <a href="vectorization_intel_notes.pdf" target="_blank">Vectorization for Intel® C++ & Fortran Compiler</a>
