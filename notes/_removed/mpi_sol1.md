### Solution - Stencil code

Exercise: Goto `src/mpi/exercise`. Program a MPI version of stencil3d.

Remember decomposition with _shadow_ regions:

![domain2](../img/domain2.png)

We wanted to _cut_ the 3D cube in a way that 
cells can be sent as a 
one dimensional array to the shadow regions of the neighbors.

![domain3](../img/domain3.png)

This can be achieved by interpreting (in the case of the C language with
row-major order of arrays) the array indexes as follows:

![domain3](../img/domain4.png)

In this case, if we include the surface cells of the cube when sending
cells to neighbors, we can send simply an array of Ny*Nz elements to
our neighbors:


    #define NPERPROC (NMAXX-2)/NDIV+2// 2^(k-p)+2 storage for each process
    ...

    double ou[NPERPROC][NMAXY][NMAXZ];
    ...

    nslice = NMAXY*NMAXZ;
    if (nproc < numprocs-1) {  // send right, receive from right
      MPI_Send(&ou[NPERPROC-2][0][0],nslice,MPI_DOUBLE,nproc+1,0,MPI_COMM_WORLD);
      MPI_Recv(&ou[NPERPROC-1][0][0],nslice,MPI_DOUBLE,nproc+1,1,MPI_COMM_WORLD,&status);
    }
    if (nproc > 0) {  // receive from left, send left
      MPI_Recv(&ou[0][0][0],nslice,MPI_DOUBLE,nproc-1,0,MPI_COMM_WORLD,&status);
      MPI_Send(&ou[1][0][0],nslice,MPI_DOUBLE,nproc-1,1,MPI_COMM_WORLD);
    }

Note: This solution is not the most efficient one, but the one with the 
shortest program. As next steps we should consider the following
improvements of our code:

* avoid redundant communications (don't send boundary values)
* reduce communications by smarter domain decomposition
* try to maximize memory reuse
* use of threads in each MPI process
