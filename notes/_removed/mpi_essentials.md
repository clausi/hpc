## MPI Essentials

* [prepare shell environment for MPI] 
* Use `#include <mpi.h>` (C) or `INCLUDE ’mpif.h’` (Fortran)
* Use `mpicc/mpif77/mpif90` as compilers
* Use `mpirun -np 4 -H host1,host2 miprog` to run. Remember, we defined: `mpirun_`


##### MPI initialization

    ------------------------    C    -------------------------
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&np);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);

    ------------------------ Fortran -------------------------
    call MPI_INIT(ierror)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, np, ierror)
    call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierror)

* [`MPI_Init`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Init.3.php)
* [`MPI_Comm_size`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Comm_size.3.php)
* [`MPI_Comm_rank`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Comm_rank.3.php)


##### MPI finalize

    ------------------------    C    -------------------------
    MPI_Finalize();
   
    ------------------------ Fortran -------------------------
    call MPI_FINALIZE(ierror)

* [`MPI_Finalize`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Finalize.3.php)

##### Barrier

    ------------------------    C    -------------------------
    MPI_Barrier(MPI_COMM_WORLD);

    ------------------------ Fortran -------------------------
    call MPI_BARRIER(MPI_COMM_WORLD, ierror)

* [`MPI_Barrier`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Barrier.3.php)

##### Runtime environment

    ------------------------    C    -------------------------
    secs = MPI_Wtime();

    ------------------------ Fortran -------------------------
    secs = MPI_Wtime()

* [`MPI_Wtime`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Wtime.3.php)

##### Send data

    ------------------------    C    -------------------------
    MPI_Send(buffer,nmax, MPI_INT, np_to, 0, MPI_COMM_WORLD);
   
    ------------------------ Fortran -------------------------
    call MPI_SEND(BUF, NMAX, MPI_INT, NP_TO, 0, MPI_COMM_WORLD, IERROR)


* [`MPI_Send`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Send.3.php)
* If possible, use combined version: [`MPI_Sendrecv`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Sendrecv.3.php)

##### Receive data

    ------------------------    C    -------------------------
    MPI_Recv(buffer,nmax, MPI_INT, np_from, 0, MPI_COMM_WORLD,&status);
   
    ------------------------ Fortran -------------------------
    call MPI_RECV(BUF, NMAX, MPI_INT, NP_FROM, 0, MPI_COMM_WORLD, IERROR)

* [`MPI_Recv`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Recv.3.php)
* If possible, use combined version: [`MPI_Sendrecv`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Sendrecv.3.php)

##### Reduce

    ------------------------    C    -------------------------
    MPI_Reduce(sum,res,1,MPI_DOUBLE,MPI_SUM, root,MPI_COMM_WORLD);
   
    ------------------------ Fortran -------------------------
    soon available :)


* [`MPI_Reduce`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Reduce.3.php)
* Also reduces vectors to vectors
* See also `MPI_Allreduce`, `MPI_Reduce_Scatter`

##### Cartesian topologies

    ------------------------    C    -------------------------
    // create cartesian grid
    for (n=0;n<3;n++) {         // no periodicity
      periods[n]=0;
    }
    dims[0]=NDIVX;
    dims[1]=NDIVY;
    dims[2]=NDIVZ;
    MPI_Cart_create(MPI_COMM_WORLD,3,dims,periods,1,&comm_cart);

    // get my own coordinates in topology
    MPI_Cart_coords(comm_cart,myid,3,mycoords);

    // shift right (1) in first dimension (0)
    me=myid;
    MPI_Cart_shift(comm_cart,0,1,&me,&other);
    // shift left (1) in second dimension (1)
    me=myid;
    MPI_Cart_shift(comm_cart,1,-1,&me,&other);


    ------------------------ Fortran -------------------------
    soon available :)

* [`MPI_Cart_create`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Cart_create.3.php)
* [`MPI_Cart_coords`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Cart_coords.3.php)
* [`MPI_Cart_shift`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Cart_shift.3.php)
* Other useful functions: `MPI_Cart_rank`, `MPI_Cart_sub`


##### Define own data types

Example:

![vector_example](../img/mpi_vector.png)

    ------------------------    C    -------------------------
    // create vector type with stride
    MPI_Type_vector(NY-2,NZ-2,NZ,MPI_DOUBLE,&cutx);
    MPI_Type_commit(&cutx);

    ------------------------ Fortran -------------------------
    soon available :)

* count=NY-2 blocks. blocklength=NZ-2 elements in block, stride=NZ distance between blocks
* [`MPI_Type_vector`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Type_vector.3.php)
* [`MPI_Type_commit`](https://www.open-mpi.org/doc/v1.8/man3/MPI_Type_commit.3.php)
