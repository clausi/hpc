## OpenMP Essentials

* Use `-fopenmp` as a compiler flag
* `export OMP_NUM_THREADS=4`
* Run your program


##### Parallel for

    ------------------------    C    -------------------------
    #pragma omp parallel for [clauses]
    for (i=0;i<N;i++) {
    }

    ------------------------ Fortran -------------------------
    !$OMP PARALLEL DO [clauses]
    DO I=1,N
    ENDDO
    [!$OMP END PARALLEL DO]

* is a combination of `parallel` and `for` (see these pragmas)

##### Parallel

    ------------------------    C    -------------------------
    #pragma omp parallel [clauses]
    {
    }
   
    ------------------------ Fortran -------------------------
    !$OMP PARALLEL [clauses]
    ..
    !$OMP END PARALLEL

* a thread that encounters `parallel` creates a team and becomes 
  master with id=0 
* implied barrier at end
* master is the one to continue
* no branches into parallel

##### For/Do

    ------------------------    C    -------------------------
    #pragma omp for [clauses]
    for (i=0;i<N;i++) {
    }

    ------------------------ Fortran -------------------------
    !$OMP DO [clauses]
    [!$OMP END DO]

* worksharing construct: distributes between threads, _does not_ create threads
* implied barrier at end (if `nowait` is not specified)
* must be encountered by all threads or none

##### Runtime environment

    ------------------------    C    -------------------------
    iam = omp_get_thread_num();   
    nt = omp_get_num_threads();
    double start = omp_get_wtime();

    ------------------------ Fortran -------------------------
    iam = omp_get_thread_num() 
    nt = omp_get_num_threads()
    double precision start = omp_get_wtime()

* include `omp.h` in your source
* compile with -fopenmp (links in lib) or use conditional compilation

##### Conditional compilation

    ------------------------    C    -------------------------
    #ifdef _OPENMP
      iam =  omp_get_thread_num();
    #else
      ..
    #endif
   
    ------------------------ Fortran -------------------------
    !$  iam =  omp_get_thread_num() 
    #ifdef .. #endif

##### Synchronization: Only one at a time

    ------------------------    C    -------------------------
    #pragma omp critical 
    {}

    ------------------------ Fortran -------------------------
    !$omp critical
    ..
    !omp end critical

* all threads pass ths section, but only one at a time 
* this is a serial construct, have a talk with Mr. Amdahl

##### Synchronization: Wait for the last

    ------------------------    C    -------------------------
    #pragma barrier 

    ------------------------ Fortran -------------------------
    !$omp barrier

* All threads wait here until the last has arrived before they may continue
* Must be encountered by all or none

##### Clauses

   
    ---------------------  C & Fortan ------------------------
    private(var1,var2) 
    reduction(+:var)
    schedule(dynamic)
    collapse(n) 

