## Programming for shared memory with OpenMP (Open Multi-Processing)

### How to use OpenMP

* Use `-fopenmp` as a compiler flag
* Include `#pragma omp parallel for` at the right places
* `export OMP_NUM_THREADS=4`
* Run your program

![Mandel scaling](../img/mandel_scaling.png) 

