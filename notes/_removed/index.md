###Index

* [Introduction - Why HPC?](intro.md)
* [Hardware](hardware.md)
* [Vectorization (single thread)](vectorization.md)
    * [Scale Example](scale.md)
	* [Mandelbrot Example](mandel.md)
* [Programming for shared memory with OpenMP](openmp.md)
    * [Exercise: Mandelbrot](openmp_ejer1.md)
    * [NUMA aspects with OpenMP](openmp_numa.md)
    * [SIMD with OpenMP 4.0](openmp_simd.md)
* [Programming with message passing (MPI)](mpi.md)
    * [Example: Game of Life](mpi_gameoflife.md)
    * [Exercise: Stencil](mpi_ejer1.md)
* [Mixed MPI - OpenMP programming](mixed.md)

[comment]: <> ( - [Solution](mpi_sol1.md))

###References

* [Libraries](libraries.md)
* [Literature](literature.md)
* [Links](links.md)
* [Tricks](tricks.md) - [Vim](vim.md) - [Git](git.md)

###System documentation

* [androbi.com](androbi.md)
* [Cluster CICA](cluster_cica.md)
* [Intel MTL](intel_mtl.md)
