## Sources of images used

I appreciate the original authors work of the following images used
in this course:

* [Eagle image](http://icc.dur.ac.uk/Eagle/blowout.png)
* [Mariano Medina, primer "Hombre del tiempo" 1977](http://estaticos02.elmundo.es/albumes/2007/06/14/sexta/1181831999_extras_albumes_1.jpg)
* [CFD in Formula 1](http://www.formula1-dictionary.net/Images/aero_fluid_flow_f1_car_small.jpg)
* [Car crash](http://innovationintelligence.com/wp-content/uploads/2015/09/crash1.jpg)
* [Concrete truck, screen shot of video](http://www.ansys.com/staticassets/ANSYS/Hall%20of%20Fame/staticassets/2014-iab-weimar.mp4?thewin=popup&width=640&height=380)
* [Moore's Law](http://education.mrsec.wisc.edu/SlideShow/images/computer/Moores_Law.png)
* [Free Lunch Is Over](http://www.gotw.ca/images/CPU.png)
* [Thiane-2](http://www.top500.org/featured/top-systems/tianhe-2-milkyway-2-national-university-of-defense/)

