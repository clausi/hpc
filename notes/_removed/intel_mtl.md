## Intel MTL (Manycore testing Lab)

* Access MTL: ssh [-Y] username@207.108.8.131
* Users denk-s01 ... denk-s05
* Login node: `acano01`
* Xeon Phi node: `xeon2`
* use intel compilers: `source /opt/intel/bin/compilervars.sh intel64` 
 
    * C: icc
    * C++: icpc
    * Fortan ifort
* [Getting Started](../pdf/Getting Started Guide.pdf) (a bit outdated)
