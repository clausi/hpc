## Programming with the Message Passing Interface (MPI)

* For a quick refernce, check our [MPI Essentials](mpi_essentials.md)

##### What is MPI

* An API for message passing between processes
* Provides scripts for compiling 
     * `mpicc`, `mpic++`
     * `mpif77`, `mpif90`, `mpifort`
* And running 
     * `mpirun`, `mpiexec`
* Available on practically all HPC clusters 

##### Implementations

* [Open MPI](http://www.open-mpi.org/) - Open Source High Performance Computing
* [MPICH](http://www.mpich.org/) 
* Vendor specific implementations

##### A first program with MPI

    #include <mpi.h>
    #include <stdio.h>

    int main(int argc, char *argv[])
    {
        int myid, np, nlen;
        char name[MPI_MAX_PROCESSOR_NAME];

        // initialize and get info about world and ourselves
        MPI_Init(&argc,&argv);
        MPI_Comm_size(MPI_COMM_WORLD,&np);
        MPI_Comm_rank(MPI_COMM_WORLD,&myid);
        MPI_Get_processor_name(name, &nlen);

        printf("Process %d of %d is running on %s\n", myid,np,name);
        fflush(stdout);

        MPI_Finalize();
        return (0);
    }

Exercise

* Setup mpi environment as described [here](cluster_cica.md)
* Try to compile with `gcc hello.c`, observe missing references
* Type `mpicc --showme`
* Compile with `mpicc hello.c -o hello`. Run with `./hello`
* Run with `mpirun_ -np 2 ./hello`

##### Running on different nodes

Processes on remote hosts are started like a remote shell by MPI. You 
must be able 
to `ssh` into the remote hosts without entering a password. Usually the 
easiest way to
set up a cluster is to share the file system between nodes (same files are
visible for every user on each node). Then, ssh may be set up to authenticate
by key pairs inside the cluster:

* [ssh without password](https://sites.google.com/a/case.edu/hpc-upgraded-cluster/cluster-faq/how-to-set-up-ssh-without-password)

In order to specify on which nodes of the cluster the program will run,
we prepare a `hostsfile` which lists the hosts we want to run our program on:

     
      scadm01
      scadm02
      ...

Exercise:

* Provide a host file with 2 different nodes
* make shure You can `ssh` into these hosts without password
* `mpirun_ -np 1 --hostfile hosts.txt ./hello`
* `mpirun_ -np 2 --map-by node --hostfile hosts.txt ./hello`
* `mpirun_ -np 4 --map-by node --hostfile hosts.txt ./hello`
* `mpirun_ -np 25 --map-by slot --hostfile hosts.txt ./hello`

##### Communications

Performance of a HPC system depends heavily on the speed with which data can 
be interchanged between the nodes. Some current technologies (2015) are

* 1 Gb Ethernet, cheap and slow
* 10 Gb Ethernet, in between. Quite cheap already.
* Infiniband, expensive and fastest

For a nice comparison and other insights, see
[Interconnect Analysis](http://www.hpcadvisorycouncil.com/pdf/IB_and_10GigE_in_HPC.pdf)

When MPI processes are run on nodes with shared memory, the 
shared memory is used to provide an efficient communication layer.

##### PingPong

we will now write a small program to benchmark point to point communications 
in our cluster.

![Pingpong](img/pingpong.png)

The heart of the program is

    other = 1 - myid;

    MPI_Barrier(MPI_COMM_WORLD);
    if (myid==0)
      secs=MPI_Wtime();
    for (n=0;n<niter;n++) {
      if (myid == 0) {
        MPI_Send(buffer,nmax, MPI_INT, other, 0, MPI_COMM_WORLD);
        MPI_Recv(buffer,nmax, MPI_INT, other, 0, MPI_COMM_WORLD,&status);
      }
      else {
        MPI_Recv(buffer,nmax, MPI_INT, other, 0, MPI_COMM_WORLD,&status);
        MPI_Send(buffer,nmax, MPI_INT, other, 0, MPI_COMM_WORLD);
      }
    }

Exercise:

* Run the program with two processes on the same node 
* Now run with the same parameters, but on different nodes. 

I have run this benchmark on the cluster,
<a href="pdf/pingpong_cluster.pdf" target="_blank">see results</a>.

Further reads:

* [MPI benchmarks](https://software.intel.com/sites/default/files/managed/66/e8/IMB_Users_Guide.pdf)
