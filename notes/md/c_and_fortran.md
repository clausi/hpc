# Program exercises in C and Fortran

The programs that I have included in this course are written in ANSI C ( `-std=c90` ) and
Fortran 95 (`-std=f95`) (with some exceptions where necessary). I have tried to make the
programs as easy to understand as possible.

The repository has a source code structure similar to one you might be using
in your own projects, all programs can be found in `src/c` and `src/f`.
In order to compile a program just type `make` in the corresponding 
directory. `make clean` removes binary and temporary files.

There are some common utilities which have to be compiled before
compiling/linking some of the programs. These common files are described in the
following two sections.

## Utilities

The directory `src/utilities` contains some utilites which are
used by the other programs such as timing routines and helpers for
reading and saving the ppm image format. These routines are written
in C and have a Fortran interface (Fortran 2003).

**Note:** For this course you don't need to understand how these
functions work, just use them as little tools in your programs.
Interfacing C and Fortran as well as some of the C code used for
the images are out of scope for an introductory course.

The C routines are (include `timing.h` and `ppm.h` and link
with `timing.o` and `ppm.o`)

    // use to start timer
    void start_timer();
    // stop timer and return seconds
    double stop_timer();

    // create a hsv image with given dimensions (and some calculated pattern)
    IMAGE *makeimage(int nx, int ny);
    // read a rgb image from file
    IMAGE *readimage(char *filename); 
    // save image to file (converts output pixels to rgb if necessary)
    void saveimage(const IMAGE *img, char *filename);
    // set image pixel (i,j) to (rh, gs, bv)
    void setpixel(IMAGE *img, int i, int j, float rh, float gs, float bv);

The Fortran equivalents of these functions are (we don't return pointers in Fortran, so the interface is slightly different)

    ! start the timer
    subroutine start_timer ()

    ! stop timer and return CPU seconds as double
    real(kind=8) function stop_timer ()

    ! make an image of size nx*ny and return pointer to image in handle
    subroutine makeimage (handle, nx, ny)
      pointer ::  handle
      integer, intent(in) ::  nx, ny

    ! save image in handle to file fname
    subroutine saveimage (handle, fname)
      pointer :: handle
      character(len=*), intent(in) :: fname

    ! free mem associated with image
    subroutine freeimage (handle)
      pointer :: handle

    ! save image in handle to file fname
    subroutine setpixel (handle, i, j, rh, gs, bv)
      pointer :: handle
      integer, intent(in) ::  i, j
      real(4), value :: rh, gs, bv

These have to be linked with `timer_c.o`, `ppm_c.o`, `timing.o` and `ppm.o`.
The modules can be included as

    use timer_c
    use ppm_c

if the `utilities` directory figures as an include directory (`-I` 
compiler option). Here, the "c" in the module names reminds us that 
these modules are not pure F90 modules but just interfaces to C functions.

There is a little example program `test_f.f90` that shows how to use 
these functions in Fortran.

## `c/common` and `f/common`

Inside the C/C++ and Fortran source trees (`src/c` and `src/f`) we 
have `common` directories. These hold code to be used by other C/C++
and Fortran programs. On the C/C++ side we have (include `functions.h`)

    // logit function on [0,1] with poles "cut" at eps and 1-eps
    double logit(double x, double eps);
    // a scaled logit [0,1] -> [0,1]
    double scaled_logit(double x,  double eps);

For Fortran we have a `precision` module which provides "kinds" for
defining single, double and quad precision variables as `sp`, `dp` and
`qp` respectively. We also have a "variable precision" type as `vp`.
The idea here is base your programs on the `vp` kind and change
the definition of `vp` in the precision module to either single,
double or quad. Using `vp` you can change the precision of your
programs by just modifying one line in `precision.f90`. Your
Makefiles should include the file `precision.mod` as a
dependency.

The following functions are defined in `functions`

    function logit(x) result(y)
      real(kind=vp) :: x, y
    function scaled_logit(x) result(y)
      real(kind=vp) :: x, y

## Compiling a program

Before compiling a program for a given platform you have
to compile the codes in `src/c/common` and/or `src/f/common`
first, then in `src/utilities` and finally your code
So, for example when using Fortran, type

    cd src/f/common
    make
    cd src/utilities
    make
    cd src/f/program_to_compile       # substitute by actual name
    make
