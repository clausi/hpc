## How to select a CPU 

Example Xeon [Cascade Lake](https://www.microway.com/knowledge-center-articles/detailed-specifications-of-the-cascade-lake-sp-intel-xeon-processor-scalable-family-cpus/)

![Peak performance](img/cascade_lake_peak_performance.png)

![Cost per GFLOP](img/cascade_lake_cost_per_gflop.png)

### Example for an entry level server:

* Intel® Xeon® Gold 6226 12 cores - 1776.00$
* 1U Server + memory (96Gb) + 1HDD + 2 CPU  - 6600€ (+VAT)

