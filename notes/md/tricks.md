##Some handy commands

__Short info about make__
    [web page](http://berrendorf.inf.h-brs.de/sonstiges/make.html)

__Obtain repository__
    
    git clone https://bitbucket.org/clausi/hpc

__Obtain repository without git__
    # get archive from bitbucket
    wget https://bitbucket.org/clausi/hpc/get/master.zip

    # copy local file to remote
    scp master.zip denk@207.108.8.131:master.zip

__Cygwin: show windows path__

    cygpath -d /home

__Create assembler code__

    gcc -S fichero.c
    gcc -c -g -Wa,-a,-ad [opciones] fichero.c > fichero.lst
    [Reference](http://x86.renejeschke.de/)

__Vectorization info on gcc 5.2__

    gcc -O2 -ftree-vectorize -ffast-math -fopt-info-vec-missed ...

__See processors threads are running on__

    #!bash
    for i in $(pgrep myapp); do ps -mo pid,tid,fname,user,psr -p $i;done

__Set affinity for threads (gcc)__


    #!bash
    export  GOMP_CPU_AFFINITY="0-7 16-23 8-15 24-31"

__Show topology of CPUs__

    #!bash
    hwloc-ls --no-io
