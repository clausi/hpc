### Intel [Xeon 6126 Skylake](https://ark.intel.com/content/www/us/en/ark/products/120483/intel-xeon-gold-6126-processor-19-25m-cache-2-60-ghz.html)

    CPU                 | Intel(R) Xeon(R) Gold 6126 CPU @ 2.60GHz
    Cores               | 12
    Threads             | 24
    L3 cache            | 19.25 MB 
    flopstriadn         | 1 thread 50 GFlops, 12 threads 854 GFlops/s double

* [Wiki Skylake](https://en.wikipedia.org/wiki/Skylake_microarchitecture)

### Intel [Xeon Phi 7230](https://ark.intel.com/content/www/us/en/ark/products/94034/intel-xeon-phi-processor-7230-16gb-1-30-ghz-64-core.html)

    CPU                 |
    Cores               | 64
    L2                  | 32 MB

### Notebook Tecra Z50 [i7-4600U](http://ark.intel.com/products/76616/Intel-Core-i7-4600U-Processor-4M-Cache-up-to-3_30-GHz)

    CPU                 |  Core i7-4600U (Haswell, AVX2)   
    Cores               |  2
    Threads             |  4
    CPU-Freq            |  2.1 GHZ - 3.3GHz
    RAM                 |  8GB
    Mem-BW              |  25,6GB/s
    L1                  |  32KB
    L2                  |  256KB
    L3                  |  4MB
    DP FLOPS/cycle      |  16 (two 4-wide FMA3)
    SP FLOPS/cycle      |  32 (two 8-wide FMA3)
   
    stream1             |  8.9 GB/s
    streamn             |  8.8 GB/s
    flopstriad1         |  16.4 / 32.8 GFlops/s
    flopstriadn         |  26.6 / 53.8 GFlops/s

### Instance t2.micro on AWS

    CPU                 | Intel(R) Xeon(R) CPU E5-2676 v3 @ 2.40GHz
    Cores               | 1
    Threads             | 1
    Cpu GHz             | 2.4
    cache size          | 30720 KB
    RAM                 | 1 GB
    Bench:stream1       | 8.9 GB/s
    Bench:flopstriad1   | 24.02 GFlops/s


