## Theoretical peak flops/cycle various instruction sets

From [stackoverflow](http://stackoverflow.com/questions/15655835/flops-per-cycle-for-sandy-bridge-and-haswell-sse2-avx-avx2)

For Haswell, <a href="pdf/Theoretical_Peak_FLOPS_per_instruction_s.pdf" target="_blank">see also</a>

Intel Core 2 and Nehalem:

* 4 DP FLOPs/cycle: 2-wide SSE2 addition + 2-wide SSE2 multiplication
* 8 SP FLOPs/cycle: 4-wide SSE addition + 4-wide SSE multiplication

Intel Sandy Bridge/Ivy Bridge:

* 8 DP FLOPs/cycle: 4-wide AVX addition + 4-wide AVX multiplication
* 16 SP FLOPs/cycle: 8-wide AVX addition + 8-wide AVX multiplication

Intel Haswell/Broadwell:

* 16 DP FLOPs/cycle: two 4-wide FMA (fused multiply-add) instructions
* 32 SP FLOPs/cycle: two 8-wide FMA (fused multiply-add) instructions

Intel Skylake/Cascade Lake with 1 FMA units: some Xeon Bronze/Silver

* 16 DP FLOPs/cycle: one 8-wide FMA (fused multiply-add) instruction
* 32 SP FLOPs/cycle: one 16-wide FMA (fused multiply-add) instruction

Intel Skylake/Cascade Lake with 2 FMA units: Xeon Gold/Platinum, i7/i9 high-end

* 32 DP FLOPs/cycle: two 8-wide FMA (fused multiply-add) instructions
* 64 SP FLOPs/cycle: two 16-wide FMA (fused multiply-add) instructions

AMD Ryzen

* 8 DP FLOPs/cycle: 4-wide FMA
* 16 SP FLOPs/cycle: 8-wide FMA

Intel Xeon Phi (Knights Landing), per core:

* 32 DP FLOPs/cycle: two 8-wide FMA every cycle
* 64 SP FLOPs/cycle: two 16-wide FMA every cycle
