## Literature

* Jim Jeffers, Intel Xeon Phi Coprocessor High-Performance Programming [Amazon](http://www.amazon.es/Intel-Xeon-Coprocessor-High-Performance-Programming/dp/0124104142)
* Same as above, Knights Landing Edition [Amazon](https://www.amazon.com/Intel-Xeon-Processor-Performance-Programming/dp/0128091940/)