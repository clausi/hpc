## Moore 's law

Paper 1965: [Electronics, pp. 114–117, April 19, 1965](http://www.cs.utexas.edu/~fussell/courses/cs352h/papers/moore.pdf) 

__Synopsis__: The number of components (on a 
single wafer) determinates the cost/component. For a given technology
in the production process (state of the art) there is a minimum of this
cost for a given component density. The component density where
this minimum is located increases exponentially with time. So cost
drives integration. The initial 
prediction was for the component density to double each 
year (extrapolation 1965-1975)

Later revised to x 2 each two years 

Performance doubles every 18 months.

2015 Moor's law celebrated it's 50th birthday with remarkable success:

![Moor's Law](img/Moores_Law_small.png)

__But ...__


[2005 - The Free Lunch Is Over](http://www.gotw.ca/publications/concurrency-ddj.htm)
![2005 - Free Lunch is over](img/free_lunch_small.png)

>    Applications will increasingly need to be concurrent if they want 
>    to fully exploit continuing exponential CPU throughput gains
>
>    Efficiency and performance optimization will get more, not less, important

Reads about Memory Wall, Power Wall, ILP (Instruction Level Parallelism) wall:

* [Dennard scaling](https://en.wikipedia.org/wiki/Dennard_scaling)
* [The future of computers](http://www.edn.com/design/systems-design/4368705/The-future-of-computers--Part-1-Multicore-and-the-Memory-Wall)
* [Post-Dennard Scaling and the final Years of Moore's Law](https://www.hs-augsburg.de/Binaries/Binary20963/PostDennard.pdf)
* [1994 - Memory wall](http://www.di.unisa.it/~vitsca/SC-2011/DesignPrinciplesMulticoreProcessors/Wulf1995.pdf)

## Top 500

Ranking of the fastest (Linpack-fastest) supercomputers in the world: 
[http://www.top500.org/](http://www.top500.org/)

__June 2015__

![Top10](img/top500_june2015.png)

__June 2019__

![Top10](img/top500_june2019.png)

![Performance](img/top500_perform2019.png)

__Cores vs Rank__

![Cores vs Rank](img/cores_vs_rank2019.png)

__Summit__

![Summit](img/summit.png)

[Tom's hardware](https://www.tomshardware.com/news/us-supercomputer-china-top500-summit,37367.html)

__España__

![MareNostrum](img/marenostrum.jpg)

* [Nº 29, MareNostrum](https://www.bsc.es/marenostrum/marenostrum)
* [PRACE](http://www.prace-ri.eu/)

## What will we probably see the next years?

__Supercomputer "arms race":__

* April 2015: USA blocks export of Xeon Phi technology to China [PC-World](http://www.pcworld.com/article/2908692/us-blocks-intel-from-selling-xeon-chips-to-chinese-supercomputer-projects.html)
* 15 th July 2015: China announces [homegrown DSP](http://www.nextplatform.com/2015/07/15/china-intercepts-u-s-intel-restrictions-with-homegrown-supercomputer-chips/) as component in Thianhe-2A
* 29 th July 2015: Obama issues Executive Order establishing the National 
  Strategic Computing Initiative (NSCI) 
  [White House](https://www.whitehouse.gov/blog/2015/07/29/advancing-us-leadership-high-performance-computing).
  Next decade: exaflops and exabytes (numerical análisis meets big data)
* June 2016 [New chinese chip in #1 computer](http://www.nextplatform.com/2016/06/20/look-inside-chinas-chart-topping-new-supercomputer/)
* January 2017 [Trump Administration Preps Plan for Huge Cuts to Department of Energy](https://www.top500.org/news/trump-administration-preps-plan-for-huge-cuts-to-department-of-energy/)
* March 2017 [EU Ratchets up the Race to Exascale Computing](https://www.hpcwire.com/2017/03/29/eu-ratchets-race-exascale-computing/)
* June 2018 [USA recovers #1 on TOP 500](https://www.top500.org/lists/2018/06/)
* November 2018 [EuroHPC](https://eurohpc-ju.europa.eu/)
* March 2019 [Trump’s 2020 Budget](https://www.hpcwire.com/2019/03/12/quick-take-trumps-2020-budget-spares-doe-funded-hpc-but-slams-nsf-and-nih/)
* May 2019 [Plans for Tianhe 3](https://www.nextplatform.com/2019/05/02/china-fleshes-out-exascale-design-for-tianhe-3/)
* 2021-2022 Exascale

__Technological developments:__

* CPU + GPU or specialized hardware [FPGA](https://insidehpc.com/2019/07/fpgas-and-the-road-to-reprogrammable-hpc/). Higher level of integration.
* [Quantum computing?](https://ec.europa.eu/digital-single-market/en/quantum-technologies)

__Computing Infrastructures:__

* Computing clouds
  * AWS - [Amazon Web Services](https://aws.amazon.com/?nc1=h_ls), Google, Azure
  * [Science foundation clouds](http://www.cloudlab.us/index.php)
