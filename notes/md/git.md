## Some Git commands

__Checkout this repository__

    cd <workspace>  
    git clone https://clausi@bitbucket.org/clausi/hpc

__Add file to index__

    git add <filename>

__Rename__

    git mv <old name> <new name>

__Commit to head__

    git commit -m "new file"

__Undo changes, get latest local head version__

    git checkout -- <filename>

__Upload to repository__

    git push origin master

__Update/merge from repository__

    git pull

__Drop all local changes and get again from server__

    git fetch origin
    git reset --hard origin/master

__Get URL of remote__

    git config --get remote.origin.url

__Set URL of remote__

    git remote set-url origin URL_HERE

__Links__

* [git - the simple guide](http://rogerdudler.github.io/git-guide/)
* [Everydays git](https://www.kernel.org/pub/software/scm/git/docs/giteveryday.html)
