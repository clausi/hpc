# Introduction

In this introduction we will first see some applications of
HPC (High Performace Computing), definition from [https://www.usgs.gov/](https://www.usgs.gov/core-science-systems/sas/arc/about/what-high-performance-computing)

> High Performance Computing most generally refers to the practice of
> aggregating computing power in a way that delivers much higher
> performance than one could get out of a typical desktop computer
> or workstation in order to solve large problems in science,
> engineering, or business

The demand for machine learning applications (deep learning) in the
last decade has shaped the hardware currently existing in HPC
platforms (GPUs as accelerators). We will show some examples
of deep learning and why it has become so important recently.

Coming back to the above definition of HPC, 20 years ago a
"typical desktop computer" was a relatively simple
machine, no special considerations were necessary for
running your code. Due to faster and faster CPUs, your
program would run faster every year. We will see why this
is not the case anymore without further effort.

Nowadays, a desktop computer is a parallel machine with various
"cores" and equipped with special instruction sets (AVX etc)
which requires the knowledge of special programming techniques to
make efficient usage of the hardware.
