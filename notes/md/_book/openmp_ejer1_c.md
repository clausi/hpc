### Exercise OpenMP - Mandelbrot

Source in `hpc/src/openmp/mandelbrot`

* `make` the program `mandelbrot`. Understand program flow. 
  Use `display` to show the image. Write down how long it takes.
* There is a copy of the original program as `mandelomp.c`. This
  program does not compile at the moment.
* In `mandelomp.c`, substitute calls to `start_timer()` and `stop_timer()` by
  calls to `omp_get_wtime()`. 
  Compile. Have you added the include file `omp.h`? Use gcc 4.8 for the
  time beeing.
* Set `export OMP_NUM_THREADS=1` and run.
* Parallelize `mandelomp.c` with a simple `parallel for`. Run with 1, 2 and 4
  threads. Is the image the same?
* Modify the code in order to have a parallel section and a for worksharing
  construct. Is the image fine? Include a statement that tells us on how
  many threads we are running.
* Measure the scaling behaviour. (You
  can use `run.sh`. The program `gnuplot` is installed at `democrito`. )
* Improve scaling behaviour by using `dynamic` scheduling
* How to further improve performance (some platforms, some compliers)?
