## A "new" kid on the block - Machine learning

### Deep learning

![Neural network](img/neural_network.png)

* Clarifai [demo](https://www.clarifai.com/demo)
Example images:
  * [Vacations](https://tecreview.tec.mx/wp-content/uploads/2018/07/VACACIONES.jpg)
  * [Faces](https://iop.harvard.edu/sites/default/files/styles/paragraph-full-width/public/sources/paragraph/full/NEMC%20GROUP%20%281200X500%29.png), 
  * [Serenegeti](https://cdn.images.express.co.uk/img/dynamic/25/590x/safari-park-africa-safari-holidays-safari-holidays-africa-frican-safari-packages-kenya-safari-the-serengeti-974454.jpg?r=1533698425422)

* [Imagenet](http://www.image-net.org/) Challenges

![Imagenet](img/imagenet_accuracy.jpg)
  
* Learning about Neural Networks and Deep Learning
    * [Coursera - Andrew Ng](https://www.coursera.org/learn/machine-learning)
    * [Machine learning for physicists](https://machine-learning-for-physicists.org/)
    * [Online book - Michael Nielsen](http://neuralnetworksanddeeplearning.com)
    * [Online book - Ian Goodfellow](http://www.deeplearningbook.org/)

* "Data driven" world (internet, IoT)
![performance gap](img/performance_gap_ai.png)

* [Human centered AI](https://hai.stanford.edu/) and an [interesting discussion](https://www.wired.com/story/will-artificial-intelligence-enhance-hack-humanity/amp?__twitter_impression=true&fbclid=IwAR1ZwyC5g6zxRvGxOP2exqJrqRKkjv46ugiQarNHcthXYlIsVGN-wgHsICo)

* [Machine learning and the physical sciences](https://arxiv.org/abs/1903.10563)
