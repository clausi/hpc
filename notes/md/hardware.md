# Hardware

## Von Neumann architecture

See [Wikipedia](https://en.wikipedia.org/wiki/Von_Neumann_architecture) (mention Turing)

![Von_Neumann](img/500px-Von_Neumann_Architecture.svg.png)

## Knights Landing (Phi)

![Knights Landing](img/knl-die-organization.jpg)

![KNL core](img/KNL-core-internal.jpg)

* [Best practice guide](http://www.prace-ri.eu/best-practice-guide-knights-landing-january-2017/)

## <a href="https://en.wikichip.org/wiki/intel/microarchitectures/skylake_(server)">Skylake (Xeon)</a>

![Skylake](img/600px-skylake_sp_xcc_die_config.png)

![1 core](img/1425px-skylake_server_block_diagram.svg.png)

# Info about our hardware

## Windows:

* Windows: Start -> Right-click on "Computer" -> Properties
* Program [cpu-z](http://www.cpuid.com/softwares/cpu-z.html)

![Win System](img/sistema_tecra_z50_small.png)

![CPU-Z](img/cpuz_tecra_z50.png)

## Linux: 

* `cat /proc/cpuinfo`
* `hwloc-ls` or `lstopo`
* `numactl --hardware`

Example:

    processor       : 3
    vendor_id       : GenuineIntel
    cpu family      : 6
    model           : 69
    model name      : Intel(R) Core(TM) i7-4600U CPU @ 2.10GHz
    stepping        : 1
    cpu MHz         : 2693.000
    cache size      : 4096 KB
    physical id     : 0
    siblings        : 4
    core id         : 1
    cpu cores       : 2
    .... more info ....

![lstopo](img/lstopo_tecra_z50.png)
