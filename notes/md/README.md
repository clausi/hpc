# Introductory course to HPC

## Preface

These notes are for an introductory course to HPC (High Performance Computing)
that I have held at various faculties at the University of Seville and more
recently also at the University of Huelva. 

The course centers on the
following fundamental "ingredients" for effcient programs on modern
architectures:

* know your hardware
* parallel constructs for shared memory (OMP)
* vectorization (SIMD)
* parallel programming on distributed memory (MPI)

The concepts are first discussed in class and then put into practice,
implementing small programs that show the most important points with
simple and understandable code.

These are by no means complete lecture notes, all subjects are explained
with more details in class. I hope these notes can serve as an overview
for the course and later on as a resource for the presented contents.

All example programs as well as these course notes are available at
[https://bitbucket.org/clausi/hpc](https://bitbucket.org/clausi/hpc).
A recent version of the notes is online at 
[https://hpc.androbi.com](http://hpc.androbi.com). This is a living
document, expect it to change frequently.

C. Denk, November 2019
