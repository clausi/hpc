## Vim commands

__Most basic commands__

    Start insert mode     | i
    End insert mode       | ESC-key
    Quit without saving   | :q!
    Write and quit        | :wq

__Select and Indent__

    :set shiftwidth=4
    V(cursor down)>

__Substitute__
    :%s/foo/bar/g
