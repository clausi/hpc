# Distribute work - Fortran

Note: The example files for this section can be found in `src/f/openmp/simple_examples`

## A parallel "do" loop

    program pardo1
        use omp_lib
        implicit none
        integer, parameter:: nmax = 9
        integer :: i, ithread
        real :: a(nmax), b(nmax)
    !
        write(*,*) 'Initialization'
        do i = 1, nmax
            a(i) = i
            b(i) = 0
            print '(" a(",i2,") = ",e10.3)', i, a(i)
        enddo
    !
        write(*,*) 'Calculation'
        !$omp parallel private(ithread)
          ithread = omp_get_thread_num()
          print '(" thread:",i2)', ithread
          !$omp do schedule(static,2)
            do i = 2, nmax
                b(i) = 0.5*(a(i-1) + a(i))
                print '(" thread: ",i2," b(",i2,") = ",e10.3)', ithread, i, b(i)
            enddo
          !$omp end do
        !$omp end parallel
    !
        write(*,*) 'Results'
        do i = 2, nmax
            print '(" b(",i2,") = ",e10.3)', i, b(i)
        enddo
    end program pardo1

The `!$omp parallel` directive creates threads, the `!$omp do`
directive distributes the work inside the loop among the
incoming threads.

![Parallel Do](img/parallel_and_do.png)

**Exercise:**

* compile and run with 4 threads
* check the OpenMP Reference guides and try different `schedule` methods

### A shorter version

`!$omp parallel do` - Shortcut for specifying a parallel
construct containing one or more associated loops
and no other statements

The parallel section of the program `pardo1` can be rewritten as compact as

    !$omp parallel do
    do i = 2, nmax
        b(i) = 0.5*(a(i-1) + a(i))
        print '(" thread: ",i2," b(",i2,") = ",e10.3)', omp_get_thread_num(), i, b(i)
    enddo

See program `pardo2.f90`. This is probably the most used OpenMP directive.
All *clauses* from `!$omp parallel` and `!$omp do` can be applied.

## What if work doesn't come in loops

*Sections* have been the traditional way to deal with this problem in OpenMP.
A *section* defines a task to be executed once. Various section directives
are wrapped in a *sections* block. If a team of threads hits such a sections
block, the sections are distributed between the threads.

    program parsections
        use omp_lib
        use work
        implicit none
        !$omp parallel
          print '(" thread: ",i2)', omp_get_thread_num()
          !$omp barrier

          !$omp sections
            !$omp section
              call work1
            !$omp section
              call work2
            !$omp section
              call work3
          !$omp end sections
        !$omp end parallel
    end program parsections

Note: the `!$omp barrier` is not necessary here, I only included it here
to make the output of the program less mixed up. See section below on
synchronization for more about barriers.

This is a different programming model. Each task has it's own code, whereas in the *parallel do* the **same** code was used (the code inside the loop) for the different "tasks". This is sometimes also called task-parallel vs. data-parallel.

With OpenMP 3.0 *tasks* were introduced. The same program as above can be
expressed with tasks as

    program partasks
        use omp_lib
        use work
        implicit none
        !$omp parallel
          !$omp single
            print '(" single thread: ",i2)', omp_get_thread_num()
            !$omp task
              call work1
            !$omp end task
            !$omp task
              call work2
            !$omp end task
            !$omp task
              call work3
            !$omp end task
          !$omp end single
        !$omp end parallel
    end program partasks

A task defines some work to be done. Within a parallel region,
a single thread (`!$omp single`) hits the task definitions
and "pushes" the tasks to a task queue. At certain program
states (when a task has been pushed, when a task has
been terminated and at barriers) these tasks will be pulled and
executed by the available threads in the team of threads
created by `!$omp parallel`.

One thread *produces*, all threads *consume*. This paradigm
is easier to implement (and usually more efficient) then sections in
recursive functions and dynamic loops.

## Dynamic loops

Compiling code like

    !$omp parallel do
    do while(a(i) .ge. 0)
        b(i) = a(i)**2
    enddo

will result in an error

        do while(a(i) .ge. 0)
                            1
    Error: !$OMP PARALLEL DO cannot be a DO WHILE or DO without loop control at (1)

Employing *tasks* we can write this loop as

    i = 1
    !$omp parallel
      !$omp single
        do while (a(i) .ge. 0)
            !$omp task
              b(i) = a(i)**2
              print '(" thread: ",i2," b(",i2,") = ",e10.3)', omp_get_thread_num(), i, b(i)
            !$omp end task
            
            !$omp taskwait
            print '(" increment on thread: ",i2)', omp_get_thread_num()
            i = i + 1
        enddo
      !$omp end single
    !$omp end parallel

Check here for <a href="pdf/10. OMP tasks.pdf"  target="_blank">
more details about tasks</a>
