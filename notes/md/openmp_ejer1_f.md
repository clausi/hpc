### Exercise OpenMP - Mandelbrot

Source in `hpc/src/f/openmp/mandelbrot`

* `make` the program `mandelbrot`. Initially there are two identical serial
  versions (`mandel0.f90` and `mandel1.f90`). Execute `mandel0` and write
  down how long it takes to run.  Use `eog mandel.ppm` to show the
  image.
* In `mainmandel.f90`, substitute calls to `start_timer()` and `stop_timer()` by
  calls to `omp_get_wtime()` and compile and run again. The results should
  be consistent with the previous version.
* Parallelize `mandel1.f90` with a simple `parallel do`. Run with 1, 2 and 4
  threads. Is the image the same? If not, what could be the reason? Fix the
  problem.
* Modify the code in order to have a parallel section and a for worksharing
  construct. Is the image fine? Include a statement that tells us on how
  many threads we are running.
* Measure the scaling behaviour. You
  can use the shell script `run.sh` 
* Improve scaling behaviour by using `dynamic` scheduling

### Results

![Scaling Mandelbrot](img/scaling_mandel.png)