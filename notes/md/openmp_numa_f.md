# NUMA aspects - Fortran

Up to this point we have not used memory accesses intensively. Most HPC
computations take place on "big" objects in memory, in order to do
calculations we have to get the data from memory and write back the
results to memory.

Many problems are limited by memory bandwidth, i.e. the machine takes
longer in reading/writing data from/to memory than for doing the
computations.

## Stream benchmark

See [STREAM webpage](https://www.cs.virginia.edu/stream/). We have
a version of stream in directory `src/bench/openmp`

4 Functions tested

    !COPY
    !$OMP PARALLEL DO
          DO 30 j = 1,n
            c(j) = a(j)
      30  CONTINUE

    !SCALE     
    !$OMP PARALLEL DO
          DO 40 j = 1,n
            b(j) = scalar*c(j)
      40  CONTINUE

    !ADD
    !$OMP PARALLEL DO
          DO 50 j = 1,n
            c(j) = a(j) + b(j)
      50  CONTINUE

    !TRIAD
    !$OMP PARALLEL DO
          DO 60 j = 1,n
            a(j) = b(j) + scalar*c(j)
      60  CONTINUE

Results on `login0` node (C/C++ version of stream)

    export OMP_NUM_THREADS=1
    Function    Best Rate MB/s  Avg time     Min time     Max time
    Copy:           11180.7     0.139522     0.114483     0.187811
    Scale:          11690.0     0.136465     0.109495     0.190446
    Add:            12971.5     0.179343     0.148017     0.214113
    Triad:          13013.7     0.172281     0.147537     0.207660

Scaling behaviour (Triad function):

    Threads   Best Rate MB/s
     1          13013.7
     2          26703.7
     4          50360.6
     8          87444.0
    12         114887.8
    24         140444.1

Now let's do our own version of the stream benchmark.

## Own stream benchmark, version 1

See directory `src/f/openmp/stream/stream1.f90`

    print '("initializing ..")'
    do i = 1, NMAX
      a(i) = 1.0
      b(i) = 2.0
      c(i) = 0.0
    end do

    !$omp parallel
    
      !$omp master
        nt =  omp_get_num_threads();
        print '("running on ",i3," threads")', nt
      !$omp end master

      !$omp single
        secs=omp_get_wtime();     
      !$omp end single

      !$omp do
      do i = 1, NMAX
        c(i) = a(i) + scal * b(i);
      end do

      !$omp single
        secs = omp_get_wtime() - secs;
      !$omp end single

    !$omp end parallel


**Exercise:**

* Read code and compile `stream1`
* Run with 1,2,4,8,12,24 threads. Does the result with 1 thread give the
  same result as the stream benchmark? Does it scale equally?

##### First touch placement

![Numa1](img/numa_first_touch1.png)

![Numa2](img/numa_first_touch2.png)

##### Own stream benchmark, version 2


    !$omp parallel
    
      !$omp master
        nt =  omp_get_num_threads();
        print '("running on ",i3," threads")', nt
      !$omp end master
      
      !$omp master
      print '("initializing ..")'
      !$omp end master

      !$omp do
      do i = 1, NMAX
        a(i) = 1.0
        b(i) = 2.0
        c(i) = 0.0
      end do

      !$omp master
      secs=omp_get_wtime();     
      !$omp end master

      !$omp do
      do i = 1, NMAX
        c(i) = a(i) + scal * b(i);
      end do

      !$omp master
      secs = omp_get_wtime() - secs;
      !$omp end master

    !$omp end parallel

**Exercise:**

* Compile `stream2` and check if it scales better now.

How is the memory layout of our cluster nodes? Remember the command `lstopo` ?

![lstopo](img/lstopo.png)

How to control "CPU affinity" of threads?

    export OMP_DISPLAY_ENV=true      # show environment when executing program
    export OMP_DISPLAY_AFFINITY=true # show thread affinity

    export OMP_PLACES=sockets      # t0 -> s0, t1 -> s1, t2 -> s0, etc
                                   # OMP_PLACES = '{0:12},{12:12}'

    export OMP_PLACES=cores        # t0 -> s0,c0, t1 -> s0,c1, t2 -> s0,c2
    export OMP_PROC_BIND=close     # OMP_PLACES = '{0},{1},..,{22},{23}'


With the Intel compiler you can use 

    export KMP_AFFINITY=verbose,balanced

More details here:

* [icc 19 - thread affintiy](https://software.intel.com/en-us/cpp-compiler-developer-guide-and-reference-thread-affinity-interface-linux-and-windows)
* [Thread affinity on Intel PHI](https://software.intel.com/en-us/articles/process-and-thread-affinity-for-intel-xeon-phi-processors-x200)
