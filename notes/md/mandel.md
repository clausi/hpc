### Example - [Mandelbrot set](https://en.wikipedia.org/wiki/Mandelbrot_set)

![Mandel](img/zoom.gif)

Source code in `hpc/src/f/mandel`

We start with a standard implementation to calculate the
set.

      dx = (x2-x1)/nx
      dy = (y2-y1)/ny
      do j = 0, ny-1 
        do i = 0, nx-1
            c = cmplx(x1 + dx * i, y1 + dy * j, kind=vp)
            z = cmplx(0, 0, kind=vp)
            ncnt = 0
            do while ((ncnt < maxiters) .and. (real(z)**2+aimag(z)**2 < 4.0))
                z = z*z + c
                ncnt = ncnt + 1
            enddo 
            hue = ..
            val = ..
            call setpixel (img, i, j, hue, sat, val)
            flops = flops + 4 + ncnt*8
        enddo
      enddo

The innermost loop is purely scalar, we will see if some vectorization
can be applied here.

The program uses some simple functions to save a picture of the
set in a ppm file (see directory `hpc/src/utilities`). You can
use `display` to view the image on the cluster, if you run
a local X server.

We will now try to apply a series of modifications to the original
code and meassure the effect these changes have on performance.

* mandel0 - version with complex. This is our baseline.
* mandel1 - work with real and imaginary parts of complex
* mandel2 - iterate vectors of points in the complex plane
* mandel3 - Ninja version

We have obtained the following results on one core of
the cluster at the University of Huelva

ifort/icc:

    mandel0 secs =  0.519E+01 Flops =  0.187E+10
    mandel1 secs =  0.437E+01 Flops =  0.221E+10
    mandel2 secs =  0.203E+01 Flops =  0.483E+10
    mandel3 secs =  0.950E+00 Flops =  0.105E+11 

This corresponds to a speedup of 5.5 on one core. Finally
the optimized 1-core version of `mandel3` is parallelized 
with OpenMP threads leading to

    nthreads = 1  secs =  0.950E+00 Flops =  0.105E+11
    nthreads = 2  secs =  0.587E+00 Flops =  0.170E+11
    nthreads = 4  secs =  0.363E+00 Flops =  0.274E+11
    nthreads = 8  secs =  0.244E+00 Flops =  0.409E+11
    nthreads = 12 secs =  0.184E+00 Flops =  0.540E+11
    nthreads = 24 secs =  0.116E+00 Flops =  0.862E+11

This corresponds to a speedup of 8.2 compared to the optimized
1-core version. The total speedup obtained on one node is approx.
45 with respect to the serial version.
