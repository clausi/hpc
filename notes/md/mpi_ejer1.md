### Exercise MPI - Stencil code


Exercise: Goto `src/mpi/exercise`. Program a MPI version of stencil3d.

Note: This is not an easy exercise. `stencil3d` is a three dimensional
[stencil code](https://en.wikipedia.org/wiki/Stencil_code). Stencil codes
are usually memory bound which makes them demanding to parallelize.

The main problem for this exercise is the domain decomposition. In class
we have recommended _cutting_ the 3D cube only in one direction, in a 
way that contiguous data in the array can be sent between processes.

##### Some hints

2D problem: 8x8 cube and boundary values, 4 processes. Map needed 
cells from neighbours
to "shadow" regions in process (yellow cells from process 1 and 3 are 
shadow regions in process 2):


![domain1](img/domain1.png)

We have added 2 columns to each process, these are used as _shadow_ regions 
or for holding the boundary values. We also indicate how data has to be 
sent in between processes: 


![domain2](img/domain2.png)

For simplicity, in the 3D case, we want to _cut_ the 3D cube in a way that 
cells can be sent as a 
one dimensional array to the shadow regions of the neighbors:

![domain3](img/domain3.png)

