## Programming for shared memory with OpenMP (Open Multi-Processing)

* OpenMP [Website](http://openmp.org/)
* Our [OpenMP essentials](openmp_essentials.md)
* <a href="pdf/OpenMP-4.5-1115-CPP-web.pdf" target="_blank">Reference Guide C/C++ ver. 4.5</a>
* <a href="pdf/OpenMP-4.5-1115-F-web.pdf" target="_blank">Reference Guide Fortran ver. 4.5</a>
* <a href="pdf/openmp-4.5.pdf" target="_blank">OpenMP 4.5 Complete Specification</a>

## Threads in OpenMP

A *process* (program) can create new *threads* (that execute code)
which share the address space with the creating process. These threads
can potentially execute on different cores of the CPU, thus enabling
parallel execution on a CPU.

![Threads](img/thread_model.png)

## Our first OpenMP program


    program hello_omp
        !$ use omp_lib
        integer :: ithread = 0
        logical :: use_openmp = .false.
        !$ use_openmp = .true.
        if (use_openmp) then
            print '("omp version")'
        else 
            print '("serial version")'
        end if

        !$ ithread = omp_get_thread_num()
        print '("start on thread  ",i2)', ithread
        
        !$omp parallel private(ithread)
          !$ ithread = omp_get_thread_num()
          print '(" running on thread",i2)', ithread
        !$omp end parallel

        !$ ithread = omp_get_thread_num()
        print '("finish on thread ",i2)', ithread
    end program hello_omp

**Exercise:**

* Compile with `gfortran hello_omp.f90` and run. `! ...` are comments in
  Fortran, so this is just a normal serial program
* Compile with `gfortran -fopenmp hellp_omp.f90` and run. If only one
  thread is created in the parallel section, try `export OMP_NUM_THREADS 4`
* Compile and run on the cluster:
  * on `login0`: `./hello_omp`. How many threads are created? Use the 
    environment to reduce the number of threads to 2.
  * run with slurm: `srun -n 1 --cpus-per-task=4 ./hello_omp`
* Are the thread numbers printed in order? Why?

## Amdahls law

The theoretical speedup is determined by the fraction of
the code that is serial

    T[n]=T[1]*(B + 1/n (1-B)), B=fraction of serial code

![Amdahls law](img/AmdahlsLaw.png)

In the following sections we will learn some of the OpenMP directives by playing
around with simple code examples. Please choose the C/C++ or Fortran version:

* [OpenMP C/C++](openmp_c.md)
* [OpenMP Fortran](openmp_f.md)
