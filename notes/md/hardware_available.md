# Currently available hardware

The following is a _selection_ of hardware for HPC available
at this moment. I have been updating this section since I first
gave this course in 2015, so we can follow the latest technical
developments.

## CPUs

### Intel Xeon processors

Intel [Xeon processors](https://ark.intel.com/content/www/us/en/ark.html#@PanelLabel595)

* 2015, see [Haswell](https://www.microway.com/knowledge-center-articles/detailed-specifications-intel-xeon-e5-2600v3-haswell-ep-processors/), up to 16(18) cores and 600 GFLOPS
* 2016, see [Broadwell](https://www.microway.com/knowledge-center-articles/detailed-specifications-of-the-intel-xeon-e5-2600v4-broadwell-ep-processors/), up to 20(22) cores and 800 GFLOPS
* 2018, see [Skylake-SP](https://www.microway.com/knowledge-center-articles/detailed-specifications-of-the-skylake-sp-intel-xeon-processor-scalable-family-cpus/), up to 22(24/28) cores and 1400 GFLOPS
* 2019, see [Cascade Lake SP](https://www.microway.com/knowledge-center-articles/detailed-specifications-of-the-cascade-lake-sp-intel-xeon-processor-scalable-family-cpus/), up to 20 (28) cores and 1600 GFLOOPS

### AMD EPYC

* 2019, AMD EPYC Rome, see [Tom's hardware](https://www.tomshardware.com/news/amd-epyc-rome-7000-series-data-center-processor-zen-2-7nm,40108.html) and [Phoronix](https://www.phoronix.com/scan.php?page=article&item=amd-linux-epyc-7002&num=1)
* 64 cores [AMD EPYC 7742](https://www.phoronix.com/scan.php?page=article&item=amd-epyc-7502-7742&num=1)
* Benchmarks [AMD EPYC 7742 vs Xeon Platinum 8280](https://www.phoronix.com/scan.php?page=article&item=epyc-rome-mitigations&num=1)
* Exascale [Frontier](https://www.amd.com/en/products/frontier)

### POWER9

* [IBM POWER9](https://en.wikipedia.org/wiki/POWER9)
* POWER9 & ARM vs X86 [performance](https://www.phoronix.com/scan.php?page=article&item=rome-power9-arm&num=1)
* [High bandwidth](https://www.nextplatform.com/2019/10/01/ibm-readies-power9-bandwidth-beast-kicker/)
* [Open specifications](https://openpowerfoundation.org/)

### ARM

* Experimental for HPC [at this stage](https://www.nextplatform.com/2019/10/01/arms-chances-in-servers-may-hinge-on-success-in-hpc/)

## GPUs

### NVIDIA Tesla 2017/18

![NVIDIA V100](img/nvidia-tesla-v100-tensor-core-gpu-500x500.jpg)

* 7.8 TFlops double precision, see [Tesla "Volta" GPU](https://www.microway.com/knowledge-center-articles/in-depth-comparison-of-nvidia-tesla-volta-gpu-accelerators/)
* "Datacenter" Tesla V100, 10k$
* [Example system](https://www.servethehome.com/inspur-systems-nf5468m5-review-4u-8x-gpu-server/)

### NVIDIA Turing

* Not for double precision, see [Turing benchmarks](https://www.microway.com/hpc-tech-tips/nvidia-turing-tesla-t4-hpc-performance-benchmarks/)

### AMD

![AMD Radeon](img/instinct.jpg)

*[AMD accelerators](https://www.amd.com/en/products/professional-graphics/instinct-mi50) Up to 6.6 TFLOPS Double Precision for HPC

## Other accelerators

### Intel Xeon Phi

The Intel Xeon Phi Line has been discontinued

* [Knights Landing discontinued](https://www.hpcwire.com/2018/07/25/end-of-the-road-for-knights-landing-phi/)
* [Knights Mill retired](https://www.tomshardware.co.uk/intel-knights-mill-xeon-phi-retire,news-60592.html)

### Intel Xeon Phi processors (2016)

![Knights Landing](img/knights_landing.jpg)

* Knights Landing: [coprocessors & processors](http://www.nextplatform.com/2016/06/20/intel-knights-landing-yields-big-bang-buck-jump/)
* AVX512
* 3,5 Tflops, 6200$ (model 7290)

### Intel's Nervana

* [Intel Nervana Neural Network Processors (NNP)](https://fuse.wikichip.org/news/2219/intels-spring-crest-nnp-l-initial-details/)
