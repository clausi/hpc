# Synchronization

* `!$omp critical` - only one at a time, but all threads execute it
* `!$omp atomic` - update variable one at a time
* `!$omp single` - only execute by a single thread
* `!$omp master` - only execute by a single thread (master thread)
* `!$omp barrier` - explicit barrier, wait for all threads to continue
* `!$omp taskwait`- wait on the completion of child tasks of the current task

## Only one at a time, but all threads execute

![Turnstile](img/turnstile.png)

We have already observed that "parallel" usually means "out of order".
Concurrent write access to data from different threads may also lead
to unexpected results ("race conditions")

    program race
      implicit none
      integer*8 :: i, imax = 10000000
      real*8 :: sum = 0.0

      !$omp parallel do
      do i = 1, imax
        sum = sum + 1
      enddo  
      print '(" sum = ",e14.9)', sum
    end program race

Exercises:

* compile with `-fopenmp` , `export OMP_NUM_THREADS=1` and run
* now `export OMP_NUM_THREADS=10` and run again. Does the result vary?
* explain behaviour
* try to fix the code. Hint: try to use `!$omp critical`.
  Measure execution time before and after the fix.
  Use `export OMP_NUM_THREADS=..` to execute with 1,2,4 threads. Why 
  is it much slower with more threads?
* remove the "critical" directive and add `reduction(+:sum)` to the
  `parallel do` directive. Measure execution time again.

Note 1: this example is a reduction of variable `sum`. Even though
`!$omp critical` and `!$omp atomic` work here, they are not the correct
solution for this problem. Please check `raceok.f90` for the correct solution
using the `reduction` clause with the `!$omp parallel do`.

Note 2: A critical section is a **serial** section! When using `!$omp critical`
in this example, the entire loop is effectively converted to serial code
with additional thread control. These threads block themselves mutually.
Use `!$omp critical` only when absolutely needed.

![Critical is serial](img/critical.png)

`!$omp atomic` is similar to critical as it limits threads to access
variables in an atomic (isolated) manner. Try to avoid variables that need
to be updated atomically.

## Only one thread executes

![Single is serial](img/single.png)

With `!$omp single` we can determine that a certain code section will
only be executed by one thread. When using `!$omp master` this thread
is the master thread. `!$omp single` has an implicit barrier at the 
end (see next section)

*Single* and *Master* are serial constructs, Amdahl applies here. Often
used for IO operations.

**Exercise:**

* Make a new version of `pardo1.f90`. The new version should do 
  everything in parallel, initialization, calculations and output. 
  The team of threads should only be created once.
* Identify the problem with parallel output and use *single* or *master* 

## Wait till all are done

![Startbox](img/startbox.jpg)

Example `barrier.f90`

    !$omp parallel private(iam,nt,ipoints,istart,i)
      iam = omp_get_thread_num()
      nt =  omp_get_num_threads()
      ipoints = NMAX / nt
      istart = iam * ipoints
      if (iam .eq. nt-1) ipoints = NMAX - istart
      print '("iam ",I3," istart ",I3," ipoints ",I3)',iam,istart,ipoints
      
      do i = istart, istart + ipoints - 1
        a(i)=sin((2*i+1)*x)/(2.0*i+1);
      end do

      !$omp do schedule(dynamic) reduction(+:psum) 
      do i = 0, NMAX - 1
          psum = psum + a(i);
      end do
    !$omp end parallel

Ejercicios:

* compile and run with different number of threads.
* why doesn't this program work correctly?
* insert a `!$omp barrier` before the `!$ omp do ..`, now it 
  should work correctly
