## HPC is an essential tool for science and industry

In this section we will show a few examples where HPC is employed.
This list is by no means exhaustive!

### Galaxy formation and evolution

![Simulation](img/blowout.png)

* Evolution and Assembly of GaLaxies and their Environments - [EAGLE](http://icc.dur.ac.uk/Eagle/index.php)
* [Video on youtube](https://www.youtube.com/watch?v=-awYNyznf58)

### The Grid - CERN

![Links at CERN grid](img/cern_grid600.jpg)

* Video [Grid at CERN](http://insidehpc.com/2013/10/cern-uses-grid-global-supercomputer/)
* Grid [activity](https://videos.cern.ch/record/2640380)

### Weather forecasts (El hombre del tiempo)

![Mariano Medina, primer "Hombre del tiempo"](img/mariomedina.jpg)

* Supercomputation at [AEMET](http://www.aemet.es/en/idi/tic/supercomputacion) (spanish)
* [European Centre for Medium-Range Weather Forecasts](https://www.ecmwf.int/)

### Formula 1

![CFD in Formula 1](img/aero_fluid_flow_f1_car_small.jpg)

* [Aerodynamics of a F1](http://www.formula1-dictionary.net/aerodynamics_of_f1.html)
* CFD and wind tunnel testing has now been limited by rules, more CFD = less wind tunnel. 100% CFD in 2018 for 8 weeks: 10⁷ "core" hours at 1 GHz = 4*10⁴ cpu hours (CPU with 2.5 Ghz and 10 cores). In 8 weeks: 30 cpu hours/hour.
* [How HPC drives Formula 1 team's continuous design process](https://www.hpe.com/us/en/insights/articles/podcast-how-hpc-drives-formula-1-teams-continuous-design-process-1904.html)

### Car industry, product design
![Car crash](img/crash1.jpg)

[//]: # (Use of HPC in [car industry](http://www.automotiveitnews.org/articles/share/993346/)

* RADIOSS [comercial application](http://www.altairhyperworks.com/product/RADIOSS)
* Hall of Fame de [Ansys](http://www.ansys.com/Other/Hall-of-Fame/Archive)
