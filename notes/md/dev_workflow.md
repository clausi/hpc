# Development workflow

Even when developing a rather small application, an efficient setup for your
development workflow is important. Your source code will usually be copied
to different machines and it is important to maintain a versioning
mechanism.

It is possible to *cross compile* code on one machine in order to use the
resulting binary on other machines. This can be tricky as you have to know
the necessary compilation and optimization options for the arquitectures
where the code will be executed. For most scientific applications it is
easier to compile the source code directly on the target machine. In this
case the compiler can detect the current arquitecture and activate all
possible optimizations.

We therefore need

* Access to the development machine (ssh)
* Tools to compile our programs (make, compilers)
* A mechanism to copy the code to the development 
  machine and apply changes (source control)
* Editor or IDE

## Makefile and make

When compiling a program that consists of various modules, it is usually
only necessary to compile a subset of files (the files that have changed and
the ones that depend on the changed files). I recommend using `make` for
all programs that have more than one source code file.

## Version control

The usage of a version control system for your code using [git](https://git-scm.com/)
or [svn](https://subversion.apache.org/) is of great help even if running a
one man show. It is **essential** if various developers colaborate. The
following git hosts are frequently used

* [github](https://github.com/) - free: unlimited private and public
  repositories, 3 collaborators for private repositories. Recommended for open source
  projects.
* [gitlab](https://gitlab.com) - free: unlimited private and public projects and
  unlimited collaborators. Ideal for private projects.
* [bitbucket](https://bitbucket.org/) - free: 5 users, unlimited private repositories.
  Less functionality than gitlab.

If privacy is a concern, gitlab is open source and can be installed on a local server.
Make sure backups are well planned in this case.

## Editor/IDE

You need at least an editor to write your code. *gedit*, *vim*, *emacs* or any editor
with a Fortran/C mode will do, however a more advanced environment such as
[Visual Studio Code](https://code.visualstudio.com/) helps integrating functions such
as source control or debugging. The **Remote** extensions in VS Code even makes it
possible to develop directly on a remote machine.

## Some previous setup steps

### ssh

On the **client** (my private machine) enter `ssh-keygen` in a shell and  use default 
values as suggested.

On the **server** add your **public** key to `~/.ssh/authorized_keys`. Now you can ssh into
the server without entering your password.

### identity in git

    $ git config --global user.name "Albert Einstein"
    $ git config --global user.email albert@einstein.com

### ssh keys in GitLab

* Create an account on [GitLab](https://gitlab.com) if you don't have one
* In GitLab, select "Settings" in the user menu (rightmost in top menu)
* Select "SSH keys" in the left menu
* Paste contents of `~/.ssh/id_rsa.pub` into the field for the key and click on "Add key".
  If `~/.ssh/id_rsa.pub` does not exist, run `ssh-keygen` as described above.

## A typical session

### Create a new private project in gitlab (once for every project)

* Log into [GitLab](https://gitlab.com)
* In "Projects", click on "New Project"
* Enter a name, i.e. "My science project" and click on "Create Project"
* GitLab creates the project, in our example it has the url
  `https://gitlab.com/androbi/my-science-project`
* Click on "New file", enter the file name "hello.f90" and paste the following code


    program hello
      print *, "Hello World!"
    end program hello

* Click on "Commit changes"

### Clone the project to a local repository (once on every machine)

* In a shell on your local development machine, change to a 
  development directory, i.e. `cd ~/dev`
* In GitLab, go to your project url and click on "Clone"
* use the button to the right of the "Clone with ssh" url to copy the url
* In the shell, type `git clone ` and paste the url, in our example
`git clone git@gitlab.com:androbi/my-science-project.git`. The output should be 
similar to


    $ git clone git@gitlab.com:androbi/my-science-project.git
    Cloning into 'my-science-project'...
    remote: Enumerating objects: 3, done.
    remote: Counting objects: 100% (3/3), done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 3 (delta 0), reused 0 (delta 0)
    Receiving objects: 100% (3/3), done.

### Compile, run and change the program (development)

Issue the following commands, adapting the name of the repository


    $ cd my-science-project/
    $ ls
    hello.f90
    $ gfortran hello.f90
    $ ./a.out
    Hello World!
  
Edit the file `hello.f90` so that it prints "Hello HPC!", compile and run.

### "Save" changes to repository (development)

Run `git status`


    $ git status
    On branch master
    Your branch is up to date with 'origin/master'.

    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)

      modified:   hello.f90

    Untracked files:
      (use "git add <file>..." to include in what will be committed)

      a.out

    no changes added to commit (use "git add" and/or "git commit -a")

"Stage" the changes to **local** repository


    $ git add hello.f90
    $ git status
    On branch master
    Your branch is up to date with 'origin/master'.

    Changes to be committed:
      (use "git reset HEAD <file>..." to unstage)

      modified:   hello.f90

    Untracked files:
      (use "git add <file>..." to include in what will be committed)

      a.out

"Commit" the changes


    $ git commit -m "change print statement"
    [master 1a7d0fd] change print statement
    1 file changed, 2 insertions(+), 2 deletions(-)

Now "Push" to the GitLab server


    $ git push
    Counting objects: 3, done.
    Delta compression using up to 4 threads.
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (3/3), 287 bytes | 287.00 KiB/s, done.
    Total 3 (delta 0), reused 0 (delta 0)
    To gitlab.com:androbi/my-science-project.git
      fd2d65d..1a7d0fd  master -> master

### New session

    $ git pull
    Already up to date.
    # now develop, "stage", "commit" and "push"

----------
## Exercise

Add the following `Makefile` to your project


    OPT=-O1 -march=native
    FC = gfortran
    FFLAGS=$(OPT) -std=f95 -Wall -fall-intrinsics

    all: hello

    hello:  hello.f90
            $(FC) $(FFLAGS) -o $@ $^

    clean:
            rm -f *.o hello

Note: The indention before `$(FC)...` and `rm ...` is a TAB!