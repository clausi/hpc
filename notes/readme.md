# directory notes

Here we organize all notes. The course notes reside in directory
`md`. Gitbook creates the directory 'md/_book' when serving.

We currently publish `_publish` on hpc.androbi.com.

We also have some "private" notes (such as `cluster_huelva` or
`fortran_notes`) which are independent gitbooks. These can be 
made available on hpc.androbi.com as subdirectories of `_publish`
if desired, which means they will be visible as 
hpc.androbi.com/subdirectory without any links referencing
these "private" notes.
