# TODO

* mandel: run tests on KNL - ALSO WITH INTEL
* include eps as parameter in logit
* admjust mandel C to behave like Fortran
* finish f/scale
* check simd version in openmp. Include in mandel??
* see what vecexamples and vectorize can help
* get more SIMPLE vectorization examples before mandel

## Later

* move src/* to src/c/*
