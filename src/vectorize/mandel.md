### Results mandelbrot

All results for parameters

    mandelbrot(img,4096,-1.2,-0.7,0.5,0.0);

##### Haswell i7 4600U

      Time      Flops   GFlops/s  Kernel  Type
 6.369e+00  1.478e+10  2.320e+00 mandel0 float
 6.704e+00  1.478e+10  2.204e+00 mandel1 float
 3.013e+00  0.000e+00  0.000e+00 mandel2 float
 1.516e+00  0.000e+00  0.000e+00 mandel3 float

 6.347e+00  1.478e+10  2.328e+00 mandel0 double
 6.747e+00  1.478e+10  2.190e+00 mandel1 double
 5.787e+00  0.000e+00  0.000e+00 mandel2 double
 3.067e+00  0.000e+00  0.000e+00 mandel3 double

##### Cluster CICA (gcc 4.8.2)

      Time      Flops   GFlops/s  Kernel  Type

 6.374e+00  1.478e+10  2.319e+00 mandel0 float
 7.527e+00  1.478e+10  1.963e+00 mandel1 float
 5.963e+00  0.000e+00  0.000e+00 mandel2 float
 2.042e+00  0.000e+00  0.000e+00 mandel3 float

 6.372e+00  1.478e+10  2.319e+00 mandel0 double
 7.525e+00  1.478e+10  1.964e+00 mandel1 double
 6.605e+00  0.000e+00  0.000e+00 mandel2 double
 4.026e+00  0.000e+00  0.000e+00 mandel3 double

