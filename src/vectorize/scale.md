### Results on Tecra Z50

 2.717e+09  1.064e-13 # scale1 double -O1
 7.528e+09  1.066e-13 # scale1 double -O3 -ftree-vectorize -ffast-math
 1.439e+10  1.066e-13 # scale2 double -O3 -ftree-vectorize -ffast-math
 1.439e+10  1.066e-13 # scale3 double -O3 -ftree-vectorize -ffast-math
 2.218e+10  1.066e-13 # scale4 double -O3 -ftree-vectorize -ffast-math
 2.413e+10  1.066e-13 # scale5 double -O3 -ftree-vectorize -ffast-math

 2.726e+09  8.296e-01 # scale1 float -O1
 9.653e+09  8.296e-01 # scale1 float -O3 -ftree-vectorize -ffast-math
 2.001e+10  8.296e-01 # scale2 float -O3 -ftree-vectorize -ffast-math
 2.001e+10  8.296e-01 # scale3 float -O3 -ftree-vectorize -ffast-math
 3.282e+10  8.296e-01 # scale4 float -O3 -ftree-vectorize -ffast-math
 5.861e+10  8.296e-01 # scale5 float -O3 -ftree-vectorize -ffast-math

### Tecra, gcc 5.4

 6.718e+09  1.066e-13 # scale1 double -O3 -ftree-vectorize -ffast-math -funroll-loops
 1.187e+10  1.066e-13 # scale2 double -O3 -ftree-vectorize -ffast-math -funroll-loops
 1.205e+10  1.066e-13 # scale3 double -O3 -ftree-vectorize -ffast-math -funroll-loops
 2.279e+10  1.066e-13 # scale4 double -O3 -ftree-vectorize -ffast-math -funroll-loops
 2.486e+10  1.066e-13 # scale5 double -O3 -ftree-vectorize -ffast-math -funroll-loops


### Results on CICA Cluster

 2.447e+09  1.064e-13 # scale1 double -O1
 2.294e+09  1.064e-13 # scale1 double -O3 -ftree-vectorize -ffast-math
 2.467e+09  1.064e-13 # scale2 double -O3 -ftree-vectorize -ffast-math
 1.361e+09  1.064e-13 # scale3 double -O3 -ftree-vectorize -ffast-math
 3.044e+09  1.064e-13 # scale4 double -O3 -ftree-vectorize -ffast-math
 2.245e+09  1.064e-13 # scale5 double -O3 -ftree-vectorize -ffast-math

 1.478e+09  8.296e-01 # scale1 float -O1
 2.054e+09  8.296e-01 # scale1 float -O3 -ftree-vectorize -ffast-math
 8.873e+09  8.296e-01 # scale2 float -O3 -ftree-vectorize -ffast-math
 1.315e+09  8.296e-01 # scale3 float -O3 -ftree-vectorize -ffast-math
 1.292e+10  8.296e-01 # scale4 float -O3 -ftree-vectorize -ffast-math
 3.117e+09  8.296e-01 # scale5 float -O3 -ftree-vectorize -ffast-math

### CICA Cluster, gcc 4.8.2

 2.489e+09  1.064e-13 # scale1 double -O1
 3.628e+09  1.064e-13 # scale1 double -O3 -ftree-vectorize -ffast-math
 2.683e+09  1.064e-13 # scale2 double -O3 -ftree-vectorize -ffast-math
 2.708e+09  1.064e-13 # scale3 double -O3 -ftree-vectorize -ffast-math
 3.190e+09  1.064e-13 # scale4 double -O3 -ftree-vectorize -ffast-math
 6.686e+09  1.064e-13 # scale5 double -O3 -ftree-vectorize -ffast-math

 1.391e+09  8.296e-01 # scale1 float -O1
 5.614e+09  8.296e-01 # scale1 float -O3 -ftree-vectorize -ffast-math
 7.061e+09  8.296e-01 # scale2 float -O3 -ftree-vectorize -ffast-math
 6.670e+09  8.296e-01 # scale3 float -O3 -ftree-vectorize -ffast-math
 1.807e+10  8.296e-01 # scale4 float -O3 -ftree-vectorize -ffast-math
 5.067e+10  8.296e-01 # scale5 float -O3 -ftree-vectorize -ffast-math

### CICA Cluster, gcc 4.9.2

 3.636e+09  1.064e-13 #   8  16 scale1 double -O3 -ftree-vectorize -ffast-math -funroll-loops
 5.053e+09  1.064e-13 #   8  16 scale2 double -O3 -ftree-vectorize -ffast-math -funroll-loops
 5.053e+09  1.064e-13 #   8  16 scale3 double -O3 -ftree-vectorize -ffast-math -funroll-loops
 6.252e+09  1.064e-13 #   0   0 scale4 double -O3 -ftree-vectorize -ffast-math -funroll-loops
 6.779e+09  1.064e-13 #   0   0 scale5 double -O3 -ftree-vectorize -ffast-math -funroll-loops

 5.738e+09  8.296e-01 #   4   8 scale1 float -O3 -ftree-vectorize -ffast-math -funroll-loops
 9.473e+09  8.296e-01 #   4   8 scale2 float -O3 -ftree-vectorize -ffast-math -funroll-loops
 9.573e+09  8.296e-01 #   4   8 scale3 float -O3 -ftree-vectorize -ffast-math -funroll-loops
 2.078e+10  8.296e-01 #   0   0 scale4 float -O3 -ftree-vectorize -ffast-math -funroll-loops
 4.993e+10  8.296e-01 #   0   0 scale5 float -O3 -ftree-vectorize -ffast-math -funroll-loops
