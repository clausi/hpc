#include <stdio.h>

#include "../utilities/ppm_aos.h"
#include "../utilities/timing.h"

#define FACTOR_RED 0.945
#define FACTOR_GREEN 0.01
#define FACTOR_BLUE 0.50

/** 
 * apply a RGB filter
 */
double filter1(IMAGE *img) {
  int i,j;
  for (i=0;i<img->ny;i++) {
    for (j=0;j<img->nx;j++) {
      img->data[i*img->nx+j].r *= FACTOR_RED;
      img->data[i*img->nx+j].g *= FACTOR_GREEN;
      img->data[i*img->nx+j].b *= FACTOR_BLUE;
    }
  }
  return 3.0*img->nx*img->ny;
}
/** 
 * find bright pixels and tint them green
 */
double filter2(IMAGE *img) {
  int i,j;
  int cnt = 0;
  for (i=0;i<img->ny;i++) {
    for (j=0;j<img->nx;j++) {
      float bright=img->data[i*img->nx+j].r +
                   img->data[i*img->nx+j].g +
                   img->data[i*img->nx+j].b;

      if(bright >= 2.80) {
        img->data[i*img->nx+j].r *= 0.01;
        img->data[i*img->nx+j].b *= 0.01;
        cnt++;
      }
    }
  }
  return 2*img->nx*img->ny+2*cnt;
}

int main(void)
{
  int i,j;
  double secs,flops,val;
  IMAGE *img=makeimage(4096,4096);
  saveimage(img,"original.ppm");
  start_timer();
  flops=filter1(img);
  secs=stop_timer();
  printf("Time: %e Flops: %e GFlops/s: %e\n",secs,flops,1.0e-9*flops/secs); 
  saveimage(img,"filtered.ppm");
}

