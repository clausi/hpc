#include "ppm_dblvec.h"
#include "../bench/utilities/timing.h"
#include <stdio.h>

#define FACTOR_RED 0.945
#define FACTOR_GREEN 0.01
#define FACTOR_BLUE 0.50
#define INNER_LOOP 64
double fa[INNER_LOOP] __attribute__((aligned(64)));

/** 
 * find the max "intensity" in image
 */
double findmax(IMAGE *img, double *max) {
  int i,j;
  if (img->nx*img->ny==0)
    return 0;
  *max=0.0;
  double sum,locmax=0.0;
  double *x = __builtin_assume_aligned(img->data_rh, 64);
  double *y = __builtin_assume_aligned(img->data_gs, 64);
  double *z = __builtin_assume_aligned(img->data_bv, 64);
  for (i=0;i<img->nx*img->ny;i+=INNER_LOOP) {
    for (j=0;j<INNER_LOOP;j++) {
       fa[j]= x[i+j]+y[i+j]+z[i+j];
    }
    for (j=0;j<INNER_LOOP;j++) {
      locmax = fa[j] > locmax ? fa[j] : locmax;
    }
  }
  *max=locmax;
  return 2.0*img->nx*img->ny;
}


int main(void)
{
  int i,j;
  double secs,flops,val;
  IMAGE *img=readimage("galaxy.ppm");
  start_timer();
  flops=findmax(img,&val);
  printf("Max: %e\n",val); 
  secs=stop_timer();
  printf("Time: %e Flops: %e GFlops/s: %e\n",secs,flops,1.0e-9*flops/secs); 
//  saveimage(img,"filtered.ppm");
}

