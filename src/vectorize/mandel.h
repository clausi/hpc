#include "../utilities/ppm.h"

#ifdef USE_DOUBLE
#define REAL double
#else
#define REAL float
#endif

void color_pixel(IMAGE *img, int i, int j, int niters, int maxiters);

double mandelbrot(IMAGE *img, int maxiters,
                  REAL x1, REAL x2, REAL y1, REAL y2);
