#include <stdio.h>

#include "mandel.h"
#include "../utilities/timing.h"

#define str(s) #s
#define xstr(s) str(s)

/**
 * set hsv values of pixel (i,j) according to niters and maxiters
 * if niters=maxiters: the pixel is set black
 * else: for small niter, blue color. as niter increases, rainbow colors
 */
void color_pixel(IMAGE *img, int i, int j, int niters, int maxiters) {
  // todo: try mu = N + 1 - log (log  |Z(N)|) / log 2
  img->data_rh[j*img->nx+i]=240.0-(niters*360.0)/maxiters;
  img->data_gs[j*img->nx+i]=1.0;
  if (niters == maxiters)
    img->data_bv[j*img->nx+i]=0.0;
  else
    img->data_bv[j*img->nx+i]=1.0;
}


/**
 * Calculate the mandelbrot set
 * standard: flops=mandelbrotX(img,4096,-2.0,1.0,1.5,-1.5);
 */

int main(void) {
  int i,j;
  double secs,flops,val;
  IMAGE *img=makeimage(1024,1024);
  start_timer();
  flops=mandelbrot(img,4096,-1.2,-0.7,0.5,0.0);
  secs=stop_timer();
//  printf("      Time      Flops   GFlops/s  Kernel  Type\n"); 
  printf("%10.3e %10.3e %10.3e %s %s\n",
          secs,flops,1.0e-9*flops/secs,xstr(MANDEL),xstr(REAL)); 
  saveimage(img,"mandel.ppm");
}
