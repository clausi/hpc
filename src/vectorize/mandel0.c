#include <complex.h>
#include "mandel.h"

/**
 * standard mandelbrot implementation
 */
double mandelbrot(IMAGE *img, int maxiters, 
                  REAL x1, REAL x2, REAL y1, REAL y2) { 
  int i,j;
  REAL dx = (x2-x1)/img->nx, dy = (y2-y1)/img->ny;
  double flops=0;
  for (j = 0; j < img->ny; ++j) {
    for (i = 0; i < img->nx; ++i) {
      REAL complex c = x1+dx*i + (y1+dy*j) * I;
      REAL complex z = 0;
      int count = 0;
      while ((++count < maxiters) && 
#ifdef USE_DOUBLE
         (creal(z)*creal(z)+cimag(z)*cimag(z) < 4.0)) {
#else
         (crealf(z)*crealf(z)+cimagf(z)*cimagf(z) < 4.0)) {
#endif
        z = z*z+c;
      }
      color_pixel(img,i,j,count,maxiters);
      flops += 4+count*8;
    }
  }
  img->cspace=TYPE_HSV;
  return flops;
}
