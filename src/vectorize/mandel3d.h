#define REGSIZE 4 

double cr[REGSIZE] __attribute__((aligned(32)));

/**
 * AVX intrinsics version of mandelbrot. For a SSE version check
 * https://www.cs.uaf.edu/2011/fall/cs441/lecture/10_04_SSE_movemask.html
 */
double mandelbrot(IMAGE *img, int maxiters, 
                  double x1, double x2, double y1, double y2) { 

  int i,j,k;

  if (img->nx % REGSIZE != 0) {
    printf("Fatal error: image x size must be multiple of %d\n",REGSIZE);
    exit(-1);
  }

  __m256d vc1 = _mm256_set1_pd(1.0); //vector constant 1.0
  __m256d vc2 = _mm256_set1_pd(2.0); //vector constant 2.0
  __m256d vc4 = _mm256_set1_pd(4.0); //vector constant 4.0
  __m256d v07 = _mm256_set_pd(3.0,2.0,1.0,0.0);
  
  double dx = (x2-x1)/img->nx;
  double dy = (y2-y1)/img->ny;

  __m256d vdx = _mm256_set1_pd(dx);                //vdx = dx
  __m256d vdy = _mm256_set1_pd(dy);                //vdx = dx

  for (j = 0; j < img->ny; j++) {
    for (i = 0; i < img->nx; i+=REGSIZE) {

      __m256d vcr = _mm256_set1_pd((double) i);    //vcr = i
      vcr = _mm256_add_pd(vcr,v07);               //vcr += 0 1 2 3 4 5 6 7
      vcr = _mm256_mul_pd(vdx,vcr);               //vcr*=vdx
      vcr = _mm256_add_pd(_mm256_set1_pd(x1),vcr);//vcr+=x1

      __m256d vci = _mm256_set1_pd((double) j);    //vci = i
      vci = _mm256_mul_pd(vdy,vci);               //vci*=vdy
      vci = _mm256_add_pd(_mm256_set1_pd(y1),vci);//vci+=y1

      __m256d vzr = _mm256_set1_pd(0.0);           //vzr = 0
      __m256d vzi = _mm256_set1_pd(0.0);           //vzi = 0
      
      __m256d cnt = _mm256_set1_pd(0.0);           //cnt = 0

      int niter = 0;
      while ((niter++ < maxiters)) {
        __m256d vzr2 = _mm256_mul_pd(vzr,vzr);  //vzr2 = vzr*vzr
        __m256d vzi2 = _mm256_mul_pd(vzi,vzi);  //vzi2 = vzi*vzi
        __m256d vlen = _mm256_add_pd(vzr2,vzi2);//vlen = vzr2+vzi2

        // cond = vlen-vc4 ( < 0 for points in set) 
        __m256d cond = _mm256_sub_pd(vlen,vc4);
        
        // movemask sets bits to 1 from most significant bit in
        // cond. This is the float sign, 0 is positive, 1 negative (IEEE_754)
        // So, if all are 0, all cond elements are positive (out of set)
        
        if (_mm256_movemask_pd(cond)==0) break;

        __m256d vzrzi = _mm256_mul_pd(vzr,vzi); //vzrzi = vzr*vzi 
        __m256d tzi = _mm256_mul_pd(vc2,vzrzi); //tzi = vc2*vzrzi 
        tzi = _mm256_add_pd(tzi,vci);          //tzi += vci 
 
        __m256d tzr = _mm256_sub_pd(vzr2,vzi2); //tzr = vzr2-vzi2
        tzr = _mm256_add_pd(tzr,vcr);           //tzr += vcr
        
        // MASK has all bits set in element i if vlen[i]<vc4[i] 
        __m256d MASK = _mm256_cmp_pd(vlen,vc4,_CMP_LT_OS);

        // The following is a vectorized conditional:
        // vzr[i] = (vlen[i] < 4) ? tzr[i] : vzr[i] 
        // vzr =  (MASK & tzr) | (!MASK & vzr)
        // Example: vlen = [1 5] -> MASK = [1 0] !MASK = [0 1]
        // vzr =  ([1 0] & [tzr0 tzr1]) | ([0 1] & [vzr0 vzr1]) =
        //        ([tzr0 0] | [0 vzr1]) = [tzr0 vzr1]

        vzr=_mm256_or_pd(_mm256_and_pd(MASK,tzr),_mm256_andnot_pd(MASK,vzr));
        vzi=_mm256_or_pd(_mm256_and_pd(MASK,tzi),_mm256_andnot_pd(MASK,vzi));
        
        // increment counter using mask
        __m256d cnt1 = _mm256_add_pd(cnt,vc1);  //cnt1 = cnt + 1
        cnt=_mm256_or_pd(_mm256_and_pd(MASK,cnt1),_mm256_andnot_pd(MASK,cnt));
        
      } 
      // store our counter to cr
      _mm256_store_pd(cr,cnt);
      for (k = 0; k < REGSIZE; k++) {
          color_pixel(img,i+k,j,cr[k],maxiters);
      }
    }
  }
  img->cspace=TYPE_HSV;
  return 0.0;
}
