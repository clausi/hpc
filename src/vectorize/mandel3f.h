#define REGSIZE 8 

float cr[REGSIZE] __attribute__((aligned(32)));

/**
 * AVX intrinsics version of mandelbrot. For a SSE version check
 * https://www.cs.uaf.edu/2011/fall/cs441/lecture/10_04_SSE_movemask.html
 */
double mandelbrot(IMAGE *img, int maxiters, 
                  float x1, float x2, float y1, float y2) { 

  int i,j,k;

  if (img->nx % REGSIZE != 0) {
    printf("Fatal error: image x size must be multiple of %d\n",REGSIZE);
    exit(-1);
  }

  __m256 vc1 = _mm256_set1_ps(1.0); //vector constant 1.0
  __m256 vc2 = _mm256_set1_ps(2.0); //vector constant 2.0
  __m256 vc4 = _mm256_set1_ps(4.0); //vector constant 4.0
  __m256 v07 = _mm256_set_ps(7.0,6.0,5.0,4.0,3.0,2.0,1.0,0.0);
  
  float dx = (x2-x1)/img->nx;
  float dy = (y2-y1)/img->ny;

  __m256 vdx = _mm256_set1_ps(dx);                //vdx = dx
  __m256 vdy = _mm256_set1_ps(dy);                //vdx = dx

  for (j = 0; j < img->ny; j++) {
    for (i = 0; i < img->nx; i+=REGSIZE) {

      __m256 vcr = _mm256_set1_ps((float) i);    //vcr = i
      vcr = _mm256_add_ps(vcr,v07);               //vcr += 0 1 2 3 4 5 6 7
      vcr = _mm256_mul_ps(vdx,vcr);               //vcr*=vdx
      vcr = _mm256_add_ps(_mm256_set1_ps(x1),vcr);//vcr+=x1

      __m256 vci = _mm256_set1_ps((float) j);    //vci = i
      vci = _mm256_mul_ps(vdy,vci);               //vci*=vdy
      vci = _mm256_add_ps(_mm256_set1_ps(y1),vci);//vci+=y1

      __m256 vzr = _mm256_set1_ps(0.0);           //vzr = 0
      __m256 vzi = _mm256_set1_ps(0.0);           //vzi = 0
      
      __m256 cnt = _mm256_set1_ps(0.0);           //cnt = 0

      int niter = 0;
      while ((niter++ < maxiters)) {
        __m256 vzr2 = _mm256_mul_ps(vzr,vzr);  //vzr2 = vzr*vzr
        __m256 vzi2 = _mm256_mul_ps(vzi,vzi);  //vzi2 = vzi*vzi
        __m256 vlen = _mm256_add_ps(vzr2,vzi2);//vlen = vzr2+vzi2

        // cond = vlen-vc4 ( < 0 for points in set) 
        __m256 cond = _mm256_sub_ps(vlen,vc4);
        
        // movemask sets bits to 1 from most significant bit in
        // cond. This is the float sign, 0 is positive, 1 negative (IEEE_754)
        // So, if all are 0, all cond elements are positive (out of set)
        
        if (_mm256_movemask_ps(cond)==0) break;

        __m256 vzrzi = _mm256_mul_ps(vzr,vzi); //vzrzi = vzr*vzi 
        __m256 tzi = _mm256_mul_ps(vc2,vzrzi); //tzi = vc2*vzrzi 
        tzi = _mm256_add_ps(tzi,vci);          //tzi += vci 
 
        __m256 tzr = _mm256_sub_ps(vzr2,vzi2); //tzr = vzr2-vzi2
        tzr = _mm256_add_ps(tzr,vcr);          //tzr += vcr
        
        // MASK has all bits set in element i if vlen[i]<vc4[i] 
        __m256 MASK = _mm256_cmp_ps(vlen,vc4,_CMP_LT_OS);

        // The following is a vectorized conditional:
        // vzr[i] = (vlen[i] < 4) ? tzr[i] : vzr[i] 
        // vzr =  (MASK & tzr) | (!MASK & vzr)
        // Example: vlen = [1 5] -> MASK = [1 0] !MASK = [0 1]
        // vzr =  ([1 0] & [tzr0 tzr1]) | ([0 1] & [vzr0 vzr1]) =
        //        ([tzr0 0] | [0 vzr1]) = [tzr0 vzr1]

        vzr=_mm256_or_ps(_mm256_and_ps(MASK,tzr),_mm256_andnot_ps(MASK,vzr));
        vzi=_mm256_or_ps(_mm256_and_ps(MASK,tzi),_mm256_andnot_ps(MASK,vzi));
        
        // increment counter using mask
        __m256 cnt1 = _mm256_add_ps(cnt,vc1);  //cnt1 = cnt + 1
        cnt=_mm256_or_ps(_mm256_and_ps(MASK,cnt1),_mm256_andnot_ps(MASK,cnt));
        
      } 
      // store our counter to cr
      _mm256_store_ps(cr,cnt);
      for (k = 0; k < REGSIZE; k++) {
          color_pixel(img,i+k,j,cr[k],maxiters);
      }
    }
  }
  img->cspace=TYPE_HSV;
  return 0.0;
}
