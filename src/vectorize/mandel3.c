#include <stdio.h>
#include <x86intrin.h>

#include "mandel.h"

#ifdef USE_DOUBLE
#include "mandel3d.h"
#else
#include "mandel3f.h"
#endif

