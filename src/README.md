# src

### Repository for some content from my hpc course ###

Directories:

* bench  -   some small benchmarks
* mpi - mpi programs (to be added to repository)
* openmp - openmp programs
* utilities - general utilities for all programs
* vectorize - examples for vectorization
