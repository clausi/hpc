#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define NMAX 10               // number of grid point
#define NMAXX (NMAX)           //
#define NMAXY (NMAX)           //
#define NMAXZ (NMAX)           //

#define ITERMAX 100            // max. number of iterations

double u[NMAXX][NMAXY][NMAXZ];
double ou[NMAXX][NMAXY][NMAXZ];

// provide an initial guess for iteration

void initialize_guess() {
  int i,j,k;

  for (i=0;i<NMAX;i++) {         // boundary conditions
    for (j=0;j<NMAX;j++) {   
      ou[0][i][j]     =  1.0;
      ou[i][0][j]     =  1.0;
      ou[i][j][0]     =  1.0;
      ou[NMAX-1][i][j] =  1.0;
      ou[i][NMAX-1][j] =  1.0;
      ou[i][j][NMAX-1] =  1.0;
    }
  }

  for (i=1;i<NMAXX-1;i++)         // inner points are 0
    for (j=1;j<NMAXY-1;j++)
      for (k=1;k<NMAXZ-1;k++)
        ou[i][j][k] = 0.0;
}

void print_grid(int n) {
  int i,j,k;
  for (i=0;i<NMAXX;i++) 
    for (j=0;j<NMAXY;j++)
      for (k=0;k<NMAXZ;k++)
        printf("%d %d %d %d %10.3e\n",n,i,j,k,ou[i][j][k]);
}

// update inner grid points

void update_grid() {
  int i,j,k;

  for (i=1;i<NMAXX-1;i++) {
    for (j=1;j<NMAXY-1;j++) { 
      for (k=1;k<NMAXZ-1;k++) {
        u[i][j][k]=(ou[i-1][j][k]+ou[i+1][j][k]+
                    ou[i][j-1][k]+ou[i][j+1][k]+
                    ou[i][j][k-1]+ou[i][j][k+1])/6.0;
      }
    }
  }

  for (i=1;i<NMAXX-1;i++) {
    for (j=1;j<NMAXY-1;j++) {
      for (k=1;k<NMAXZ-1;k++) {
        ou[i][j][k]=u[i][j][k];
      }
    }
  }
}

// main program

int main() {
  int n;

  initialize_guess();          // initialize matrix
  
  n=1;
  while (n < ITERMAX) {         // relaxation
   update_grid();
   n++;
  }

  print_grid(n);
  return 0;
}
