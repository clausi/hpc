#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include "omp.h"
#include "../../utilities/ppm.h"

/*
 * This is a full MPI version, but not optimized.
 */

#define NSIZE 4096 
#define NITER 10
#define NSAVE -1
#define ICO 1
#define INT unsigned char

// set universe to all "dead"

void univzero(int nx, int ny, INT univ[][ny]) {
  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < ny; j++) {
      univ[i+ICO][j]= 0;
    }
  }
} 

// set borders of univ to "alive"

void univborders(int nx, int ny, INT univ[][ny]) {
  for (int i = 0; i < nx; i++) {
    univ[i+ICO][0]= 1;
    univ[i+ICO][ny-1]= 1;
  }
  for (int j = 0; j < ny; j++) {
    univ[1][j]= 1;
    univ[nx][j]= 1;
  }
} 

// glider at point i,j
// 010
// 001
// 111
void univglider(int i, int j, int nx, int ny, INT univ[][ny]) {
  univ[i+ICO][j+1] = univ[i+1+ICO][j+2] = 1;
  univ[i+2+ICO][j] = univ[i+2+ICO][j+1] = univ[i+2+ICO][j+2] = 1;
} 

// copy shadow cells between processes

void copyshadowmpi(int nx, int ny, INT univ[][ny],
                   int before, int myid, int after) {
  MPI_Status status;
  if (myid % 2) {
    MPI_Send(&univ[1][0],ny, MPI_UNSIGNED_CHAR, before, 0, MPI_COMM_WORLD);
    MPI_Recv(&univ[0][0],ny, MPI_UNSIGNED_CHAR, before, 0, MPI_COMM_WORLD,
              &status);
    MPI_Recv(&univ[nx+1][0],ny, MPI_UNSIGNED_CHAR, after, 0, MPI_COMM_WORLD,
              &status);
    MPI_Send(&univ[nx][0],ny, MPI_UNSIGNED_CHAR, after, 0, MPI_COMM_WORLD);
  } else {
    MPI_Recv(&univ[nx+1][0],ny, MPI_UNSIGNED_CHAR, after, 0, MPI_COMM_WORLD,&status);
    MPI_Send(&univ[nx][0],ny, MPI_UNSIGNED_CHAR, after, 0, MPI_COMM_WORLD);
    MPI_Send(&univ[1][0],ny, MPI_UNSIGNED_CHAR, before, 0, MPI_COMM_WORLD);
    MPI_Recv(&univ[0][0],ny, MPI_UNSIGNED_CHAR, before, 0, MPI_COMM_WORLD,&status);
  }
}

// copy shadow cells, same process

void copyshadow(int nx, int ny, INT univ[][ny]) {
  for (int j=0;j<ny;j++) {
     univ[0][j]=univ[nx][j];
     univ[nx+1][j]=univ[1][j];
  }
}

// copy new generation into universe

void univcopynew(int n1, int n2, int nx, int ny, 
                 INT univ[][ny], INT newu[][ny]) { 
  for (int i = n1; i < n2; i++) {
    for (int j = 0; j < ny; j++) {
      univ[i+ICO][j] = newu[i+ICO][j];
    }
  }
}

// create random initial state

void univrand(int nx, int ny, INT univ[][ny]) {
  time_t t;
  srand((unsigned) time(&t));
  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < ny; j++) {
      univ[i+ICO][j]= rand() < RAND_MAX / 10 ? 1 : 0;
    }
  }
} 

// univ -> img. Image is only on master

void univ2img(IMAGE *img, int nx, int ny, INT univ[][ny], INT newu[][ny],
               int myid, int np) {
  MPI_Status status;
  if (myid==0) { // master first writes its own data to image
    for (int j = 0; j < ny; j++) {
      for (int i = 0; i < nx; i++) {
        img->data_rh[j*img->nx+i] = 180;
        img->data_gs[j*img->nx+i] = 1.0;
        img->data_bv[j*img->nx+i] = univ[i+ICO][j];
      }
    }
    for (int n=1; n < np; n++) { // now receive from all other
      // we use array "newu" as a data buffer
      MPI_Recv(&newu[1][0],nx*ny, MPI_UNSIGNED_CHAR, n, 0, MPI_COMM_WORLD,
                &status);
      for (int j = 0; j < ny; j++) {
        for (int i = 0; i < nx; i++) { // and write to image
          int i_img = i+n*nx;
          img->data_rh[j*img->nx+i_img] = 180;
          img->data_gs[j*img->nx+i_img] = 1.0;
          img->data_bv[j*img->nx+i_img] = newu[i+ICO][j];
        }
      }
    } 
  } else { // all other processes just send their data to master
    MPI_Send(&univ[1][0],nx*ny, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD);
  }
} 

// img -> univ. image is only stored on master

void img2univ(IMAGE *img, int nx, int ny, INT univ[][ny], INT newu[][ny],
               int myid, int np) {
  MPI_Status status;
  if (myid==0) { // master writes its part of image to its universe
    for (int j = 0; j < ny; j++) {
      for (int i = 0; i < nx; i++) {
        univ[i+ICO][j] = img->data_bv[j*img->nx+i] > 0.5 ? 1 : 0;
      }
    }
    for (int n=1; n < np; n++) { // write part of image to newu
      for (int j = 0; j < ny; j++) {
        for (int i = 0; i < nx; i++) {
          int i_img = i+n*nx;
          newu[i+ICO][j] = img->data_bv[j*img->nx+i_img] > 0.5 ? 1 : 0;
        }
      }
      // and send to other processes
      MPI_Send(&newu[1][0],nx*ny, MPI_UNSIGNED_CHAR, n, 0, MPI_COMM_WORLD);
    }
  } else { // all other processes just receive their univ from master
      MPI_Recv(&univ[1][0],nx*ny, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD,
                &status);
  }
}

// calculate next generation

void evolve(int n1, int n2, int nx, int ny, 
             INT univ[][ny], INT newu[][ny],
             int np, int before, int myid, int after) {

  for (int i = n1; i < n2; i++) {
    for (int j = 0; j < ny; j++) {
      int n = 0;
      for (int i1 = i - 1; i1 <= i + 1; i1++) {
        for (int j1 = j - 1; j1 <= j + 1; j1++) {
          n+=univ[i1+ICO][(j1 + ny) % ny];
        } 
      }
      n-=univ[i+ICO][j];
      newu[i+ICO][j] = (n == 3 || (n == 2 && univ[i+ICO][j]));
    }
  }
}

// main program

int main() {

  int nx,ny,np,myid,before,after,init_img,nlen;
  char filename[80],name[MPI_MAX_PROCESSOR_NAME];
  double t0=0,time=0;

  // initialize and get info about world and ourselves
  MPI_Init(NULL,NULL);
  MPI_Comm_size(MPI_COMM_WORLD,&np);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Get_processor_name(name, &nlen);
  printf("Process %d of %d is running on %s\n", myid,np,name);

  init_img=0;
  IMAGE *img=NULL;
  // full image only on master 
  if (myid==0) {
    img=readimage("input.ppm");
    if (img!=NULL) {
      if (img->nx%np) {
        printf("image width must be divisible by NP\n");
        MPI_Abort(MPI_COMM_WORLD,1);
        MPI_Finalize();
        exit(0);
      } 
      init_img=1;
      image2hsv(img);
      nx=img->nx/np; 
      ny=img->ny;
    } else {
      if (NSIZE%np) {
        printf("NSIZE must be divisible by NP\n");
        MPI_Abort(MPI_COMM_WORLD,2);
        MPI_Finalize();
        exit(0);
      } 
      nx=NSIZE/np;
      ny=NSIZE;
      printf("using empty image %d x %d\n",nx,ny);
    }
  }

  // broadcast dimesions to other processes
  MPI_Bcast(&nx, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&ny, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&init_img, 1, MPI_INT, 0, MPI_COMM_WORLD);

  // allocate mem for universe and new universe
  INT(*univ)[ny];
  INT(*newu)[ny];
  univ = malloc(sizeof(INT)*(nx+2)*ny);
  newu = malloc(sizeof(INT)*(nx+2)*ny);

  if (init_img==1) {
    img2univ(img,nx,ny,univ,newu,myid,np);
  } else {
    if (myid==0) img=makeimage(nx*np,ny); //todo: dont store full image 
    univzero(nx,ny,univ);
    if (myid==0) univglider(4,4,nx,ny,univ);
  }

  // calculate our(myid) neighbors
  before=(myid - 1 + np) % np;
  after=(myid + 1 + np) % np;
  // copy shadow cells
  if (np==1)
    copyshadow(nx,ny,univ);
  else 
    copyshadowmpi(nx,ny,univ,before,myid,after);

  // transfer universe to image
  univ2img(img,nx,ny,univ,newu,myid,np);

  // save initial state
  if ((NSAVE >0) && (myid==0)) saveimage(img,"step_000.ppm");
  
  if (myid==0) t0=MPI_Wtime(); 
  int ncnt=1;
#pragma omp parallel
  {
    int iam = omp_get_thread_num();
    int nt =  omp_get_num_threads();
    int npoints= nx / nt;
    int n1=iam*npoints;
    int n2=n1+npoints;
    printf("thread %d of Process %d of %d is running on %s\n", iam,myid,np,name);

    for (int n = 1; n <= NITER; n++) {
      evolve(n1,n2,nx,ny,univ,newu,np,before,myid,after);
#pragma omp barrier
      univcopynew(n1,n2,nx,ny,univ,newu);
#pragma omp barrier
#pragma omp master
      if (np==1)
        copyshadow(nx,ny,univ);
      else
        copyshadowmpi(nx,ny,univ,before,myid,after);

      // at some points save image
#pragma omp master
      if ((NSAVE > 0) && (n % NSAVE == 0)) {
        univ2img(img,nx,ny,univ,newu,myid,np);
        if (myid==0) {
          sprintf(filename,"step_%03d.ppm",ncnt++);
          saveimage(img,filename);
        }
      }
    }
  }
  if (myid==0) {
     time=MPI_Wtime()-t0;
     printf("Done. %d iterations in %f secs\n",NITER,time);
  }
  MPI_Finalize();
}
