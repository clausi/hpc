Rules:

1) Any live cell with fewer than two live neighbours dies, 
   as if caused by underpopulation.
2) Any live cell with two (a) or three (b) live neighbours lives 
   on to the next generation.
3) Any live cell with more than three live neighbours dies, 
   as if by overpopulation.
4) Any dead cell with exactly three live neighbours becomes a live cell, 
   as if by reproduction.

new[i][j] = (n == 3 || (n == 2 && univ[i][j]));

              2b,4     2a  

Condiciones periodicas:
 i=0..9 (nx=10 values)

 i   (i+nx) % nx
=================
-1    9   
-------
 0    0 
 5    5 
 9    9 
--------
 10   0
 
