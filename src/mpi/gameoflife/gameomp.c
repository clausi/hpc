#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "omp.h"
#include "../../utilities/timing.h"
#include "../../utilities/ppm.h"

#define NSIZE 64
#define NITER 200
#define NSAVE 10
#define INT unsigned char

// set universe to all "dead"

void univzero(int nx, int ny, INT univ[][ny]) {

  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < ny; j++) {
      univ[i][j]= 0;
    }
  }
} 

// set borders of univ to "alive"

void univborders(int nx, int ny, INT univ[][ny]) {
  for (int i = 0; i < nx; i++) {
    univ[i][0]= 1;
    univ[i][ny-1]= 1;
  }
  for (int j = 0; j < ny; j++) {
    univ[0][j]= 1;
    univ[nx-1][j]= 1;
  }
} 


// glider at point i,j
// 010
// 001
// 111

void univglider(int i, int j, int nx, int ny, INT univ[][ny]) {
  univ[i][j+1] = univ[i+1][j+2] = 1;
  univ[i+2][j] = univ[i+2][j+1] = univ[i+2][j+2] = 1;
}

// random initial state

void univrand(int nx, int ny, INT univ[][ny]) {
  time_t t;
  srand((unsigned) time(&t));
  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < ny; j++) {
      univ[i][j]= rand() < RAND_MAX / 10 ? 1 : 0;
    }
  }
} 

// univ -> img

void univ2img(IMAGE *img, int nx, int ny, INT univ[][ny]) {
  for (int j = 0; j < img->ny; j++) {
    for (int i = 0; i < img->nx; i++) {
      img->data_rh[j*img->nx+i] = 180;
      img->data_gs[j*img->nx+i] = 1.0;
      img->data_bv[j*img->nx+i] = univ[i][j];
    }
  }
} 

// img -> univ

void img2univ(IMAGE *img, int nx, int ny, INT univ[][ny]) {
  for (int j = 0; j < img->ny; j++) {
    for (int i = 0; i < img->nx; i++) {
      univ[i][j] = img->data_bv[j*img->nx+i] > 0.5 ? 1 : 0;
    }
  }
}

// calculate next generation

void evolve(int n1, int n2, int nx, int ny, 
             INT  univ[][ny], INT  newu[][ny]) {
  for (int i = n1; i < n2; i++) {
    for (int j = 0; j < ny; j++) {
      int n = 0;
      for (int i1 = i - 1; i1 <= i + 1; i1++) {
        for (int j1 = j - 1; j1 <= j + 1; j1++) {
          n+=univ[(i1 + nx) % nx][(j1 + ny) % ny];
        } 
      }
      n -= univ[i][j];
      newu[i][j] = (n == 3 || (n == 2 && univ[i][j]));
    }
  }
}

// copy new generation into universe

void univcopynew(int n1, int n2, int nx, int ny, 
                  INT univ[][ny], INT newu[][ny]) {
  for (int i = n1; i < n2; i++) {
    for (int j = 0; j < ny; j++) {
      univ[i][j] = newu[i][j];
    }
  }
}

// main program

int main() {

  int nx,ny;
  char filename[80];

  // try to read an image from disk, if not found, use empty image 
  IMAGE *img=readimage("input.ppm");
  if (img!=NULL) {
    image2hsv(img);
    nx=img->nx;
    ny=img->ny;
  } else {
    nx=ny=NSIZE;
    printf("using empty image %d x %d\n",nx,ny);
  }

  // allocate mem for universe and new universe
  INT(*univ)[ny];
  INT(*newu)[ny];
  univ = malloc(sizeof(INT)*nx*ny);
  newu = malloc(sizeof(INT)*nx*ny);

  // initialize universe
  if (img!=NULL) {
    img2univ(img,nx,ny,univ);
  } else {
    img=makeimage(nx,ny);
    univzero(nx,ny,univ);
    univglider(4,4,nx,ny,univ);
  }

  // transfer universe to image
  univ2img(img,nx,ny,univ);

  // save initial state
  if (NSAVE > 0) saveimage(img,"step_000.ppm");

  int ncnt=1;
  double secs;
  secs = omp_get_wtime();

#pragma omp parallel
  {
    int iam = omp_get_thread_num();
    int nt =  omp_get_num_threads();
    int npoints= nx / nt; 
    int n1=iam*npoints;
    int n2=n1+npoints;

    printf("thread %d active: %d %d\n",iam,n1,n2);
    for (int n = 1; n <= NITER; n++) {
      evolve(n1,n2,nx,ny,univ,newu);
#pragma omp barrier
      univcopynew(n1,n2,nx,ny,univ,newu);

    // at some points save image
#pragma omp master
      if ((NSAVE > 0) && (n % NSAVE == 0)) {
        univ2img(img,nx,ny,univ);
        sprintf(filename,"step_%03d.ppm",ncnt++);
        saveimage(img,filename);
      }

    } 
  }
  secs=omp_get_wtime()-secs;
  printf("%d iterations in %e secs\n",NITER,secs);
  free(univ);
  free(newu);
}
