#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#define NMINELEMS 10000000
#define NMINREP 10000
#define min(a,b) (((a)<(b))?(a):(b))
int main(int argc, char *argv[])
{
    int myid, left, right, np, nmax, niter, i, n;
    double *buffer; 
    double secs;
    MPI_Status status;

    // initialize and get info about world and ourselves
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&np);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    nmax = 1000;
    niter = min(NMINREP,NMINELEMS/nmax);
    buffer = malloc(sizeof(double)*nmax);
    if (buffer == NULL) {
      printf("Error allocating buffer\n");
      MPI_Finalize();
      return -1;
    }
    for (i=0;i<nmax;i++) {
      if (myid==0) 
        buffer[i] = 1;
      else
        buffer[i] = 0;
    }

    if (myid == np-1) 
      right=0;
    else 
      right=myid+1;

    if (myid == 0 ) 
      left = np-1;
    else 
      left = myid-1;
    
    MPI_Barrier(MPI_COMM_WORLD);
    if (myid==0)
      secs=MPI_Wtime();
    for (n=0;n<niter;n++) {
      if (myid==0) {
        if (n==0) printf("%d %d %d before: %10.3e\n",left,myid,right,buffer[0]);
        MPI_Send(buffer,nmax, MPI_DOUBLE, right, 0, MPI_COMM_WORLD);
        MPI_Recv(buffer,nmax, MPI_DOUBLE, left, 0, MPI_COMM_WORLD,&status);
        if (n==0) printf("%d %d %d recv: %10.3e\n",left,myid,right,buffer[0]);
      } else {
        if (n==0) printf("%d %d %d before: %10.3e\n",left,myid,right,buffer[0]);
        MPI_Recv(buffer,nmax, MPI_DOUBLE, left, 0, MPI_COMM_WORLD,&status);
        if (n==0) printf("%d %d %d recv: %10.3e\n",left,myid,right,buffer[0]);
        MPI_Send(buffer,nmax, MPI_DOUBLE, right, 0, MPI_COMM_WORLD);
      }
    }
    if (myid==0) {
      secs=(MPI_Wtime()-secs)/(2.0*niter);
      printf("%d elements in %10.3e usecs, bw = %10.3e MB/s (%d repeats)\n",
           nmax,secs/1.0e-6,nmax*sizeof(double)/secs/(1024*1024),niter);
    }
    MPI_Finalize();
    free(buffer);
    return (0);
}
