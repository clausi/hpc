      program hello
      include 'mpif.h'
      integer myid, np, len, ierror, status(MPI_STATUS_SIZE)
      character*(MPI_MAX_PROCESSOR_NAME) name
C   
      call MPI_INIT(ierror)
      call MPI_COMM_SIZE(MPI_COMM_WORLD, np, ierror)
      call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierror)
      call MPI_GET_PROCESSOR_NAME(name, len, ierror)
      print "('process ',I3,' of ',I3,' on ',A)",myid,np,trim(name)
      call MPI_FINALIZE(ierror)
      end
