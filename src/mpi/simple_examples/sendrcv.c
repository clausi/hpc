#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    int myid, np, ival, nlen;
    char name[MPI_MAX_PROCESSOR_NAME];

    MPI_Status status;

    // initialize and get info about world and ourselves
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&np);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    MPI_Get_processor_name(name, &nlen);

    if (np < 3) {
      printf("Error - need at least 2 processes\n");
      MPI_Finalize();
      return (-1);
    }
    printf("Process %d is running on %s\n", myid,name);
    fflush(stdout); 

    if (myid == 0) {               // first just sends to next
      ival = 1;
      MPI_Send(&ival,1, MPI_INT, myid+1, 0, MPI_COMM_WORLD);
    }
    else if (myid < np - 1) {      // receive, add 1, and send to next
      MPI_Recv(&ival, 1, MPI_INT, myid-1, 0, MPI_COMM_WORLD,&status);
      ival++;
      MPI_Send(&ival,1, MPI_INT, myid+1, 0, MPI_COMM_WORLD);
    }
    else if (myid == np - 1) {      // last just prints result
      MPI_Recv(&ival, 1, MPI_INT, myid-1, 0, MPI_COMM_WORLD,&status);
      printf("Result: %d \n",ival);
    }

    MPI_Finalize();
    return (0);
}
