#include <mpi.h>
#include <stdio.h>

#define NMAX 10
double vec[NMAX];

int main(int argc, char *argv[])
{
    int iam, np;
    int i,n,ipoints, istart,isend;
    MPI_Status status;
    double sum=0,rcv=0;

    // initialize and get info about world and ourselves
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&np);
    MPI_Comm_rank(MPI_COMM_WORLD,&iam);

    ipoints = NMAX / np;     /* cake slice size */

    if (iam == 0) {               
      // first thread initializes array
      for (i=0;i<NMAX;i++) {
        vec[i]=i+1;
      }
      // now send a slice to all other threads
      for (n=1; n < np; n++) {
        istart = n * ipoints;
        isend = ipoints;
        if (n == np-1)
          isend = NMAX - istart;

        printf("%d - send %d to %d\n", iam,isend,n);
        MPI_Send(&vec[istart],isend, MPI_DOUBLE, n, 0, MPI_COMM_WORLD);
      }
      // first slice is ours
      for (i=0; i < ipoints; i++) {
        sum += vec[i]; 
      }
    }
    else {
      if (iam == np-1) {
        istart = iam * ipoints;
        ipoints = NMAX - istart;
      }

      // receive ipoints, store in first elements of vec
      MPI_Recv(vec,ipoints, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD,&status);
      printf("%d - received %d\n", iam,ipoints);
     
      // do our slice 
      for (i=0; i < ipoints; i++) {
        sum += vec[i]; 
      }
    }
    MPI_Reduce(&sum,&rcv,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    if (iam == 0) {               
      printf("result: %10.3e\n", rcv);
    }
    MPI_Finalize();
    return (0);
}
