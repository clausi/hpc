#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
    int myid, other, np, nmax, i, n;
    double *buffer; 
    MPI_Status status;

    // initialize and get info about world and ourselves
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&np);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    if (np != 2) {
      printf("Error - need at exactly 2 processes\n");
      MPI_Finalize();
      return -1;
    }
    nmax = 3;
    buffer = malloc(sizeof(double)*nmax);
    if (buffer == NULL) {
      printf("Error allocating buffer\n");
      MPI_Finalize();
      return -1;
    }
    if (myid==0)  {
      for (i=0;i<nmax;i++) {
        buffer[i]= i;
      }
    } else {
      for (i=0;i<nmax;i++) {
        buffer[i]= 0;
      }
    }

    printf("myid: %d  - ",myid);
    for (i=0;i<nmax;i++) {
       printf("%e ",buffer[i]);
    }
    printf("\n");
    MPI_Finalize();
    free(buffer);
    return (0);
}
