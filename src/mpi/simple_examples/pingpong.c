#include <mpi.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#define NMINELEMS 10000000
#define NMINREP 10000
#define min(a,b) (((a)<(b))?(a):(b))
int main(int argc, char *argv[])
{
    int myid, other, np, nmax, niter, i, n;
    double *buffer; 
    double secs;
    MPI_Status status;

    // initialize and get info about world and ourselves
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&np);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    if (np != 2) {
      printf("Error - need at exactly 2 processes\n");
      MPI_Finalize();
      return -1;
    }
    if (argc > 1) {
      char *p;
      errno = 0;
      long conv = strtol(argv[1], &p, 10);
      if (errno != 0 || *p != '\0' || conv > 1000000) {
        printf("usage: pingpong [nnn]\n");
        exit(-1);
      }
      nmax = strtol(argv[1], &p, 10);
    }
    else
      nmax = 1000;
    niter = min(NMINREP,NMINELEMS/nmax);
    buffer = malloc(sizeof(double)*nmax);
    if (buffer == NULL) {
      printf("Error allocating buffer\n");
      MPI_Finalize();
      return -1;
    }
    for (i=0;i<nmax;i++) {
      if (myid==0) 
        buffer[i]= 1;
      else
        buffer[i] = 0;
    }

    other = 1 - myid;
    
    MPI_Barrier(MPI_COMM_WORLD);
    if (myid==0)
      secs=MPI_Wtime();
    for (n=0;n<niter;n++) {
      if (myid == 0) {
        MPI_Send(buffer,nmax, MPI_DOUBLE, other, 0, MPI_COMM_WORLD);
        MPI_Recv(buffer,nmax, MPI_DOUBLE, other, 0, MPI_COMM_WORLD,&status);
      }
      else {     
        MPI_Recv(buffer,nmax, MPI_DOUBLE, other, 0, MPI_COMM_WORLD,&status);
        MPI_Send(buffer,nmax, MPI_DOUBLE, other, 0, MPI_COMM_WORLD);
      }
    }
    if (myid==0) {
      secs=(MPI_Wtime()-secs)/(2.0*niter);
      printf("%d elements in %10.3e usecs, bw = %10.3e MB/s (%d repeats)\n",
           nmax,secs/1.0e-6,nmax*sizeof(double)/secs/(1024*1024),niter);
    }
    MPI_Finalize();
    free(buffer);
    return (0);
}
