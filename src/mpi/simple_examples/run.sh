source ~/.bashrc
for i in 1 10 20 50 100 200 500 1000 10000 100000 1000000;
do
   mpirun -np 2 -map-by ppr:2:socket -hostfile hosts.txt pingpong $i
done
