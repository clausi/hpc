#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    int myid, np, nlen;
    char name[MPI_MAX_PROCESSOR_NAME];

    // initialize and get info about world and ourselves
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&np);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    MPI_Get_processor_name(name, &nlen);

    printf("Process %d of %d is running on %s\n", myid,np,name);
    fflush(stdout); 

    MPI_Finalize();
    return (0);
}
