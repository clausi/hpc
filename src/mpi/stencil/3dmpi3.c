#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <omp.h>

#define NINNER  (1 << 9)            // 2^k inner grid points  
#define NDIV  (1 << (PROCPOW))      // 2^p number of divions
#define NMAX  (NINNER + 2)          // 2^k+2 total points
#define NMAXX (NMAX)                // 
#define NMAXY (NMAX)                // 
#define NMAXZ (NMAX)                // 
#define NPERPROC ((NMAXX-2)/NDIV+2) // 2^(k-p)+2 storage for each process 
#define ITERMAX 10                  // max. number of iterations

double u[NPERPROC][NMAXY][NMAXZ];
double ou[NPERPROC][NMAXY][NMAXZ];

// provide an initial guess for iteration

void initialize_guess(int nproc, int numprocs) {
  int i,j,k;

#pragma omp parallel for collapse(2)
  for (i=1;i<NPERPROC-1;i++)           // inner points
    for (j=1;j<NMAXY-1;j++)   
      for (k=1;k<NMAXZ-1;k++)    
        ou[i][j][k]     =  0.0;

  for (i=1;i<NPERPROC-1;i++) {         // boundary conditions
    for (k=0;k<NMAXZ;k++) {            // bottom & top 
      ou[i][0][k]  =  1.0;
      ou[i][NMAXY-1][k] =  1.0;
    }
    for (j=0;j<NMAXY;j++) {            // front & back 
      ou[i][j][0]        =  1.0;
      ou[i][j][NMAXZ-1]  =  1.0;
    }
  }
  if (nproc == 0) {                       // left
    for (j=0;j<NMAXY;j++)    
      for (k=0;k<NMAXZ;k++)    
        ou[0][j][k]  =  1.0;
  }
  if (nproc == numprocs-1) {              // right
    for (j=0;j<NMAXY;j++)    
      for (k=0;k<NMAXZ;k++)    
        ou[NPERPROC-1][j][k]  =  1.0;
  }
}

// print grid points corresponding to proc nproc
void print_grid(int nproc, int numprocs, int n) {
  int i,j,k,ie,is;
  is = (nproc == 0) ? 0 : 1;
  ie = (nproc == numprocs-1) ? NPERPROC : NPERPROC-1;
  for (i=is;i<ie;i++)   
    for (j=0;j<NMAXY;j++)    
      for (k=0;k<NMAXZ;k++)    
        printf("%d %d %d %d %10.3e\n",n,i+nproc*(NPERPROC-2),j,k,ou[i][j][k]);
}

// just print a single point
void print_point(int i, int j, int k) {
  printf("%d %d %d %10.3e\n",i,j,k,ou[i][j][k]);
}

// update inner grid points
void update_grid(int nproc, int numprocs,MPI_Datatype alongzcut) { 
  int i,j,k,nslice;
  MPI_Status status;

 // nslice = NMAXY*NMAXZ;

  if (nproc < numprocs-1) {  // send right, receive from right
    MPI_Sendrecv(&ou[NPERPROC-2][0][0]+NMAXZ+1,1,alongzcut,nproc+1,0,
                 &ou[NPERPROC-1][0][0]+NMAXZ+1,1,alongzcut,nproc+1,1,
                                                MPI_COMM_WORLD,&status);
  }
  if (nproc > 0) {  // receive from left, send left
    MPI_Sendrecv(&ou[1][0][0]+NMAXZ+1,1,alongzcut,nproc-1,1,
                 &ou[0][0][0]+NMAXZ+1,1,alongzcut,nproc-1,0,
                                                MPI_COMM_WORLD,&status);
  }

#pragma omp parallel for collapse(2)
  for (i=1;i<NPERPROC-1;i++)          // inner points
    for (j=1;j<NMAXY-1;j++)    
      for (k=1;k<NMAXZ-1;k++)  
        u[i][j][k]=(ou[i-1][j][k]+ou[i+1][j][k]+
                    ou[i][j-1][k]+ou[i][j+1][k]+
                    ou[i][j][k-1]+ou[i][j][k+1])/6.0;

#pragma omp parallel for collapse(2)
  for (i=1;i<NPERPROC-1;i++)          // ou = u
    for (j=1;j<NMAXY-1;j++)
      for (k=1;k<NMAXZ-1;k++) 
        ou[i][j][k]=u[i][j][k];
}

// main program
int main(int argc, char *argv[]) {
  int n,np,myid,nlen;
  char name[MPI_MAX_PROCESSOR_NAME];
  double secs=0;
  MPI_Datatype alongzcut;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&np);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Get_processor_name(name, &nlen);
  if (np != NDIV) {
    printf("Error - need at exactly %d processes\n",NDIV);
    MPI_Finalize();
    return -1;
  }
  if (myid==0)
    printf("Starting on grid %d %d %d with %d per proc\n",NMAXX,NMAXY,NMAXZ,
	   NPERPROC);
//  printf("%d  process %d on %s\n",myid, myid,name);

  MPI_Type_vector(NMAXY-2,NMAXZ-2,NMAXZ,MPI_DOUBLE,&alongzcut);
  MPI_Type_commit(&alongzcut);

  initialize_guess(myid,np);
  MPI_Barrier(MPI_COMM_WORLD);
  if (myid==0)
    secs=MPI_Wtime();
  n=1;
  while (n < ITERMAX) { 
    MPI_Barrier(MPI_COMM_WORLD);
    update_grid(myid,np,alongzcut);
    n++;
  }
  if (myid==0) {
    secs=MPI_Wtime()-secs;
    printf(" time elapsed: %10.3e\n",secs);
    print_point(5,5,5); 
  }

  MPI_Finalize();
  return 0;
}
