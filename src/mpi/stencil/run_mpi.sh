source ~/.bashrc
export OMP_NUM_THREADS=2
for i in 0 1 2;
do
   make clean >& /dev/null
   make 3dmpi4 PROCPOW=$i >& /dev/null
   export num_p=$(echo "(2^$i)^2"|bc)
   mpirun_ -np $num_p -x OMP_NUM_THREADS -hostfile hosts2.txt 3dmpi4
#   mpirun_ -np $num_p -hostfile hosts.txt 3dmpi3
done
