#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <omp.h>

#define NINNER (1 << 9)             // 2^k inner grid points  
#define NDIVX  (1 << (PROCPOW))     // 2^px number of divions in x
#define NDIVY  (1 << (PROCPOW))     // 2^py number of divions in y 
#define NDIVZ  1                    // 2^pz number of divions in z
#define NPROCS (NDIVX*NDIVY*NDIVZ)  // total number of procs
#define NMAXX  (NINNER + 2)         // 
#define NMAXY  (NINNER + 2)         // 
#define NMAXZ  (NINNER + 2)         // 
#define NPERX  ((NMAXX-2)/NDIVX+2)  // 2^(k-p)+2 storage for each process 
#define NPERY  ((NMAXY-2)/NDIVY+2)  // 2^(k-p)+2 storage for each process 
#define NPERZ  ((NMAXZ-2)/NDIVZ+2)  // 2^(k-p)+2 storage for each process 
#define ITERMAX 100                 // max. number of iterations

double u[NPERX][NPERY][NPERZ];
double ou[NPERX][NPERY][NPERZ];

int right[NPROCS],left[NPROCS];
int up[NPROCS],down[NPROCS];
int front[NPROCS],back[NPROCS];

// provide an initial guess for iteration

void initialize_guess(int nproc, int numprocs) {
  int i,j,k;

#pragma omp parallel for collapse(2)
  for (i=1;i<NPERX-1;i++)           // inner points
    for (j=1;j<NPERY-1;j++)   
      for (k=1;k<NPERZ-1;k++)    
        ou[i][j][k]     =  0.0;

  if (left[nproc] == MPI_PROC_NULL) {
    for (j=0;j<NPERY;j++)    
      for (k=0;k<NPERZ;k++)    
        ou[0][j][k]  =  1.0;
  }
  if (right[nproc] == MPI_PROC_NULL) {
    for (j=0;j<NPERY;j++)    
      for (k=0;k<NPERZ;k++)    
        ou[NPERX-1][j][k]  =  1.0;
  }
  if (up[nproc] == MPI_PROC_NULL) { 
    for (i=0;i<NPERX;i++)    
      for (k=0;k<NPERZ;k++)    
        ou[i][NPERY-1][k]  =  1.0;
  }
  if (down[nproc] == MPI_PROC_NULL) {
    for (i=0;i<NPERX;i++)    
      for (k=0;k<NPERZ;k++)    
        ou[i][0][k]  =  1.0;
  }
  if (front[nproc] == MPI_PROC_NULL) {
    for (i=0;i<NPERX;i++)    
      for (j=0;j<NPERY;j++)    
        ou[i][j][0]  =  1.0;
  }
  if (back[nproc] == MPI_PROC_NULL) {
    for (i=0;i<NPERX;i++)    
      for (j=0;j<NPERY;j++)    
        ou[i][j][NPERZ-1]  =  1.0;
  }
}

// print grid points corresponding to proc nproc
void print_grid(int myid, MPI_Comm comm_cart,int n) {
  int i,j,k;
  int mycoords[3];
  MPI_Cart_coords(comm_cart,myid,3,mycoords);
  for (i=1;i<NPERX-1;i++)   
    for (j=1;j<NPERY-1;j++)    
      for (k=1;k<NPERZ-1;k++)    
        printf("%d %d %d %d %10.3e\n",n,
               i+mycoords[0]*(NPERX-2),
               j+mycoords[1]*(NPERY-2),
               k+mycoords[2]*(NPERZ-2),ou[i][j][k]);
}

// just print a single point
void print_point(int i, int j, int k) {
  printf("%d %d %d %10.3e\n",i,j,k,ou[i][j][k]);
}

// update inner grid points
void update_grid(int nproc, int numprocs,
                 MPI_Datatype cutx, MPI_Datatype cuty) {
  int i,j,k;
  MPI_Status status;

  if (right[nproc] != MPI_PROC_NULL) {  // send right, receive from right
    MPI_Sendrecv(&ou[NPERX-2][1][1],1,cutx,right[nproc],0,
                 &ou[NPERX-1][1][1],1,cutx,right[nproc],1,
                                                MPI_COMM_WORLD,&status);
  }
  if (left[nproc] != MPI_PROC_NULL) {   // receive from left, send left
    MPI_Sendrecv(&ou[1][1][1],1,cutx,left[nproc],1,
                 &ou[0][1][1],1,cutx,left[nproc],0,
                                                MPI_COMM_WORLD,&status);
  }
  if (up[nproc] != MPI_PROC_NULL) {     // send up, receive from up
    MPI_Sendrecv(&ou[1][NPERY-2][1],1,cuty,up[nproc],2,
                 &ou[1][NPERY-1][1],1,cuty,up[nproc],3,
                                                MPI_COMM_WORLD,&status);
  }
  if (down[nproc] != MPI_PROC_NULL) {  // send down, receive from down
    MPI_Sendrecv(&ou[1][1][1],1,cuty,down[nproc],3,
                 &ou[1][0][1],1,cuty,down[nproc],2,
                                                MPI_COMM_WORLD,&status);
  }

#pragma omp parallel for collapse(2)
  for (i=1;i<NPERX-1;i++)          // inner points
    for (j=1;j<NPERY-1;j++)    
      for (k=1;k<NPERZ-1;k++)  
        u[i][j][k]=(ou[i-1][j][k]+ou[i+1][j][k]+
                    ou[i][j-1][k]+ou[i][j+1][k]+
                    ou[i][j][k-1]+ou[i][j][k+1])/6.0;

#pragma omp parallel for collapse(2)
  for (i=1;i<NPERX-1;i++)          // ou = u
    for (j=1;j<NPERY-1;j++)
      for (k=1;k<NPERZ-1;k++) 
        ou[i][j][k]=u[i][j][k];
}

// main program
int main(int argc, char *argv[]) {
  int n,np,myid,me,other,nlen;
  char name[MPI_MAX_PROCESSOR_NAME];
  double secs=0;
  int periods[3],dims[3],mycoords[3];
  MPI_Datatype cutx,cuty;
  MPI_Comm comm_cart;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&np);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Get_processor_name(name, &nlen);
  if (np != NPROCS) {
    printf("Error - need at exactly %d processes\n",NPROCS);
    MPI_Finalize();
    return -1;
  }
  if (myid==0)
    printf("Starting on grid %d %d %d\n",NMAXX,NMAXY,NMAXZ);

  // create vector type with stride
  MPI_Type_vector(NPERY-2,NPERZ-2,NPERZ,MPI_DOUBLE,&cutx);
  MPI_Type_commit(&cutx);
  MPI_Type_vector(NPERX-2,NPERZ-2,NPERY*NPERZ,MPI_DOUBLE,&cuty);
  MPI_Type_commit(&cuty);
  // create cartesian grid
  for (n=0;n<3;n++) {         // no periodicity
    periods[n]=0;
  }
  dims[0]=NDIVX;
  dims[1]=NDIVY;
  dims[2]=NDIVZ;
  MPI_Cart_create(MPI_COMM_WORLD,3,dims,periods,0,&comm_cart); 
  // get my own coordinates in topology
  MPI_Cart_coords(comm_cart,myid,3,mycoords);
  printf("%d  process %d on %s: %d %d %d\n",myid, myid,name,
                mycoords[0],mycoords[1],mycoords[2]);

  me=myid;
  MPI_Cart_shift(comm_cart,0,1,&me,&other);
  right[myid] = other;
  me=myid;
  MPI_Cart_shift(comm_cart,0,-1,&me,&other);
  left[myid] = other;
  me=myid;
  MPI_Cart_shift(comm_cart,1,1,&me,&other);
  up[myid] = other;
  me=myid;
  MPI_Cart_shift(comm_cart,1,-1,&me,&other);
  down[myid] = other;
  me=myid;
  MPI_Cart_shift(comm_cart,2,1,&me,&other);
  back[myid] = other;
  me=myid;
  MPI_Cart_shift(comm_cart,2,-1,&me,&other);
  front[myid] = other;

  initialize_guess(myid,np);
  MPI_Barrier(MPI_COMM_WORLD);
  if (myid==0)
    secs=MPI_Wtime();
  n=1;
  while (n < ITERMAX) { 
    MPI_Barrier(MPI_COMM_WORLD);
    update_grid(myid,np,cutx,cuty);
    n++;
  }
  if (myid==0) {
    secs=MPI_Wtime()-secs;
    printf(" time elapsed: %10.3e\n",secs);
  }
  //print_grid(myid,comm_cart,n); 
  MPI_Finalize();
  return 0;
}
