#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <omp.h>

#define POWX 1
#define POWY 1
#define NDIVX  (1 << (POWX))      // 2^px number of divions in x
#define NDIVY  (1 << (POWY))      // 2^py number of divions in y
#define NDIVZ  (1 << 0)           // 2^pz number of divions in z

// provide an initial guess for iteration

// main program
int main(int argc, char *argv[]) {
  int n,np,myid,me,other,nlen;
  char name[MPI_MAX_PROCESSOR_NAME];
  int periods[3],dims[3],mycoords[3];
  MPI_Comm comm_cart;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&np);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Get_processor_name(name, &nlen);
  if (np != NDIVX*NDIVY*NDIVZ) {
    printf("Error - need at exactly %d processes\n",NDIVX*NDIVY*NDIVZ);
    MPI_Finalize();
    return -1;
  }
  if (myid==0)
    printf("Number of procs in direction (x,y,z): %d %d %d\n",
                                                NDIVX,NDIVY,NDIVZ);
  // create cartesian grid
  for (n=0;n<3;n++) {         // no periodicity
    periods[n]=0;
  }
  dims[0]=NDIVX;
  dims[1]=NDIVY;
  dims[2]=NDIVZ;
  MPI_Cart_create(MPI_COMM_WORLD,3,dims,periods,1,&comm_cart); 

  // get my own coordinates in topology
  MPI_Cart_coords(comm_cart,myid,3,mycoords);
  printf("%d  process %d on %s: %d %d %d\n",myid, myid,name,
                mycoords[0],mycoords[1],mycoords[2]);

  // shift from own position in all possible directions
  me=myid;
  MPI_Cart_shift(comm_cart,0,1,&me,&other);
  if (other!=MPI_PROC_NULL) {
    MPI_Cart_coords(comm_cart,other,3,mycoords);
    printf("%d  right: %d %d %d\n",myid, mycoords[0],mycoords[1],mycoords[2]);
  }
  me=myid;
  MPI_Cart_shift(comm_cart,0,-1,&me,&other);
  if (other!=MPI_PROC_NULL) {
    MPI_Cart_coords(comm_cart,other,3,mycoords);
    printf("%d  left: %d %d %d\n",myid, mycoords[0],mycoords[1],mycoords[2]);
  }
  me=myid;
  MPI_Cart_shift(comm_cart,1,1,&me,&other);
  if (other!=MPI_PROC_NULL) {
    MPI_Cart_coords(comm_cart,other,3,mycoords);
    printf("%d  up: %d %d %d\n",myid, mycoords[0],mycoords[1],mycoords[2]); 
  }
  me=myid;
  MPI_Cart_shift(comm_cart,1,-1,&me,&other);
  if (other!=MPI_PROC_NULL) {
    MPI_Cart_coords(comm_cart,other,3,mycoords);
    printf("%d  down: %d %d %d\n",myid, mycoords[0],mycoords[1],mycoords[2]); 
  }
  MPI_Finalize();
  return 0;
}
