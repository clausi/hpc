3d.c       Serial 3D stencil program. Starting point for MPI exercise
3domp.c    Same as 3d.c, but with OpenMP threads
3dmpi.c    Same as 3d.c, with domain decomposition and MPI
3dmpiomp.c Same as 3d.c, with domain decomposition and MPI and OpenMP threads
3dmpi2.c   As 3dmpi.c, only sending inner points (MPI_Vector datattype)
3dmpi3.c   Use MPI_Sendrecv
3dmpi4.c   Use cartesian topology (as an easy example see cart.c)
