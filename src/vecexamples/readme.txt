Versions:
=========
0    basis
1    restrict
2    restrict, red. var
3    restrict, red. var., loops <->
4    loops <->
5    loops <->, restrict
6    loops <->, red.var
s1   red. var,  pragma
s2   red. var,  pragma, loops <->

Small problem:
==============
K=100 M=100 N=100 NITER=1000 MDIM=100  
Flops: N*M*K*NITER = 1000 Mega * 2(ops) => 2 GFlops
Mem: 2 (matrices) * 10(K) *8 (bytes) * 100 (K) *1000 Iter = 16 G

    cluster     aws         aws
    gcc4.9     gcc5.4       icc17
0   S  1.6     S  1.9        1.18
1   V  0.98    S  1.9        0.7
2   V  0.92    S  1.9        0.4
3   V  0.43    V  0.365      0.4
4   S  1.6     S  1.9        1.16
5   V  0.49    V  0.37       0.355
6   V  0.43    
s1  V  0.92    V  1.9        0.58
s2  V  0.43    V  0.63       0.40

Bigger problem:
===============
K=100 M=2048 N=2048 NITER=1 MDIM=2048  
Flops: N*M*K=100*2*2*2(ops) MFlops = 800M
Mem: 2 (matrices) * 4(Mega)*8(byte) * 100 (K) = 6400 = 6.4 GigaByte 

   Cluster           Tecra
   MD=2048 MD=2049   2048  2049
1  28.8    5.1       10.5  11.4
5  9.9     3.3        2.2   1.7

Coeff transposed problem:
=========================
Interchange indexes i <-> j in coeff, MD=2049

1  29.0    4.3        6.2   5.2
5  28.7    4.3        6.2   5.1

Coeff transposed, blocked loop:
===============================

       Tecra
 4    4  2.9
64   64  2.6
128 128  2.2
