#include "func14.h"
// just reduction and openmd
void func14(int K, int M, int N, int MDIM, 
           double in[][MDIM], double coeff[][MDIM], double out[]) { 
  int i,j,k;
  for (k = 0; k < K; k++) {
    double sum=0;
#pragma omp simd reduction (+:sum)
    for (j = 0; j < M; j++) {
      for (i = 0; i < N; i++) {
        sum += in[i+k][j] * coeff[i][j];
      }
    }
    out[k]=sum;
  }
}
