#include "func14.h"
// reduction, openmd, loop interchanged
void func14(int K, int M, int N, int MDIM,
           double in[][MDIM], double coeff[][MDIM], double out[]) { 
  int i,j,k;
  for (k = 0; k < K; k++) {
    double sum=0;
    for (i = 0; i < N; i++) {
#pragma omp simd aligned (in,coeff:32) reduction (+:sum)
      for (j = 0; j < M; j++) {
        sum += in[i+k][j] * coeff[i][j];
      }
    }
    out[k]=sum;
  }
}
