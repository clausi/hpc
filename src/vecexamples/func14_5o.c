#include "func14.h"
// loop interchanged, restrict
void func14(int K, int M, int N, int MDIM, 
           double in[__restrict__][MDIM], double coeff[__restrict__][MDIM], 
           double out[__restrict__]) { 
  int i,j,k;
  for (k = 0; k < K; k++) {
    out[k] = 0;
    for (i = 0; i < N; i++) {
      for (j = 0; j < M; j++) {
        out[k] += in[i+k][j] * coeff[i][j];
      }
    }
  }
}
