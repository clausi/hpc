#include "func14.h"
// base version
void func14(int K, int M, int N, int MDIM, 
           double in[][MDIM], double coeff[][MDIM], double out[]) { 
  int i,j,k;
  for (k = 0; k < K; k++) {
    out[k] = 0;
    for (j = 0; j < M; j++) {
      for (i = 0; i < N; i++) {
        out[k] += in[i+k][j] * coeff[i][j];
      }
    }
  }
}
