#include<stdio.h>
#include<math.h>
#include "../utilities/timing.h"
#include "func14.h"

#define K 100
#define M 2048
#define MDIM 2048
#define N 2048
#define NITER 1

double coeff[N][MDIM] __attribute__((aligned (32)));
double in[N+K][MDIM] __attribute__((aligned (32)));
double out[K] __attribute__((aligned (32)));


int main() { 
  int i,j,k;
  double sum;
  for (j = 0; j < M; j++) {
    for (i = 0; i < N; i++) {
       coeff[i][j]=(1.0*i+j)/100.0;
    }
  }
  for (k = 0; k < K; k++) {
    for (j = 0; j < M; j++) {
      for (i = 0; i < N; i++) {
         in[i+k][j]=1.0*k;
      }
    }
  }
  for (i=0;i<NITER;i++) {
    func14(K,M,N,MDIM,in,coeff,out);
  }

  printf("%10.3e\n",out[K-1]);
/*
  for (k = 0; k < K; k++) {
    printf("%10.3e\n",out[k]);
  }
*/
  return 0;
}
