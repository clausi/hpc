#include "func14.h"
// TRANSPOSED loop interchanged, restrict, blocked
#define NB 1
#define MB 1
void func14(int K, int M, int N, int MDIM, 
           double in[__restrict__][MDIM], double coeff[__restrict__][MDIM], 
           double out[__restrict__]) { 
  int i,j,k,ib,jb;
  for (k = 0; k < K; k++) {
    double sum=0;
    for (i = 0; i < N; i+=NB) { 
      for (j = 0; j < M; j+=MB) 
        for (ib = 0; ib < NB; ib++) 
          for (jb = 0; jb < MB; jb++) 
             sum += in[i+ib+k][j+jb] * coeff[j+jb][i+ib];
    } 
    out[k]=sum;
  }
}
