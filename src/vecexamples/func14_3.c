#include "func14.h"
// restrict, reduction var, loop interchanged
void func14(int K, int M, int N, int MDIM,
           double in[__restrict__][MDIM], double coeff[__restrict__][MDIM], 
           double out[__restrict__]) { 
  int i,j,k;
  for (k = 0; k < K; k++) {
    double sum = 0.0;
    for (i = 0; i < N; i++) {
      for (j = 0; j < M; j++) {
        sum += in[i+k][j] * coeff[i][j];
      }
    }
    out[k]=sum;
  }
}
