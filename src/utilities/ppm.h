/**
 * PPM Image format
 * 
 * A pixel can be expressed as a float rgb or hsv triplet
 *
 * The file on disk itself is standard PPM in RGB (24bit), so 
 * information is lost on saving!
 */

#ifndef _PPM_INCLUDED
#define _PPM_INCLUDED

typedef enum {TYPE_RGB,TYPE_HSV} CSPACE;  // image type

typedef struct {
    int nx;              // width
    int ny;              // height
    float *data_rh;      // array with red / hue values
    float *data_gs;      // array with green / saturation values
    float *data_bv;      // array with blue / value values
    CSPACE cspace;
} IMAGE;

// create a hsv image with given dimensions (and some calculated pattern)
IMAGE *makeimage(int nx, int ny);

// read a rgb image from file
IMAGE *readimage(char *filename); 

// save image to file (converts output pixels to rgb if necessary)
void saveimage(const IMAGE *img, char *filename);

// set image pixel (i,j) to (rh, gs, bv)
void setpixel(IMAGE *img, int i, int j, float rh, float gs, float bv);

// convert image to hsv
void image2hsv(IMAGE *img);

// scale hue to range [min,max]
void scalehue(IMAGE *img, float min, float max);

// free image data in image
void freeimage(IMAGE *img);

#endif
