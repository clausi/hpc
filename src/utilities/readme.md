This directory contains some utilities to be used by other programs.

* timing routines
* PPM image format (see [PPM format](http://people.cs.clemson.edu/~dhouse/courses/405/notes/ppm-files.pdf))
