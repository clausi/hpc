module timer_c
  use, intrinsic :: ISO_C_BINDING
  use precision
  implicit none
    type(c_ptr) :: module_ptr
    ! interface to external C functions
    interface
      subroutine start_timer_c() bind(C, name='start_timer')
        use, intrinsic :: ISO_C_BINDING
      end subroutine start_timer_c

      real (c_double) function stop_timer_c () bind(C, name='stop_timer')
        use, intrinsic :: ISO_C_BINDING
      end function stop_timer_c 
    end interface      
contains
    ! fortan interfaces with no ISO_C_BINDING types
    
    ! start the timer
    subroutine start_timer ()
      call start_timer_c ! call C version
    end subroutine start_timer

    ! stop timer and return CPU seconds as double
    real(kind=dp) function stop_timer ()
      stop_timer = stop_timer_c() ! call C version but return fortran double precision
    end function stop_timer

end module