module ppm_c
  use, intrinsic :: ISO_C_BINDING
  implicit none
    type(c_ptr) :: module_ptr
    ! interface to external C functions
    interface
      
      function makeimage_c (nx, ny) result(handle) bind(C, name='makeimage')
        use, intrinsic :: ISO_C_BINDING
        type(c_ptr) :: handle  
        integer(c_int), value :: nx, ny
      end function makeimage_c 

      subroutine save_image_c(handle, str) bind(C, name='saveimage')
        use, intrinsic :: ISO_C_BINDING
        type(c_ptr), value :: handle
        character(kind=c_char), dimension(*), intent(in) :: str
      end subroutine save_image_c  

      subroutine free_image_c(handle) bind(C, name='freeimage')
        use, intrinsic :: ISO_C_BINDING
        type(c_ptr), value :: handle
      end subroutine free_image_c   

      subroutine set_pixel_c(handle, i, j, rh, gs, bv) bind(C, name='setpixel')
        use, intrinsic :: ISO_C_BINDING
        type(c_ptr), value :: handle
        integer(c_int), value :: i, j
        real(c_float), value :: rh, gs, bv
      end subroutine set_pixel_c  

      subroutine scale_hue_c(handle, min, max) bind(C, name='scalehue')
        use, intrinsic :: ISO_C_BINDING
        type(c_ptr), value :: handle
        real(c_float), value :: min, max
      end subroutine scale_hue_c  

    end interface

contains
    ! fortan interface with no ISO_C_BINDING types
    
    ! make an image of size nx*ny and return pointer to image in handle
    subroutine makeimage (handle, nx, ny)
      use, intrinsic :: ISO_C_BINDING
      integer, pointer ::  handle
      integer, intent(in) ::  nx, ny

      type(c_ptr) :: ptr
      ptr = makeimage_c(nx, ny)     ! returns c pointer
      call c_f_pointer(ptr, handle)  ! convert to fortran pointer
    end subroutine makeimage

    ! save image in handle to file fname
    subroutine saveimage (handle, fname)
      use, intrinsic :: ISO_C_BINDING
      integer, pointer :: handle
      character(len=*), intent(in) :: fname

      type(c_ptr) :: ptr
      ptr = c_loc(handle)
      call save_image_c(ptr, fname//char(0))
    end subroutine saveimage

    ! free mem associated with image
    subroutine freeimage (handle)
      use, intrinsic :: ISO_C_BINDING
      integer, pointer :: handle

      type(c_ptr) :: ptr
      ptr = c_loc(handle)
      call free_image_c(ptr)
      nullify(handle)
    end subroutine freeimage

    ! save image in handle to file fname
    subroutine setpixel (handle, i, j, rh, gs, bv)
      use, intrinsic :: ISO_C_BINDING
      integer, pointer :: handle
      integer, intent(in) ::  i, j
      real(4), value :: rh, gs, bv

      type(c_ptr) :: ptr
      ptr = c_loc(handle)
      call set_pixel_c(ptr, i, j, rh, gs, bv)
    end subroutine setpixel

    ! scale hue to [min,max]
    subroutine scalehue (handle, hmin, hmax)
      use, intrinsic :: ISO_C_BINDING
      integer, pointer :: handle
      real(4), value :: hmin, hmax

      type(c_ptr) :: ptr
      ptr = c_loc(handle)
      call scale_hue_c(ptr, hmin, hmax)
    end subroutine scalehue

end module