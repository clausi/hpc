
subroutine print_test(img)
    integer, pointer :: img
    write(*,*) img
end subroutine print_test

program test_timer
    use timer_c
    use ppm_c
    use precision
    implicit none
    ! explicit interface required, img is pointer
    interface
        subroutine print_test(img)
        implicit none 
        integer, pointer :: img
        end subroutine print_test
    end interface

    integer, pointer :: ptr
    integer :: i
    real(kind=dp) :: secs    
    
    call start_timer()
    call makeimage(ptr, 100, 100)
    call scalehue(ptr, 0.0, 100.0);
    secs=stop_timer()
    print '("secs = ",e30.20)', secs
    ! should print 100, as first element in img is nx    
    write(*,*) ptr
    call print_test(ptr)
    do i=1,80
        call setpixel(ptr,i,50,1.0,1.0,1.0)
    enddo
    call saveimage(ptr, "plunder.ppm")
end program test_timer
