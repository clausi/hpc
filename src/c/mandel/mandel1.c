#include <stdio.h>
#include "mandel.h"

/**
 * standard mandelbrot implementation
 */
double mandelbrot(IMAGE *img, int maxiters, 
                  REAL x1, REAL x2, REAL y1, REAL y2) { 
  int i,j;
  REAL dx = (x2-x1)/img->nx, dy = (y2-y1)/img->ny;
  REAL cr,ci,zr,zi,zr2,zi2,tzr,tzi;
  double flops=0;
  for (j = 0; j < img->ny; ++j) {
    for (i = 0; i < img->nx; ++i) {
      cr = x1+dx*i;
      ci = y1+dy*j;
      zr = 0;
      zi = 0;
      int count = 0;
      while ((++count < maxiters)) {
        zr2 = zr*zr;
        zi2 = zi*zi;
        if (zr2+zi2 >= 4)
          break;
        tzr = zr2-zi2+cr;
        tzi = 2*zr*zi+ci;
        zr = tzr;
        zi = tzi;
      }
      color_pixel(img,i,j,count,maxiters);
      flops += 4+count*8;
    }
  }
  img->cspace=TYPE_HSV;
  return flops;
}
