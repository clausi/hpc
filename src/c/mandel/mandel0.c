#include <complex.h>
#include "mandel.h"

/**
 * standard mandelbrot implementation
 */
double mandelbrot(IMAGE *img, int maxiters, 
                  REAL x1, REAL x2, REAL y1, REAL y2, REAL enhance) { 
  int i,j;
  REAL dx = (x2-x1)/img->nx, dy = (y2-y1)/img->ny;
  double flops=0;
  float hue, sat, val;
  sat = 1.0; 
  for (j = 0; j < img->ny; ++j) {
    for (i = 0; i < img->nx; ++i) {
      REAL complex c = x1+dx*i + (y1+dy*j) * I;
      REAL complex z = 0;
      int count = 0;
      while ((++count < maxiters) && 
         (creal(z)*creal(z)+cimag(z)*cimag(z) < 4.0)) {
        z = z*z+c;
      }
      float x  = ((float)count)/maxiters;
      hue = enhance > 0.0 ? scaled_logit(x)*240 : x*240;  
      val = count == maxiters ? 0.0 : 1.0;
      setpixel(img, i, j, hue, sat, val);
      flops += 4+count*8;
    }
  }
  img->cspace=TYPE_HSV;
  return flops;
}
