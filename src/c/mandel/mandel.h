#include "../../utilities/ppm.h"

#define REAL double

double mandelbrot(IMAGE *img, int maxiters,
                  REAL x1, REAL x2, REAL y1, REAL y2, float enhance);
