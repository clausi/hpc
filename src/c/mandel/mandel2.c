#include <stdio.h>
#include <stdlib.h>
#include "mandel.h"

#define VECSIZE 4 

REAL zr[VECSIZE] __attribute__((aligned(32)));
REAL zi[VECSIZE] __attribute__((aligned(32)));
REAL cr[VECSIZE] __attribute__((aligned(32)));
REAL ci[VECSIZE] __attribute__((aligned(32)));
REAL zr2[VECSIZE] __attribute__((aligned(32)));
REAL zi2[VECSIZE] __attribute__((aligned(32)));
REAL tzr[VECSIZE] __attribute__((aligned(32)));
REAL tzi[VECSIZE] __attribute__((aligned(32)));
int count[VECSIZE] __attribute__((aligned(32)));
int calc[VECSIZE] __attribute__((aligned(32)));


/**
 * rewrite in vector form: iterate various pixels at once
 */
double mandelbrot(IMAGE *img, int maxiters, 
                  REAL x1, REAL x2, REAL y1, REAL y2) { 
  int i,j,k;
  if (img->nx % VECSIZE != 0) {
    printf("Fatal error: image x size must be multiple of %d\n",VECSIZE);
    exit(-1);
  }
  REAL dx = (x2-x1)/img->nx, dy = (y2-y1)/img->ny;
  double flops=0;
  for (j = 0; j < img->ny; j++) {
    for (i = 0; i < img->nx; i+=VECSIZE) {
      for (k = 0; k < VECSIZE; k++) {
         cr[k]=x1+dx*(i+k);  // real and imaginary part of c
         ci[k]=y1+dy*j;
         zr[k]=0.0;          // real and imaginary part of z
         zi[k]=0.0;
         count[k]=0;         // iteration count
         calc[k]=1;          // flag for each k to see if calc continues
      }
      int niter = 0;
      while ((niter++ < maxiters)) {
        for (k = 0; k < VECSIZE; k++) {
          zr2[k] = zr[k]*zr[k];
          zi2[k] = zi[k]*zi[k];
        }
        int csum=0;
        //calc[k] will be 0 if iteration stops
        for (k=0; k < VECSIZE; k++) {
          calc[k] = (zr2[k]+zi2[k] < 4) ? 1 : 0;
          csum+=calc[k];
        }
        if (csum==0) //all are 0
          break;
        for (k = 0; k < VECSIZE; k++) {
          tzr[k] = zr2[k]-zi2[k]+cr[k];
          tzi[k] = 2.0*zr[k]*zi[k]+ci[k];
          zr[k] = calc[k] ? tzr[k] : zr[k];
          zi[k] = calc[k] ? tzi[k] : zi[k];
          count[k] += calc[k];
        }
      } 
      flops += 4+niter*VECSIZE*8;
      for (k = 0; k < VECSIZE; k++) {
        color_pixel(img,i+k,j,count[k],maxiters);
      }
    }
  }
  img->cspace=TYPE_HSV;
  return flops;
}
