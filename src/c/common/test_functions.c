#include <stdio.h>
#include "functions.h"

int main(void)
{
  double eps=1.0e-4;
  printf("%e\n",logit(0.0, eps));
  printf("%e\n",logit(1.0, eps));
  printf("%e\n",scaled_logit(0.0, eps));
  printf("%e\n",scaled_logit(1.0, eps));
  printf("%e\n",logit(0.1, eps));
  return 0;
}
