
// logit function on [0,1] with poles "cut" at eps and 1-eps
double logit(double x, double eps);
// a scaled logit [0,1] -> [0,1]
double scaled_logit(double x,  double eps);
