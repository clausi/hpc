#include "functions.h"
#include <math.h>
#include <stdlib.h>

double logit(double x, double eps) {
  if (fabs(x) < eps) {
    return log(eps/(1.0-eps));
  }
  if (fabs(1-x) < eps) {
    return log((1.0-eps)/eps);
  }
  return log(x/(1.0-x));
}
double scaled_logit(double x, double eps) {
  double ymin = logit(0.0, eps);
  double ymax = logit(1.0, eps);
  return (logit(x, eps)-ymin)/(ymax-ymin);
}
