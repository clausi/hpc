      subroutine scale(ndim, a, va, vb)
      integer ndim,i
      real*8 a, va(ndim), vb(ndim)
      do i = 1, ndim
        va(i) = a*va(i) + vb(i)
      enddo
      end subroutine scale
      
      program iterate
      IMPLICIT NONE
      INTEGER ITERATIONS, NSIZE
      PARAMETER (ITERATIONS = 100000000, NSIZE = 64)
C
      type mytype
        sequence
C        character(len=16) :: cpad1
        real*8 :: va(NSIZE)
        character(len=32) :: cpad2
        real*8 :: vb(NSIZE)
      end type mytype

      type (mytype) :: data
      REAL*8 a 
      INTEGER i
      INTEGER*8 orig
      REAL start,finish,secs,flops
      read(*,*) a
C     where is our data
      orig=LOC(data)
      print '("&va % 32 = ",I3," &vb % 32 = ",I3)',
     &          MOD(LOC(data%va),32),MOD(LOC(data%vb),32)
c     initialize
      DO i=1,NSIZE
        data%va(i) = 0.25*(i-1)
        data%vb(i) = (i-1)
      ENDDO
C
      call cpu_time(start)
      do i=1,ITERATIONS
        call scale(NSIZE,a,data%va,data%vb)
      enddo
      call cpu_time(finish)
C
      secs=finish-start;
      flops = (ITERATIONS*2.0/secs)*NSIZE
      print '("Flops = ",e15.7," va = ",e15.7)',flops,data%va(NSIZE)
      end program iterate
