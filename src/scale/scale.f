      subroutine scale(NDIM, a, va, vb)
      integer NDIM,i
      real*8 a, va(NDIM), vb(NDIM)
      do i = 1, NDIM
        va(i) = a*va(i) + vb(i)
      enddo
      end subroutine scale
