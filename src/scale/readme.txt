Note: on the cluster use gcc 4.9:

scl enable devtoolset-3 bash
gcc -O3 -ftree-vectorize -fopt-info-vec-optimized example14.c

see https://fossies.org/linux/gcc/gcc/doc/optinfo.texi


AWS micro:
==========
gcc 5.4
iterate1 6.395e+09  1.066e-13 #   8  16
iterate2 1.273e+10  1.066e-13 #   8  16
iterate3 1.271e+10  1.066e-13 #   8  16
iterate4 1.981e+10  1.066e-13 #   0   0
iterate5 2.755e+10  1.066e-13 #   0   0

icc 17
iterate1 6.716e+09  1.064e-13 #   8  16
iterate2 9.449e+09  1.064e-13 #   8  16
iterate3 9.429e+09  1.064e-13 #   8  16
iterate4 1.648e+10  1.064e-13 #   0   0
iterate5 2.752e+10  1.064e-13 #   0   0

Tecra
======
gcc 5.4
iterate1 6.664e+09  1.066e-13 #   8  16
iterate2 1.189e+10  1.066e-13 #   8  16
iterate3 1.189e+10  1.066e-13 #   8  16
iterate4 2.159e+10  1.066e-13 #   0   0
iterate5 2.279e+10  1.066e-13 #   0   0

Cluster CICA
============
gcc 4.9 
iterate1 3.600e+09  1.064e-13 #   8  16
iterate2 5.050e+09  1.064e-13 #   8  16
iterate3 5.085e+09  1.064e-13 #   8  16
iterate4 6.283e+09  1.064e-13 #   0   0
iterate5 6.791e+09  1.064e-13 #   0   0
