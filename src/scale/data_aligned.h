#define REAL double
#define ARRAY_SIZE 64
/**
 * Our data structure
 */
struct
{
   char pad1;
   REAL va[ARRAY_SIZE] __attribute__((aligned (32)));
   char pad2;
   REAL vb[ARRAY_SIZE] __attribute__((aligned (32)));
   char pad3;
} data;

