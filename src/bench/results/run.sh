BINFILE=../openmp/ompmemspeed
for i in 1 2 4 8 16 32;
do
  export OMP_NUM_THREADS="$i"
  echo OMP_NUM_THREADS: $OMP_NUM_THREADS
  $BINFILE
done
