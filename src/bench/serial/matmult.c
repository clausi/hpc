#include <stdio.h>
#include <stdlib.h>

double* make_sqmatrix(int nmax) {
  int i,k;
  double *m = malloc((size_t)(nmax*nmax*sizeof(double)));
  for (i=0;i<nmax;i++) {
    for (k=0;k<nmax;k++) {
      m[i+nmax*k]=1.0/(i+k+2);
    }
  }
  return m;
}

/* 
   c = a*b 
*/
void matmult(int nmax, double* a, double* b, double* c) { 
  int i,k,n;
  for (i=0; i<nmax; i++) {
     for (k=0; k<nmax; k++) {
       double sum=0.0;
       for (n=0;n < nmax; n++) {
         sum += a[i+nmax*n]*b[n+nmax*k];
       }
       c[i+nmax*k]=sum;
     }
  }
}

/*
  main program
*/
int main() {
    int nmax=1024, niter=10, i,k,n;
    double *a,*b,*c;
    a = make_sqmatrix(nmax);
    b = make_sqmatrix(nmax);
    c = make_sqmatrix(nmax);

    for (n=0; n < niter; n++) {
      matmult(nmax,a,b,c);
      for (i=0;i<nmax;i++) {
        for (k=0;k<nmax;k++) {
          b[i+nmax*k]=c[i+nmax*k];
        }
      }
    }
    printf("%e \n",c[0]);
    return 0;
}

