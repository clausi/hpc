	PROGRAM TST
	PARAMETER (N1K=1024,NMAX=N1K,NDIM=NMAX+1,NITER=10)
	REAL*8 A(NDIM,NDIM),B(NDIM,NDIM),C(NDIM,NDIM)
        write(*,*) 'Memory req.: (MB)',3.0d0*8*NDIM**2/N1K/N1K
        write(*,*) 'G-Flops:',NITER*2.0d0*NMAX**3/N1K/N1K/N1K
C	preset matrices
	DO I=1,NMAX
	  DO K=1,NMAX
	    A(I,K)=1.0d0/dble(I+K)
	    B(I,K)=A(I,K)
          ENDDO
	ENDDO
	DO N=1,NITER
          CALL MATMULT(A,NMAX,NDIM,B,NMAX,NDIM,C,NMAX,NDIM)
	  DO I=1,NMAX
	    DO K=1,NMAX
	      B(I,K)=C(I,K)
            ENDDO
	  ENDDO
	ENDDO
        write(*,*) C(1,1)
	END
C
	SUBROUTINE MATMULT(A,MMAX,LDA,B,NMAX,LDB,C,KMAX,LDC)
C         C (MxK) = A(MxN) * B(NxK)
	REAL*8 A(LDA,*),B(LDB,*),C(LDC,*),SUM
	DO I=1,MMAX
	  DO K=1,KMAX
            SUM=0.0d0
	    DO N=1,NMAX
              SUM=SUM+A(I,N)*B(N,K)
	    ENDDO
            C(I,K)=SUM
	  ENDDO
	ENDDO
        END
	
	SUBROUTINE MATMULT2(A,MMAX,LDA,B,NMAX,LDB,C,KMAX,LDC)
C         C (MxK) = A(MxN) * B(NxK)
	REAL*8 A(LDA,*),B(LDB,*),C(LDC,*),TMP
	DO K=1,KMAX
	  DO I=1,MMAX
            C(I,K)=0.0D0
	  ENDDO
	ENDDO
	DO K=1,KMAX
	  DO N=1,NMAX
	    TMP=B(N,K)
	    DO I=1,MMAX
              C(I,K)=C(I,K)+A(I,N)*TMP
	    ENDDO
	  ENDDO
	ENDDO
        END
