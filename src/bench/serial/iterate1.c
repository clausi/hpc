#define REAL double
#define LOOP_COUNT 32 
#define MAXFLOPS_ITERS 800000000
#define FLOPS_ARRAY_SIZE 1024 
#define FLOPS MAXFLOPS_ITERS*(LOOP_COUNT*2.0)

#include<stdio.h>

//REAL fa[FLOPS_ARRAY_SIZE] __attribute__((__aligned__(64)));
//REAL fb[FLOPS_ARRAY_SIZE] __attribute__((__aligned__(64)));
REAL fa[FLOPS_ARRAY_SIZE] __attribute__((aligned(64)));
REAL fb[FLOPS_ARRAY_SIZE] __attribute__((aligned(64)));

int main() { 
    int i,j,k;
    REAL a = 1.1;
    printf("FLOPS = %10.3e\n",FLOPS);
    for(i=0; i<FLOPS_ARRAY_SIZE; i++) {
        fa[i] = (REAL)i + 0.1;
        fb[i] = (REAL)i + 0.2;
    }	

    for(j=0; j<MAXFLOPS_ITERS; j++) { 
        for(k=0; k<LOOP_COUNT; k++) { 
            fa[k] = a * fa[k] + fb[k];
        }
    }
    return 0;
}
