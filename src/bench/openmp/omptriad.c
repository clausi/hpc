#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <mm_malloc.h>
#include "../../utilities/timing.h"



float   *x_cpu;                  // Pointer to CPU arrays

// main program that executes in the CPU
int main(int argc, char *argv[])
{
    double secs;

    int     i,k,opwd;
    float   fpmops;
    float   mflops;
    int     words     = 10000000;      // E Number of words in arrays
    int     repeats   = 25000;        // R Number of repeat passes 
    size_t  size_x;
    float   newdata = 0.999999f;
    float   a = 0.000020f;
    float   b = 0.981f;

    printf("\n");
    printf("  %2ld Byte  Ops/   Repeat    Seconds   MFLOPS       First   All\n",sizeof(float));
    printf("    Words  Word   Passes                         Results  Same\n\n");

    size_x = words * sizeof(float);

    // Allocate arrays for host CPU
    x_cpu = (float *)malloc(size_x);
    if (x_cpu  == NULL) {
	printf("Error allocating arrays.\n");
	exit(1);
    }

    // Data for array
#pragma omp parallel for
    for (i=0; i<words; i++) {
	x_cpu[i] = newdata;
    }
    start_timer();
    for (k=0; k < repeats; k++) {              
#pragma omp parallel for
	for(i=0; i<words; i++) {
	    x_cpu[i] = (x_cpu[i]+a)*b;
	}
    }
    secs=stop_timer();
    opwd = 2;
    fpmops = opwd * (float)words;
    mflops = (float)repeats * fpmops / 1000000.0f / (float)secs;

    // Print results
    printf("%9d %5d %8d %10.6f %8.0f ", words, opwd, repeats, secs, mflops);
    // Cleanup
    _mm_free(x_cpu);
    words = words * 10;
    repeats = repeats / 10;
    if (repeats < 1) repeats = 1; 
    printf("\n");

    return 0;
}
