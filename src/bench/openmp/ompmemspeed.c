 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
 #include <math.h>
 #include <malloc.h>
 #include <mm_malloc.h>
 #include "../../utilities/timing.h"

//  #define Integer64Bits
  #define Integer32Bits

  double secs = 0.0;
  double runSecs = 0.1;   
  int n1;

  double * xd;
  double * yd;
  float  * xs;
  float  * ys;

  #ifdef Integer64Bits
   long long  * xi;   
   long long  * yi;   
  #else
   int    * xi; 
   int    * yi;
  #endif


  void checkTime()
  {
     if (secs < runSecs)
     {
          if (secs < runSecs / 8.0)
          {
                n1 = n1 * 10;
          }
          else
          {
                n1 = (int)(runSecs * 1.25 / secs * (double)n1+1);
          }
      }
  }


int main()
{
    
    int passes[25];
    int allocMem[25];

    double  ramKB,ramGB;
    int useMem;   

  
    float  sums;
    float  ones = 1.0;
    float  zeros = 0;
      
    int g, i, m, j, nn;
    int kd, ks, ki, mem;

    #ifdef Integer64Bits
     long long  sumi;   
    #else
     int  sumi;
    #endif
   
    int  zeroi = 0;
    int onei = 1;
             
    int inc;
    double sumd, mbpsd, mbpss, mbpsi;
    double oned = 1.0;
    double zerod = 0;
    double memMB;
    
    int runs = 10; // 2 MB;
    
    printf("\n");

    ramGB = 4;
    ramKB = ramGB * 1000000;

    allocMem[0] = 2;  // KB two arrays
    for (i=1; i<23; i++)
    {
        allocMem[i] = allocMem[i-1] * 2;
    }


    passes[0] =      250;   //      4 KB
    for (i=1;i<=22;i++) {
      passes[i]=passes[i-1]*2;    //  8, 16 , 32, 64, 128,256
    }

    if (ramKB >    14000) runs = 11;
    if (ramKB >    30000) runs = 12;
    if (ramKB >    60000) runs = 13;
    if (ramKB >   120000) runs = 14;
    if (ramKB >   250000) runs = 15;
    if (ramKB >   500000) runs = 16;
    if (ramKB >  1000000) runs = 17;
    if (ramKB >  1500000) runs = 18;
    if (ramKB >  3500000) runs = 19;
    if (ramKB >  7500000) runs = 20;
    if (ramKB > 15500000) runs = 21;
    if (ramKB > 31500000) runs = 22;
    if (ramKB > 63500000) runs = 23;

    useMem = allocMem[runs - 1];

    xd = (double *)_mm_malloc(useMem * 1024, 16);
    if (xd == NULL)
    {
       printf("Error allocating memory\n");
       exit(1);
    }

    yd = (double *)_mm_malloc(useMem * 1024, 16);
    if (yd == NULL)
    {
       printf("Error allocating memory\n");
       _mm_free(xd);
       exit(1);
    }

    float *xs = (float *) xd;
    float *ys = (float *) yd;

    #ifdef Integer64Bits
     long long *xi = (long long *) xd;
     long long *yi = (long long *) yd;     
    #else
     int   *xi = (int *) xd;
     int   *yi = (int *) yd;     
    #endif

    printf("  Memory   x[m]=x[m]+s*y[m] Int  x[m]=x[m]+y[m]         x[m]=y[m]\n"); 

    #ifdef Integer64Bits
     printf("  KBytes    Dble   Sngl  Int64   Dble   Sngl  Int64   Dble   Sngl  Int64\n");   
    #else
     printf("  KBytes    Dble   Sngl  Int32   Dble   Sngl  Int32   Dble   Sngl  Int32\n");   
    #endif

    printf("    Used    MB/S   MB/S   MB/S   MB/S   MB/S   MB/S   MB/S   MB/S   MB/S\n\n");
       
    for (j=0; j<runs; j++)
    {
        kd = passes[j];
        
        nn = 6400000 / kd;
        if (nn < 1) nn = 1;
        
        ks = kd * 2;

        #ifdef Integer64Bits   
         ki = kd; 
        #else
         ki = kd * 2; 
        #endif
        
        memMB = (double)kd * 16.0 / 1000000;
        mem = (int)((double)kd * 16.0 / 1000);

        inc = 4;
          
        n1 = nn;
        
        do
        {
            sumd = 1.00001;
            for (m=0; m<kd; m++)
            {
                  xd[m] = oned;
                  yd[m] = oned;
            }          
            start_timer();
            for (i=0; i<n1; i++)
            {
#ifdef UNROLL_LOOPS
#pragma omp parallel for
                for (m=0; m<kd; m=m+inc)
                {
                   xd[m]   = xd[m]   + sumd * yd[m];
                   xd[m+1] = xd[m+1] + sumd * yd[m+1];
                   xd[m+2] = xd[m+2] + sumd * yd[m+2];
                   xd[m+3] = xd[m+3] + sumd * yd[m+3];
                 }
#else
#pragma omp parallel for
                for (m=0; m<kd; m++)
                {
                   xd[m]   = xd[m]   + sumd * yd[m];
                }
#endif
            }
            secs=stop_timer();
            checkTime();
        }
        while (secs < runSecs);
        
        mbpsd = (double)n1 * memMB / secs;
        printf("%8d %7d", mem, (int)mbpsd);

                 
        n1 = nn;
        do
        {
            sums = 1.0001;
            for (m=0; m<ks; m++)
            {
                  xs[m] = ones;
                  ys[m] = ones;
            }          
            start_timer();
            for (i=0; i<n1; i++)
            {
#pragma omp parallel for
                for (m=0; m<ks; m=m+inc)
                {
                   xs[m]   = xs[m]   + sums * ys[m];
                   xs[m+1] = xs[m+1] + sums * ys[m+1];
                   xs[m+2] = xs[m+2] + sums * ys[m+2];
                   xs[m+3] = xs[m+3] + sums * ys[m+3];
                }
            }
            secs=stop_timer();
            checkTime();
        }
        while (secs < runSecs);

        mbpss =  (double)n1 * memMB / secs;
        printf("%7d", (int)mbpss);
         
        n1 = nn;
        do
        {
            sumi = 0;
            for (m=0; m<ki; m++)
            {
                  xi[m] = zeroi;
                  yi[m] = zeroi;
            }          
            yi[ki-1] = onei;
                    
            start_timer();
            for (i=0; i<n1; i++)
            {
#pragma omp parallel for
               for (m=0; m<ki; m=m+inc)
               {
                   xi[m]   = xi[m]   + sumi + yi[m];
                   xi[m+1] = xi[m+1] + sumi + yi[m+1];
                   xi[m+2] = xi[m+2] + sumi + yi[m+2];
                   xi[m+3] = xi[m+3] + sumi + yi[m+3];
                }
            }
            secs=stop_timer();
            checkTime();
        }
        while (secs < runSecs);
                  
        mbpsi = (double)n1 * memMB / secs;
        printf("%7d", (int)mbpsi);
            
        n1 = nn;
        do
        {
            for (m=0; m<kd; m++)
            {
                  xd[m] = zerod;
                  yd[m] = oned;
            }          
            start_timer();
            for (i=0; i<n1; i++)
            {
#pragma omp parallel for
                for (m=0; m<kd; m=m+inc)
                {
                      xd[m]   = xd[m] + yd[m];
                      xd[m+1] = xd[m+1] + yd[m+1];
                      xd[m+2] = xd[m+2] + yd[m+2];
                      xd[m+3] = xd[m+3] + yd[m+3];
                }
            }
            secs=stop_timer();
            checkTime();
        }
        while (secs < runSecs);
    
        sumd = xd[1];

        mbpsd = (double)n1 * memMB / secs;
        printf("%7d", (int)mbpsd);

        n1 = nn;
        do
        {
            for (m=0; m<ks; m++)
            {
                  xs[m] = zeros;
                  ys[m] = ones;
            }          
            start_timer();
            for (i=0; i<n1; i++)
            {
#pragma omp parallel for
                for (m=0; m<ks; m=m+inc)
                {
                      xs[m] = xs[m] + ys[m];
                      xs[m+1] = xs[m+1] + ys[m+1];
                      xs[m+2] = xs[m+2] + ys[m+2];
                      xs[m+3] = xs[m+3] + ys[m+3];
                }
            }
            secs=stop_timer();
            checkTime();
        }
        while (secs < runSecs);
            
        sums = xs[1];
        mbpss = (double)n1 * memMB / secs;
        printf("%7d", (int)mbpss);

        n1 = nn;
        do
        {
            for (m=0; m<ki; m++)
            {
                  xi[m] = zeroi;
                  yi[m] = onei;
            }          
                    
            start_timer();
            for (i=0; i<n1; i++)
            {
#pragma omp parallel for
                for (m=0; m<ki; m=m+inc)
                {
                     xi[m] = xi[m] + yi[m];
                     xi[m+1] = xi[m+1] + yi[m+1];
                     xi[m+2] = xi[m+2] + yi[m+2]; 
                     xi[m+3] = xi[m+3] + yi[m+3]; 
                }
            }
            secs=stop_timer();
            checkTime();
        }
        while (secs < runSecs);
     
        sumi = xi[1];
        mbpsi = (double)n1 * memMB / secs;
        printf("%7d", (int)mbpsi);
        
        memMB = (double)kd * 8 / 1000000;
    
        n1 = nn;
        do
        {              
            for (m=0; m<kd+inc; m++)
            {
                  xd[m] = zerod;
                  yd[m] = m;
            }        
            start_timer();
            for (i=0; i<n1; i++)
            {
#pragma omp parallel for
                for (m=0; m<kd; m=m+inc)
                {
                   xd[m] = yd[m];
                   xd[m+1]=yd[m+1];
                   xd[m+2]=yd[m+2];
                   xd[m+3]=yd[m+3];
                }
            }
            secs=stop_timer();
            checkTime();
        }
        while (secs < runSecs);
            
        sumd = xd[kd-1]+1;
        mbpsd = (double)n1 * memMB / secs;
        printf("%7d", (int)mbpsd);
        
        n1 = nn;
        do
        {
            for (m=0; m<ks+inc; m++)
            {
                  xs[m] = zeros;
                  ys[m] = m;
            }        
            start_timer();
            for (i=0; i<n1; i++)
            {
#pragma omp parallel for
                for (m=0; m<ks; m=m+inc)
                {
                   xs[m] = ys[m];
                   xs[m+1]=ys[m+1];
                   xs[m+2]=ys[m+2];
                   xs[m+3]=ys[m+3];
                }
            }
            secs=stop_timer();
            checkTime();
        }
        while (secs < runSecs);
        
        sums = xs[ks-1]+1;    
        mbpss = (double)n1 * memMB / secs;
        printf("%7d", (int)mbpss);

        n1 = nn;
        do
        {
            for (m=0; m<ki+inc; m++)
            {
                  xi[m] = zeroi;
                  yi[m] = m;
            }
                    
            start_timer();
            for (i=0; i<n1; i++)
            {
#pragma omp parallel for
                for (m=0; m<ki; m=m+inc)
                {
                   xi[m] = yi[m];
                   xi[m+1]=yi[m+1];
                   xi[m+2]=yi[m+2];
                   xi[m+3]=yi[m+3];
                }
            }
            secs=stop_timer();
            checkTime();
        }
        while (secs < runSecs);

        sumi = xi[ki-1]+1;
        mbpsi = (double)n1 * memMB / secs;
        printf("%7d\n", (int)mbpsi);

    }
    _mm_free(yd);
    _mm_free(xd);
    return 0;
}
