program iterate
  implicit none
  integer, parameter :: dp = kind(1.d0)
  integer, parameter :: ITERATIONS = 10000000, NSIZE = 64
  integer(8), parameter :: NBYTE4 = 32, NBYTE8 = 64
  real(kind=dp) :: powsum

  type simple_struct
    sequence ! stored in memory as in code
    character(len=1) :: cpad1
    real(kind=dp) :: x
    character(len=90) :: cpad2
    real(kind=dp) :: y
  end type simple_struct

  type (simple_struct), dimension(NSIZE) :: data_vec

  real(kind=dp) :: a 
  integer :: i, k
  real(kind=dp) start, finish, secs, flops, expected, err

  a = 1.000000001d0
    ! print data alignment
  print '("&x % 64 = ",I3," &y % 64 = ",I3," dist = ",I3)',   &
               mod(loc(data_vec(1)%x), NBYTE8), mod(loc(data_vec(1)%y), NBYTE8), &
               loc(data_vec(1)%y) - loc(data_vec(1)%x)

  ! initialize
  do i = 1, NSIZE
    data_vec(i)%x = (i-1)*0.25d0
    data_vec(i)%y = (i-1)
  enddo
  !
  call cpu_time(start)
  do k = 1, ITERATIONS
    do i = 1, NSIZE
      data_vec(i)%x = a*data_vec(i)%x + data_vec(i)%y
    enddo
  enddo
  call cpu_time(finish)

  expected = (a**ITERATIONS)*(NSIZE-1)*0.25d0 + data_vec(NSIZE)%y*powsum(a,ITERATIONS-1)
  err = abs(data_vec(NSIZE)%x-expected)/expected; 

  secs=finish-start;
  flops = 2.0d0*ITERATIONS*NSIZE/secs
  print '("Secs = ",e15.7," Flops = ",e15.7," Err = ",e15.7)', secs, flops, err

end program iterate
