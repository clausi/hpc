subroutine scale(ndim, a, va, vb)
  implicit none
  integer, parameter :: dp = kind(1.d0)

  integer :: ndim
  real(kind=dp) :: a
  real(kind=dp), dimension(ndim) :: va, vb

  integer :: i

  do i = 1, ndim
    va(i) = a*va(i) + vb(i)
  enddo
end subroutine scale