program iterate
  implicit none
  integer, parameter :: dp = kind(1.d0)
  integer, parameter :: ITERATIONS = 1000000, NSIZE = 64
  integer(8), parameter :: NBYTE4 = 32, NBYTE8 = 64
  real(kind=dp) :: powsum

  type simple_struct
    sequence ! stored in memory as in code
    character(len=1) :: cpad1
    real(kind=dp) :: x
    character(len=1) :: cpad2
    real(kind=dp) :: y
  end type simple_struct

  type struct_of_vec
    sequence ! stored in memory as in code
    character(len=1) :: cpad1
    real(kind=dp), dimension(NSIZE) :: va
    character(len=1) :: cpad2
    real(kind=dp), dimension(NSIZE) :: vb
  end type struct_of_vec

  !dir$ attributes align: 64:: veca
  real(kind=dp), dimension(NSIZE) :: veca
  type (simple_struct), dimension(NSIZE) :: vec_struct
  type (struct_of_vec) :: data

  real(kind=dp) :: a 
  integer :: i
  real(kind=dp) start, finish, secs, flops, expected, err

  !read(*,*) a
  a = 1.000000001d0
  ! print data alignment
  print '("&va % 64 = ",I3," &vb % 64 = ",I3," vec ",I3)',   &
               mod(loc(data%va), NBYTE8), mod(loc(data%vb), NBYTE8), mod(loc(veca), NBYTE8)
  ! initialize
  do i = 1, NSIZE
    data%va(i) = (i-1)*0.25d0
    data%vb(i) = (i-1)
    vec_struct(i)%x = (i-1)*0.25d0
    vec_struct(i)%y = (i-1)
  enddo
!
  call cpu_time(start)
  do i = 1, ITERATIONS
    call scale(NSIZE, a, data%va, data%vb)
  enddo
  call cpu_time(finish)

  expected = (a**ITERATIONS)*(NSIZE-1)*0.25d0 + data%vb(NSIZE)*powsum(a,ITERATIONS-1)
  err = abs(data%va(NSIZE)-expected)/expected; 

  secs=finish-start;
  flops = (ITERATIONS*2.0/secs)*NSIZE
  print '("Secs = ",e15.7," Flops = ",e15.7," Err = ",e15.7)', secs, flops, err

  call cpu_time(start)
  do i = 1, ITERATIONS
    call scale(NSIZE, a, vec_struct%x, vec_struct%y)
  enddo
  call cpu_time(finish)

  expected = (a**ITERATIONS)*(NSIZE-1)*0.25d0 + data%vb(NSIZE)*powsum(a,ITERATIONS-1)
  err = abs(data%va(NSIZE)-expected)/expected; 

  secs=finish-start;
  flops = (ITERATIONS*2.0/secs)*NSIZE
  print '("Secs = ",e15.7," Flops = ",e15.7," Err = ",e15.7)', secs, flops, err

end program iterate
