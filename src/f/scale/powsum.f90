function powsum(x, n) result(pow)
  implicit none
  integer, parameter :: dp = kind(1.d0)

  real(kind=dp) :: x, pow
  integer :: n
  if (x /= 1.0d0) then
    pow =  (x**(n+1)-1)/(x-1.0d0)
  else
    pow = n+1;
  end if
end function powsum
