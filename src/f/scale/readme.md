# Compiler options

floating point math
https://gcc.gnu.org/wiki/FloatingPointMath

https://www.itu.dk/~sestoft/bachelor/IEEE754_article.pdf

## simple_struct, character(len=90) :: cpad2

* use OPT=-O1 and OPT=-O3 (no -march option)
  * -O1 uses SSE2,                              2.8 GFlops
  * -O3 same, but unrolled,                     2.9 Gflops
* use -march=native
  * -O1 uses AVX                                3.1 GFlops
    135 vmulsd	(%rax), %xmm1, %xmm0
    136 vaddsd	104(%rax), %xmm0, %xmm0
    137 vmovsd	%xmm0, (%rax)
  * -O3 uses fused add multiply                 3.2 GFlops
    193	vmovsd	(%rax), %xmm0
    194	vfmadd213sd	104(%rax), %xmm1, %xmm0
    195 addq	$120, %rax
    196 vmovsd	%xmm0, -120(%rax)
  * -O3 -funroll-loops unrolls loops ;)         5.2 GFlops

