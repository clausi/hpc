module vecfuncs1
    implicit none
    integer, parameter :: dp = kind(1.d0)
    contains
        function vecsum(va, ndim) result(sum)
            integer ndim
            real(kind=dp), dimension(ndim) :: va
            real(kind=dp) :: sum
            integer :: i
            sum = 0.0d0
            do i = 1, ndim
                sum = sum + va(i)
            end do
        end function 

        function sigmoid(x) result(y)
            real*8 :: x, y
            y =  1.0d0 / (1.0d0 + exp(-x))
        end function
end module vecfuncs1
