program some_loops
    use vecfuncs2
    implicit none
    integer, parameter :: ndim = 1024
    real(kind=dp), dimension(ndim) :: va, vb, vc
    integer :: i

    call random_number(va)
    call random_number(vb)

    do i = 1, ndim
        vc(i) = 3.0*va(i) + vb(i)
    end do
    write(*,*) vecsum(vc, ndim)

    !$omp parallel do simd
    do i = 1, ndim
        vc(i) = sigmoid(va(i))
    end do
    write(*,*) vecsum(vc, ndim)
end program