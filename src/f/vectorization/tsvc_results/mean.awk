#!/usr/bin/awk -f
BEGIN {
  min = 1000
  max = 0
  total_novec = 0
  total_vec = 0
}
NR > 1 {
   total_novec += $2;
   total_vec += $4;
   speedup  = $2/$4; 
   if (speedup > max) { 
     max = speedup
     max_name = $1;
   } 
   if (speedup < min) { 
     min = speedup;
     min_name = $1;
   } 
   C += log(speedup);  
   D++; 
 }

 END {
   print "Total Novec: ",total_novec;
   print "Total Vec: ",total_vec;
   print "Speedup Min: ",min, " at ",min_name;
   print "Speedup Max: ",max, " at ",max_name;
   print "Speedup Geom. mean: ",exp(C/D);
}
