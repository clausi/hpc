echo ""
echo "=== GNU -march=native ==="
join --header -j 1 -o 1.1,1.2,2.1,2.2 gcc_native_std_novec.out gcc_native_std_vec.out > gcc_native_std.out
cat gcc_native_std.out | awk -f mean.awk 

echo "ok"
echo "=== GNU -march=native relaxed ==="
join --header -j 1 -o 1.1,1.2,2.1,2.2 gcc_native_relaxed_novec.out gcc_native_relaxed_vec.out > gcc_native_relaxed.out
cat gcc_native_relaxed.out | awk -f mean.awk

echo ""
echo "=== Intel -xHost ==="
join --header -j 1 -o 1.1,1.2,2.1,2.2 icc_host_std_novec.out icc_host_std_vec.out > icc_host_std.out
cat icc_host_std.out | awk -f mean.awk

echo ""
echo "=== Intel -xHost relaxed ==="
join --header -j 1 -o 1.1,1.2,2.1,2.2 icc_host_relaxed_novec.out icc_host_relaxed_vec.out > icc_host_relaxed.out
cat icc_host_relaxed.out | awk -f mean.awk

echo ""
echo "=== KNL Intel -xknl ==="
join --header -j 1 -o 1.1,1.2,2.1,2.2 icc_knl_std_novec.out icc_knl_std_vec.out > icc_knl_std.out
cat icc_knl_std.out | awk -f mean.awk 
