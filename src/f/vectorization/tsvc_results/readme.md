# DATA for TLSC benchmark on Huelva cluster

run . ./calc.sh to show results

## Compiler settings

File names are descriptive:

* "gcc" and "icc" are the compiler names (version gcc 7.3 and icc 2019)
* "novec" ans "vec" mean vectorization disabled/enabled. 
* "native" is `-march=native` on GNU, "host" is `-xHost` on Intel
* "relaxed" is `-ffast-math` on GNU and `-fp-model fast=2` on Intel
* for icc, "knl" is -xknl and executed on KNL 


