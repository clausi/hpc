module functions
    use precision
    implicit none
    ! function_eps is global so it can easily be adjusted from the outside
    real(kind=vp) :: functions_eps = 1.0d-4
    contains
        ! logit function on [0,1] with poles "cut"
        function logit(x) result(y)
            real(kind=vp) :: x, y
            ! if close to pole, cut by value at eps or 1-eps
            if (abs(x) < functions_eps) then
                y = log(functions_eps/(1.0d0-functions_eps))
                return
            end if
            if (abs(1-x) < functions_eps) then
                y = log((1.0-functions_eps)/functions_eps)
                return
            end if
            y = log(x/(1.0d0-x))
        end function logit
        ! a scaled logit [0,1] -> [0,1]
        function scaled_logit(x) result(y)
            real(kind=vp) :: x, y
            real(kind=vp) :: ymin, ymax
            ymin=logit(real(0.0d0,kind=vp))
            ymax=logit(real(1.0d0,kind=vp))
            y = (logit(x)-ymin)/(ymax-ymin)
        end function scaled_logit
end module functions