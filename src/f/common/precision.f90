module precision
    implicit none
    !single precision, 32 BYTES
    integer, parameter :: sp = selected_real_kind(6, 37)
    !double precision 64 BYTES
    integer, parameter :: dp = selected_real_kind(15, 307)
    !quadruple precision 128 BYTES
    integer, parameter :: qp = selected_real_kind(33, 4931)
    !vp indicates "variable precision", it may vary 
    integer, parameter :: vp = selected_real_kind(15, 307)
    !Fortran 2008:
    !integer, parameter :: sp = REAL32
    !integer, parameter :: dp = REAL64
    !integer, parameter :: qp = REAL128
end module precision
