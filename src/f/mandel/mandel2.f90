module mandel
  use precision
  use functions
  implicit none
  integer, parameter:: VECSIZE=4
  real(kind=vp), dimension(VECSIZE) :: zr,zi,cr,ci,zr2,zi2,tzr,tzi
  integer, dimension(VECSIZE) :: count 
  logical, dimension(VECSIZE) :: calc

  contains
    subroutine whoami ()
      write (*,*) 'mandel2, precision:',vp
    end subroutine whoami
    !
    ! iterate various values in parallel
    !
    function mandelbrot(img, nx, ny, maxiters, x1, x2, y1, y2, enhance) result(flops)
      use ppm_c
      implicit none
      integer, pointer :: img
      integer :: nx, ny, maxiters
      real(kind=vp) :: x1, x2, y1, y2
      real(kind=sp) :: enhance
      real(kind=sp) :: flops

      integer :: i, j, k, ncnt
      real(4) :: hue, sat=1.0, val 
      real(kind=vp) :: dx, dy
      integer :: csum

      flops = 0
      if (enhance .gt. 0.0) then
        functions_eps = enhance
      end if
      dx = (x2-x1)/nx
      dy = (y2-y1)/ny

      do j = 0, ny-1
        do i = 0, nx-1, VECSIZE
          do k = 1, VECSIZE
            cr(k) = x1 + dx*(i+k-1)
            ci(k) = y1 + dy*j
            zr(k) = 0
            zi(k) = 0
            zr2(k) = 0
            zi2(k) = 0
            ncnt = 0
            count(k)=0;       
          enddo

          do while (ncnt < maxiters)
            do k = 1, VECSIZE
              zr2(k) = zr(k)**2
              zi2(k) = zi(k)**2
            enddo
            csum = 0
            do k = 1, VECSIZE
              !variable = merge(value if true, value if false, condition)
              calc(k) = merge(.true., .false., zr2(k)+zi2(k) < 4) 
              csum = csum + merge(1,0,calc(k))
            enddo
            if (csum .eq. 0) then
              exit
            end if
            do k = 1, VECSIZE
              tzr(k) = zr2(k) - zi2(k) + cr(k)
              tzi(k) = 2 * zr(k) * zi(k) + ci(k)
              zr(k) = merge(tzr(k), zr(k), calc(k))
              zi(k) = merge(tzi(k), zi(k), calc(k))
              count(k) = count(k) + merge( 1, 0, calc(k));
            enddo
            ncnt = ncnt + 1
          enddo 
          do k = 1, VECSIZE
            ! hue is linear or enhanced by logit function
            hue = real(count(k), kind=sp)/maxiters
            if (enhance .gt. 0.0) then
              hue = real(scaled_logit(real(hue,kind=vp)) * 240.0, kind=sp)
            else
              hue = hue * 240
            endif
            ! all within set are black (val = 0)
            if (count(k) == maxiters) then
              val = 0.0
            else
              val = 1.0
            end if
            call setpixel (img, i+k-1, j, hue, sat, val)
          enddo
          flops = flops + 4 + ncnt*VECSIZE*8; ! todo: use count(k)??
        enddo
      enddo
    end function mandelbrot
end module mandel
