# Program Mandelbrot

This is the Fortran version of the Mandelbrot program. We have 
one main program which is linked to different modules 
(mandel0..mandel3), each containing a different version of the code

* mandel0 - standard implementation with complex variables
* mandel1 - complex variables expanded to real
* mandel2 - inner loop iterating over array of points
* mandel3 - using AVX intrinsics

All programs can be compiled either using `REAL*4` or `REAL*8` for the
iteration variables, the precision can be changed in module
common/precision.f90 (set variable `vp` - "variable precision").

As intrinsics are not available in Fortran, we call a C function 
from Fortran and the intrinsics are called from the C code. 
The usage of intrinsics and interfacing between C and Fortran 
are more advanced subjects. Intrinsic calls have been
implemented for float and double precision (`mandel3_f.c` and `mandel3_d.c`).

ifort/icc:

    mandel0 secs =  0.519E+01 Flops =  0.187E+10
    mandel1 secs =  0.437E+01 Flops =  0.221E+10
    mandel2 secs =  0.203E+01 Flops =  0.483E+10
    mandel 3:
    nthreads = 1  secs =  0.950E+00 Flops =  0.105E+11 (speedup = 5,46)
    nthreads = 2  secs =  0.587E+00 Flops =  0.170E+11
    nthreads = 4  secs =  0.363E+00 Flops =  0.274E+11
    nthreads = 8  secs =  0.244E+00 Flops =  0.409E+11
    nthreads = 12 secs =  0.184E+00 Flops =  0.540E+11
    nthreads = 24 secs =  0.116E+00 Flops =  0.862E+11 (Open MP 8.8)

    Total Speedup = 44.7
    
        nthreads = 24 mandel3 secs =  0.115E+00 Flops =  0.864E+11 

gfortran/gcc 

    mandel0 secs =  0.338E+01 Flops =  0.286E+10
    mandel1 secs =  0.413E+01 Flops =  0.234E+10
    mandel2 secs =  0.338E+01 Flops =  0.290E+10
    mandel3 secs =  0.985E+00 Flops =  0.101E+11
