!
! this module just interfaces the fortran with the c version of "mandelbrot"
! so we can call it fortran style from the main program. Note that we could 
! as well include the interface in the main program and call the c code directly
!
module mandel
  use precision
  use, intrinsic :: ISO_C_BINDING

  implicit none
  interface
    ! double precision version
    ! float mandelbrot(IMAGE *img, int maxiters, double x1, double x2, double y1, double y2) 
    function mandelbrot_d (img, maxiters, x1, x2, y1, y2, enhance) result(flops) bind(C, name='mandelbrot_d')
      use, intrinsic :: ISO_C_BINDING
      type(c_ptr), value :: img
      integer(c_int), value :: maxiters
      real(c_double), value :: x1,x2,y1,y2
      real(c_float), value :: enhance
      real(c_float) :: flops  
    end function mandelbrot_d 
    ! float version
    ! float mandelbrot(IMAGE *img, int maxiters, float x1, float x2, float y1, float y2) 
    function mandelbrot_f (img, maxiters, x1, x2, y1, y2, enhance) result(flops) bind(C, name='mandelbrot_f')
      use, intrinsic :: ISO_C_BINDING
      type(c_ptr), value :: img
      integer(c_int), value :: maxiters
      real(c_float), value :: x1,x2,y1,y2
      real(c_float), value :: enhance
      real(c_float) :: flops  
    end function mandelbrot_f 

  end interface

  contains
    subroutine whoami ()
      write (*,*) 'mandel3, precision:',vp
    end subroutine whoami
    
    ! our fortran version
    function mandelbrot(img, nx, ny, maxiters, x1, x2, y1, y2, enhance) result(flops)
      implicit none
      integer, pointer :: img
      integer :: nx, ny, maxiters
      real(kind=vp) :: x1, x2, y1, y2
      real(kind=sp) :: sx1, sx2, sy1, sy2
      real(kind=dp) :: dx1, dx2, dy1, dy2

      real(kind=sp) :: enhance
      real(kind=sp) :: flops
      type(c_ptr) :: ptr
      ! prepare pointer for C
      ptr = c_loc(img)
      ! and call C routine
      if (vp .eq. sp) then
        sx1=real(x1, kind=sp)
        sx2=real(x2, kind=sp)
        sy1=real(y1, kind=sp)
        sy2=real(y2, kind=sp)
        flops = mandelbrot_f(ptr, maxiters, sx1, sx2, sy1, sy2, enhance) 
      else if (vp .eq. dp) then
        dx1=x1
        dx2=x2
        dy1=y1
        dy2=y2
        flops = mandelbrot_d(ptr, maxiters, dx1, dx2, dy1, dy2, enhance) 
      else
        write(*,*) 'error: unknown precision kind:',vp
        stop -1
      end if
    end function mandelbrot
end module mandel
