#include <stdio.h>
#include <stdlib.h>
#include <x86intrin.h>
#include <omp.h>

#include "../../utilities/ppm.h"
#include "../../c/common/functions.h"

#ifndef INSTRSET
  #if defined ( __AVX512VL__ ) && defined ( __AVX512BW__ ) && defined ( __AVX512DQ__ ) 
    #define INSTRSET 10
  #elif defined ( __AVX512F__ ) || defined ( __AVX512__ ) // || defined ( __AVX512ER__ ) 
    #define INSTRSET 9
  #elif defined ( __AVX2__ )
    #define INSTRSET 8
  #else 
    #define INSTRSET 0
  #endif // instruction set defines
#endif

#define MANDEL_NAME mandelbrot_f
#define REAL float

#if INSTRSET >= 9 // AVX512

  #define REGSIZE 16  // 16 * 32 = 512  
  #define REG_TYPE __m512
  #define MASK_TYPE __mmask16 

  // available also for I < 9 (for _mm256)
  #define REG_MUL _mm512_mul_ps
  #define REG_ADD _mm512_add_ps
  #define REG_SET1 _mm512_set1_ps
  #define REG_SET _mm512_set_ps
  #define REG_SUB _mm512_sub_ps
  #define REG_STORE _mm512_store_ps
  
  // only available I9P, even for _mm256
  #define I9P_MASK_CMP_MASK _mm512_cmp_ps_mask
  #define I9P_MASKED_MOVE _mm512_mask_mov_ps 
  #define I9P_MASKED_ADD _mm512_mask_add_ps

#elif INSTRSET == 8 //AVX

  #define REGSIZE 8  // 8 * 32 = 256  
  #define REG_TYPE __m256
  
  #define REG_MUL _mm256_mul_ps
  #define REG_ADD _mm256_add_ps
  #define REG_SET1 _mm256_set1_ps
  #define REG_SET _mm256_set_ps
  #define REG_SUB _mm256_sub_ps
  #define REG_STORE _mm256_store_ps

  // AVX functions for masked compare and ternary ops
  #define WMASK_CMP _mm256_cmp_ps
  #define REG_OR _mm256_or_ps
  #define REG_AND _mm256_and_ps
  #define REG_ANDNOT _mm256_andnot_ps 

  // AVX function used for comparison
  #define REG_MOVEMASK _mm256_movemask_ps

#endif

//REAL cr[REGSIZE] __attribute__((aligned(32)));

#include "mandel3.h"
