!
! Main program for mandelbrot set. This program is linked
! with different versions of module "mandel". Precision "vp"
! can be set in ../common/precision.f90
!
program mainmandel
    use precision
    use timer_c
    use ppm_c
    use mandel

    implicit none
    ! image dimensions: must me multiple of vecsize in mandel3!
    integer, parameter:: nx = 1024, ny = 1024 

    real(kind=sp) :: flops
    real(kind=dp) :: secs
    real(kind=vp) :: x_center, y_center, delta, x1, x2, y1, y2
    integer :: maxiter
    integer, pointer :: img

    ! see if the right module is used
    call whoami() 

    ! read input data
    write(*,*) 'enter x_center,y_center,delta,maxiter:'
    read(*,*) x_center, y_center, delta, maxiter
    write(*,*) 'values:',x_center,y_center,delta,maxiter

    ! image coordinates
    x1 = x_center - 0.5*delta
    x2 = x_center + 0.5*delta
    y1 = y_center - 0.5*delta
    y2 = y_center + 0.5*delta

    ! initialize image
    call makeimage(img, nx, ny);

    ! main computation
    call start_timer()
    flops = mandelbrot(img,nx,ny,maxiter,x1,x2,y1,y2,1.0e-2)
    secs = stop_timer()
    
    ! save & print
    call saveimage(img,"mandel.ppm");
    print '(" secs = ",e10.3, " Flops = ", e10.3)', secs, flops/secs
end program mainmandel