module mandel
  use precision
  use functions
  implicit none
  contains
    subroutine whoami ()
      write (*,*) 'mandel1, precision:',vp
    end subroutine whoami
    !
    ! complex calculations with real variables
    !
    function mandelbrot(img, nx, ny, maxiters, x1, x2, y1, y2, enhance) result(flops)
      use ppm_c
      implicit none
      integer, pointer :: img
      integer :: nx, ny, maxiters
      real(kind=vp) :: x1, x2, y1, y2
      real(kind=sp) :: enhance
      real(kind=sp) :: flops

      integer :: i, j, ncnt
      ! image hue, saturation and value
      real(kind=sp) :: hue, sat = 1.0, val 
      real(kind=vp) :: dx, dy
      real(kind=vp) :: cr, ci, zr, zi, zr2, zi2, tzr, tzi

      flops = 0
      if (enhance .gt. 0.0) then
        functions_eps = enhance
      end if
      dx = (x2-x1)/nx
      dy = (y2-y1)/ny
      
      do j = 0, ny-1
        do i = 0, nx-1
            cr = x1 + dx*i
            ci = y1 + dy*j
            zr = 0
            zi = 0
            zr2 = 0
            zi2 = 0
            ncnt = 0
            do while ((ncnt < maxiters))
                zr2 = zr**2
                zi2 = zi**2
                if (zr2+zi2 >= 4.0) then
                  exit
                end if
                tzr = zr2 - zi2 + cr
                tzi = 2 * zr * zi + ci
                zr = tzr
                zi = tzi
                ncnt = ncnt + 1
            enddo 
            ! hue is linear or enhanced by logit function
            hue = real(ncnt, kind=sp)/maxiters
            if (enhance .gt. 0.0) then
              hue = real(scaled_logit(real(hue,kind=vp)) * 240.0, kind=sp)
            else
              hue = hue * 240
            endif
            ! all within set are black (val = 0)
            if (ncnt == maxiters) then
              val = 0.0
            else
              val = 1.0
            end if
            call setpixel (img, i, j, hue, sat, val)
            flops = flops + 4 + ncnt*8
        enddo
      enddo
    end function mandelbrot
end module mandel
