/**
 * AVX intrinsics version of mandelbrot. This function uses macros  
 * to permit AVX & AVX512 for float and double precision. See mandel3_f.c
 * and mandel3_d.c for the definition of these macros. 
 * 
 * For a SSE version check
 * https://www.cs.uaf.edu/2011/fall/cs441/lecture/10_04_SSE_movemask.html
 * 
 * Also check https://github.com/vectorclass/version2 vor a nice
 * vector class.
 */
float MANDEL_NAME(IMAGE *img, int maxiters, 
                  REAL x1, REAL x2, REAL y1, REAL y2, float enhance) { 

  float sat = 1.0;
  float flops=0.0;

  if (img->nx % REGSIZE != 0) {
    printf("Fatal error: image x size must be multiple of %d\n",REGSIZE);
    exit(-1);
  }

  REG_TYPE vc1 = REG_SET1(1.0); //vector constant 1.0
  REG_TYPE vc2 = REG_SET1(2.0); //vector constant 2.0
  REG_TYPE vc4 = REG_SET1(4.0); //vector constant 4.0

  #if REGSIZE == 4
    REG_TYPE v_idx = REG_SET(3.0,2.0,1.0,0.0);
  #elif REGSIZE == 8
    REG_TYPE v_idx = REG_SET(7.0,6.0,5.0,4.0,3.0,2.0,1.0,0.0);
  #elif REGSIZE == 16
    REG_TYPE v_idx = REG_SET(15.0,14.0,13.0,12.0,11.0,10.0,9.0,8.0,
                              7.0,6.0,5.0,4.0,3.0,2.0,1.0,0.0);
  #else
    printf("Fatal error: unknown register size %d\n",REGSIZE);
    exit(-2);
  #endif
  
  REAL dx = (x2-x1)/img->nx;
  REAL dy = (y2-y1)/img->ny;

  REG_TYPE vdx = REG_SET1(dx);                //vdx = dx
  REG_TYPE vdy = REG_SET1(dy);                //vdx = dx
#pragma omp parallel for reduction(+:flops) schedule(dynamic) collapse(2)
  for (int j = 0; j < img->ny; j++) {
    for (int i = 0; i < img->nx; i+=REGSIZE) {
    
      REG_TYPE vcr = REG_SET1((REAL) i);    //vcr = i
      vcr = REG_ADD(vcr,v_idx);             //vcr += 0 1 2 3 [4 5 6 7]
      vcr = REG_MUL(vdx,vcr);               //vcr*=vdx
      vcr = REG_ADD(REG_SET1(x1),vcr);      //vcr+=x1

      REG_TYPE vci = REG_SET1((REAL) j);    //vci = i
      vci = REG_MUL(vdy,vci);               //vci*=vdy
      vci = REG_ADD(REG_SET1(y1),vci);      //vci+=y1

      REG_TYPE vzr = REG_SET1(0.0);           //vzr = 0
      REG_TYPE vzi = REG_SET1(0.0);           //vzi = 0
      
      REG_TYPE cnt = REG_SET1(0.0);           //cnt = 0

      int niter = 0;
      while ((niter++ < maxiters)) {
        REG_TYPE vzr2 = REG_MUL(vzr,vzr);  //vzr2 = vzr*vzr
        REG_TYPE vzi2 = REG_MUL(vzi,vzi);  //vzi2 = vzi*vzi
        REG_TYPE vlen = REG_ADD(vzr2,vzi2);//vlen = vzr2+vzi2

        #if INSTRSET >= 9
          // newer CPUs have a compare that returns a mask
          if (I9P_MASK_CMP_MASK(vlen,vc4,1) == 0) break;
        #else
          // cond = vlen-vc4 ( < 0 for points in set) 
          REG_TYPE cond = REG_SUB(vlen,vc4);
          // movemask sets bits to 1 from most significant bit in
          // cond. This is the float sign, 0 is positive, 1 negative (IEEE_754)
          // So, if all are 0, all cond elements are positive (out of set)
          if (REG_MOVEMASK(cond)==0) break;
        #endif

        REG_TYPE vzrzi = REG_MUL(vzr,vzi); //vzrzi = vzr*vzi 
        REG_TYPE tzi = REG_MUL(vc2,vzrzi); //tzi = vc2*vzrzi 
        tzi = REG_ADD(tzi,vci);          //tzi += vci 
 
        REG_TYPE tzr = REG_SUB(vzr2,vzi2); //tzr = vzr2-vzi2
        tzr = REG_ADD(tzr,vcr);          //tzr += vcr
        
         
        // The following is a vectorized conditional:
        // MASK has all bits set in element i if vlen[i]<vc4[i]
        // vzr[i] = (vlen[i] < 4) ? tzr[i] : vzr[i] 
        // vzr =  (MASK & tzr) | (!MASK & vzr)
        // Example: vlen = [1 5] -> MASK = [1 0] !MASK = [0 1]
        // vzr =  ([1 0] & [tzr0 tzr1]) | ([0 1] & [vzr0 vzr1]) =
        //        ([tzr0 0] | [0 vzr1]) = [tzr0 vzr1]
        #if INSTRSET >= 9
          MASK_TYPE MASK = I9P_MASK_CMP_MASK(vlen,vc4,_CMP_LT_OS);
          vzr = I9P_MASKED_MOVE(vzr, MASK, tzr);
          vzi = I9P_MASKED_MOVE(vzi, MASK, tzi);
          cnt = I9P_MASKED_ADD(cnt, MASK, cnt, vc1);
        #else
          REG_TYPE MASK = WMASK_CMP(vlen,vc4,_CMP_LT_OS);
          vzr=REG_OR(REG_AND(MASK,tzr),REG_ANDNOT(MASK,vzr));
          vzi=REG_OR(REG_AND(MASK,tzi),REG_ANDNOT(MASK,vzi));
          REG_TYPE cnt1 = REG_ADD(cnt,vc1);  //cnt1 = cnt + 1
          cnt=REG_OR(REG_AND(MASK,cnt1),REG_ANDNOT(MASK,cnt));
        #endif
      }
      flops = flops + 4 + niter*REGSIZE*8;
      // store our counter to cr
      // REG_STORE(cr,cnt);
      // set pixel
      for (int k = 0; k < REGSIZE; k++) {
        double x  = cnt[k]/maxiters;
        float hue = enhance > 0.0 ? scaled_logit(x,enhance)*240 : x*240;  
        float val = cnt[k] == maxiters ? 0.0 : 1.0;
        setpixel(img, i+k, j, hue, sat, val);
      }
    }
  }
  return flops;
}
