! See https://www.researchgate.net/publication/220261458_A_Remarkable_Example_of_Catastrophic_Cancellation_Unraveled
! The exact result is -0.8273960599 , see for example http://ex-cs.sist.ac.jp/~tkouya/try_mpfr.html 
! with the expression
! 333.75*33096^6 + 77617^2*(11*77617^2*33096^2-33096^6-121*33096^4-2)+5.5*33096^8+77617/(2*33096)
!
program WTF
  real(kind=4) :: y4, a4 = 77617, b4 = 33096
  real(kind=8) :: y8, a8 = 77617, b8 = 33096
  real(kind=16) :: y16, a16 = 77617, b16 = 33096

  y4 = 333.75*b4**6 + a4**2*(11*a4**2*b4**2-b4**6-121*b4**4-2)+5.5*b4**8+a4/(2*b4)
  y8 = 333.75*b8**6 + a8**2*(11*a8**2*b8**2-b8**6-121*b8**4-2)+5.5*b8**8+a8/(2*b8)
  y16 = 333.75*b16**6 + a16**2*(11*a16**2*b16**2-b16**6-121*b16**4-2)+5.5*b16**8+a16/(2*b16)
  
  print '("y4 = ",e30.20," y8 = ",e30.20," y16 = ",e30.20)', y4, y8, y16
end program WTF