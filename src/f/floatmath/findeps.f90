! simple program to find machine epsilon for given precision
! what happens when compiled with -ffast-math ?
program findeps
  real :: eps
  integer :: i
  eps = 1
  i = 0
  do 
    eps = 0.5 * eps
    i = i+1 
    if (eps + 1 == 1) then
      exit
    end if
  end do
  print '("Eps = ",e30.20, " Iterations = ", i10)', eps, i
end program findeps
