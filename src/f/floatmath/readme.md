# Some examples for floating point math

For a general discussion on this subject, see excellent paper of 
[Goldberg](https://www.itu.dk/~sestoft/bachelor/IEEE754_article.pdf)

## Find machine epsilon

A simple program to calculate the machine *epsilon*. Use
`findeps.f90` to calculate the value of *epsilon* for single
and double precision on your machine.

What happens when you compile with `-ffast-math`? What compiler
options are activated with this option?

## The "WTF" program

Run `wtf.f90`. Interpret the results :)

## Paranoia

Program to check floating point math by [William Kahan](https://en.wikipedia.org/wiki/William_Kahan)