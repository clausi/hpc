program pardo1
    use omp_lib
    implicit none
    integer, parameter:: nmax = 9
    integer :: i, ithread
    real :: a(nmax), b(nmax)
!
    write(*,*) 'Initialization'
    do i = 1, nmax
        a(i) = i
        b(i) = 0
        print '(" a(",i2,") = ",e10.3)', i, a(i)
    enddo
!
    write(*,*) 'Calculation'
    !$omp parallel private(ithread)
    ithread = omp_get_thread_num()
    print '(" thread:",i2)', ithread
    !$omp do schedule(static,2)
    do i = 2, nmax
        b(i) = 0.5*(a(i-1) + a(i))
        print '(" thread: ",i2," b(",i2,") = ",e10.3)', ithread, i, b(i)
    enddo
    !$omp end do
    !$omp end parallel
!
    write(*,*) 'Results'
    do i = 2, nmax
        print '(" b(",i2,") = ",e10.3)', i, b(i)
    enddo
end program pardo1
