program raceok
  implicit none
  integer*8 :: i, imax = 100000000
  real*8 :: sum = 0.0

  !$omp parallel do reduction(+:sum)
  do i = 1, imax
    sum = sum + 1
  enddo  
  print '(" sum = ",e14.9)', sum
end program raceok
