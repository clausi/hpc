module work
    use omp_lib
    implicit none
    contains
        subroutine work1
            print '(" work1 thread: ",i2)', omp_get_thread_num()
        end subroutine
        subroutine work2
            print '(" work2 thread: ",i2)', omp_get_thread_num()
        end subroutine
        subroutine work3
            print '(" work3 thread: ",i2)', omp_get_thread_num()
        end subroutine
end module work

program partasks
    use omp_lib
    use work
    implicit none
    !$omp parallel
      !$omp single
        print '(" single thread: ",i2)', omp_get_thread_num()
        !$omp task
          call work1
        !$omp end task

        !$omp task
          call work2
        !$omp end task

        !$omp task
          call work3
        !$omp end task
      !$omp end single
    !$omp end parallel

end program partasks
