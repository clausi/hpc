program hello_omp
    !$ use omp_lib
    integer :: ithread = 0
    logical :: use_openmp = .false.
    !$ use_openmp = .true.
    if (use_openmp) then
        print '("omp version")'
    else 
        print '("serial version")'
    end if

    !$ ithread = omp_get_thread_num()
    print '("start on thread  ",i2)', ithread
    
    !$omp parallel private(ithread)
    !$ ithread = omp_get_thread_num()
    print '(" running on thread",i2)', ithread
    !$omp end parallel

    !$ ithread = omp_get_thread_num()
    print '("finish on thread ",i2)', ithread
end program hello_omp