program pardo2
    use omp_lib
    implicit none
    integer, parameter:: nmax = 9
    integer :: i
    real :: a(nmax), b(nmax)
!
    !write(*,*) 'Initialization'
    do i = 1, nmax
        a(i) = i
        b(i) = 0
        !print '(" a(",i2,") = ",e10.3)', i, a(i)
    enddo
!
    write(*,*) 'Calculation'
    !$omp parallel do
    do i = 2, nmax
        b(i) = 0.5*(a(i-1) + a(i))
        print '(" thread: ",i2," b(",i2,") = ",e10.3)', omp_get_thread_num(), i, b(i)
    enddo
!
    !write(*,*) 'Results'
    !do i = 2, nmax
    !    print '(" b(",i2,") = ",e10.3)', i, b(i)
    !enddo
end program pardo2
