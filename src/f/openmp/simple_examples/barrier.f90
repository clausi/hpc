!
! see http://mathworld.wolfram.com/FourierSeriesSquareWave.html
! for Fourier series of step function
!
program barrier
  use omp_lib
  implicit none
  integer, parameter :: NMAX = 100
  real*8, parameter :: PI = 3.141592653589793d0
  integer :: i, iam, nt, ipoints, istart
  real*8 :: a(0:NMAX-1) 
  real*8 :: psum = 0.0, x = 1.0

  !$omp parallel private(iam,nt,ipoints,istart,i)
    iam = omp_get_thread_num()
    nt =  omp_get_num_threads()
    ipoints = NMAX / nt
    istart = iam * ipoints
    if (iam .eq. nt-1) ipoints = NMAX - istart
    print '("iam ",I3," istart ",I3," ipoints ",I3)',iam,istart,ipoints
    
    do i = istart, istart + ipoints - 1
      a(i)=sin((2*i+1)*x)/(2.0*i+1);
    end do

    !$omp do schedule(dynamic) reduction(+:psum) 
    do i = 0, NMAX - 1
        psum = psum + a(i);
    end do
  !$omp end parallel

  print '("Result: ",e10.3)',4.0/PI*psum;
end program barrier

