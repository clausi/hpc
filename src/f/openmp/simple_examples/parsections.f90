module work
    use omp_lib
    implicit none
    contains
        subroutine work1
            print '(" work1 thread: ",i2)', omp_get_thread_num()
        end subroutine
        subroutine work2
            print '(" work2 thread: ",i2)', omp_get_thread_num()
        end subroutine
        subroutine work3
            print '(" work3 thread: ",i2)', omp_get_thread_num()
        end subroutine
end module work

program parsections
    use omp_lib
    use work
    implicit none
    !$omp parallel
      print '(" thread: ",i2)', omp_get_thread_num()
      !$omp barrier

      !$omp sections
        !$omp section
          call work1
        !$omp section
          call work2
        !$omp section
          call work3
      !$omp end sections
    !$omp end parallel

end program parsections
