real(8) :: a(1000), b(1000)!
!$omp parallel!
!$omp workshare!
!
A(:) = a(:) + b(:)!
!
!$omp end workshare[nowait]!
!$omp end parallel!