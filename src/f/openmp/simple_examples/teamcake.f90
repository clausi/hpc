program teamcake
    use omp_lib
    integer, parameter:: nmax = 11
    real*8 a(nmax), b(nmax)
!
    write(*,*) 'Initialization'
    do i=1,NMAX
      a(I)=I
      b(I)=0
      write(*,*) i,a(i)
    enddo
!
    write(*,*) 'Calculation'

      !$omp parallel private(iam,nt,ipoints,istart,i) 
      iam = omp_get_thread_num()
      nt =  omp_get_num_threads()
      ipoints = nmax / nt
      istart = iam * ipoints
      if (iam .eq. nt-1) then
        ipoints = nmax-istart
      endif
!     avoid referencing a(0)
      if (iam .EQ. 0) then
        istart = 1
        ipoints=ipoints-1
      endif
      DO i=istart+1, istart+ipoints
        b(i) = 0.5*(a(i-1) + a(i))
        write(*,*) iam,i,b(i)
      ENDDO
      !$omp end parallel
!
      write(*,*) 'Results'
      do i=2,nmax
        write(*,*) i,b(i)
      ENDDO
end program teamcake
