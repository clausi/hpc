program dyndo1
    use omp_lib
    implicit none
    integer, parameter:: nmax = 9
    integer :: i
    real :: a(nmax), b(nmax)
!
    write(*,*) 'Initialization'
    do i = 1, nmax
        a(i) = sin(i*6.28/nmax)
        b(i) = 0
        print '(" a(",i2,") = ",e10.3)', i, a(i)
    enddo
!
    write(*,*) 'Calculation'
    i = 1
    !$omp parallel
      !$omp single
        do while (a(i) .ge. 0)
            !$omp task
              b(i) = a(i)**2
              print '(" thread: ",i2," b(",i2,") = ",e10.3)', omp_get_thread_num(), i, b(i)
            !$omp end task
            
            !$omp taskwait
            print '(" increment on thread: ",i2)', omp_get_thread_num()
            i = i + 1
        enddo
      !$omp end single
    !$omp end parallel
!
    write(*,*) 'Results'
    do i = 1, nmax
        print '(" b(",i2,") = ",e10.3)', i, b(i)
    enddo
end program dyndo1
