program stream2
  use omp_lib
  implicit none
  integer*8, parameter :: GIGABYTE_DOUBLE = 134217728
  integer*8, parameter :: NMAX= 1*GIGABYTE_DOUBLE
  integer*8 :: i
  integer :: nt, stat
  real*8 :: scal = 0.5, secs = 0.0, bw = NMAX*8*3.0
  real*8, dimension(:), allocatable :: a,b,c

  allocate ( a(NMAX), STAT = stat)
  allocate ( b(NMAX), STAT = stat)
  allocate ( c(NMAX), STAT = stat)

  !$omp parallel
  
    !$omp master
      nt =  omp_get_num_threads();
      print '("running on ",i3," threads")', nt
    !$omp end master
    
    !$omp master
    print '("initializing ..")'
    !$omp end master

    !$omp do
    do i = 1, NMAX
      a(i) = 1.0
      b(i) = 2.0
      c(i) = 0.0
    end do

    !$omp master
    secs=omp_get_wtime();     
    !$omp end master

    !$omp do
    do i = 1, NMAX
      c(i) = a(i) + scal * b(i);
    end do

    !$omp master
    secs = omp_get_wtime() - secs;
    !$omp end master

  !$omp end parallel
  
  print '("time: ",e10.3," bandwidth: ",e10.3)', secs, bw/secs;
  deallocate(a, STAT = stat)
  deallocate(b, STAT = stat)
  deallocate(c, STAT = stat)

end program
