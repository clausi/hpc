!
! Main program for mandelbrot set. 
!
program mainmandel
    use precision
    use timer_c
    use ppm_c
    use mandel
    !$ use omp_lib
    implicit none
    ! image dimensions
    integer, parameter:: nx = 1024, ny = 1024 
    logical :: use_openmp = .false.

    real(kind=sp) :: flops
    real(kind=dp) :: secs
    real(kind=vp) :: x_center, y_center, delta, x1, x2, y1, y2
    integer :: maxiter, nmax_threads = 1
    integer, pointer :: img

    ! when compiled with -fopenmp, set flag and number of threads
    !$ use_openmp = .true.
    !$ nmax_threads = omp_get_max_threads()

    ! read input data
    !write(*,*) 'enter x_center,y_center,delta,maxiter:'
    read(*,*) x_center, y_center, delta, maxiter
    !write(*,*) 'values:',x_center,y_center,delta,maxiter

    ! image coordinates
    x1 = x_center - 0.5*delta
    x2 = x_center + 0.5*delta
    y1 = y_center - 0.5*delta
    y2 = y_center + 0.5*delta

    ! initialize image
    call makeimage(img, nx, ny);

    !use openmp or own timing routines
    if (use_openmp) then
        !$ secs = omp_get_wtime()
    else
        call start_timer()
    end if
    ! main computation
    flops = mandelbrot(img,nx,ny,maxiter,x1,x2,y1,y2,1.0e-2)
    if (use_openmp) then
        !$ secs = omp_get_wtime() - secs
    else
        secs = stop_timer()
    end if
    
    ! save & print
    call saveimage(img,"mandel.ppm");
    print '("threads = ",i3," secs = ",e10.3, " Flops = ", e10.3)', nmax_threads, secs, flops/secs
end program mainmandel