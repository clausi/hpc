#!/bin/bash
for i in 1 8 16 32 64 128 256 ;
do
  export OMP_NUM_THREADS=$i
  ./mandel2 < in.txt
done
