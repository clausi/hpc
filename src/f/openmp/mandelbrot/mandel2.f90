module mandel
  use precision
  use functions
  implicit none
  contains
    !
    ! standard mandelbrot implementation with complex variables
    !
    function mandelbrot(img, nx, ny, maxiters, x1, x2, y1, y2, enhance) result(flops)
      use ppm_c
      implicit none
      integer, pointer :: img
      integer :: nx, ny, maxiters
      real(kind=vp) :: x1, x2, y1, y2
      real(kind=sp) :: enhance
      real(kind=sp) :: flops
      
      integer :: i, j, ncnt
      ! image hue, saturation and value
      real(kind=sp) :: hue, sat = 1.0, val 
      real(kind=vp) :: dx, dy
      complex(kind=vp) :: c, z

      flops = 0
      if (enhance .gt. 0.0) then
        functions_eps = enhance
      end if
      
      dx = (x2-x1)/nx
      dy = (y2-y1)/ny
      !$omp parallel do private(c,z,ncnt,hue,val) reduction(+:flops) collapse(2) schedule(dynamic)
      do j = 0, ny-1 
        do i = 0, nx-1
            c = cmplx(x1 + dx * i, y1 + dy * j, kind=vp)
            z = cmplx(0, 0, kind=vp)
            ncnt = 0
            do while ((ncnt < maxiters) .and. (real(z)**2+aimag(z)**2 < 4.0))
                z = z*z + c
                ncnt = ncnt + 1
            enddo 
            ! hue is linear or enhanced by logit function
            hue = real(ncnt, kind=sp)/maxiters
            if (enhance .gt. 0.0) then
              hue = real(scaled_logit(real(hue,kind=vp)) * 240.0, kind=sp)
            else
              hue = hue * 240
            endif
            ! all within set are black (val = 0)
            if (ncnt == maxiters) then
              val = 0.0
            else
              val = 1.0
            end if
            call setpixel (img, i, j, hue, sat, val)
            flops = flops + 4 + ncnt*8
        enddo
      enddo
    end function mandelbrot
end module mandel
