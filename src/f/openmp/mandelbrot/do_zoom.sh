#!/bin/bash
rm *.ppm
let count=1
while [ $count -lt 48 ]
do
  echo $count
  echo "-0.7746806106269039d0" > in.txt
  echo "-0.1374168856037867d0" >> in.txt
  val=$(awk "BEGIN {printf \"%12.10e\n\",0.1*10^(-0.25*$count)}")
  echo $val >> in.txt
  echo 4096 >> in.txt
  ./mandel2 < in.txt
  foo=$(printf "mandel%02d.ppm" $count)
  mv mandel.ppm "$foo"
  let count+=1
done
echo "run convert -resize 50% -delay 20 -loop 1 *.ppm zoom.gif"
