//----------------------------------------------------------------------------
// Example code for solution of Laplace equation on a square
// (x,y)=([0,1],[0,1]). The Dirichlet boundary conditions
// are: u(x,y)=1 on the boundaries. This gives the trivial solution
// u(x,y)=1 on the square
//
// Serial version
//
//----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#define NMAX 258             // number of grid point
#define ITERMAX 50            // max. number of iterations

double u[NMAX][NMAX][NMAX];         // discrete values on a grid
double ou[NMAX][NMAX][NMAX];

// provide an initial guess for iteration

void initialize_guess() {
  int i,j,k;

  for (i=0;i<NMAX;i++) {         // boundary conditions
    for (j=0;j<NMAX;j++) {   
      u[0][i][j]     =  1.0;
      u[i][0][j]     =  1.0;
      u[i][j][0]     =  1.0;
      u[NMAX-1][i][j]     =  1.0;
      u[i][NMAX-1][j]     =  1.0;
      u[i][j][NMAX-1]     =  1.0;
    }
  }

  for (i=1;i<NMAX-1;i++)         // inner points are 0
    for (j=1;j<NMAX-1;j++)
      for (k=1;k<NMAX-1;k++)
        u[i][j][k]=0.0;

  for (i=0;i<NMAX;i++)         // inner points are 0
    for (j=0;j<NMAX;j++)
      for (k=0;k<NMAX;k++)
        ou[i][j][k]=u[i][j][k];
}

// print max absolute deviation of solution on the grid

double print_maxerr_grid() {
  double err=0.0,derr;
  int i,j,k,ierr=-1,jerr=-1,kerr=-1;

  for (i=0;i<NMAX;i++) {
    for (j=0;j<NMAX;j++) {
      for (k=0;k<NMAX;k++) {
        derr=fabs(u[i][j][k]-1.0);
        if (derr > err) {
          err=derr;ierr=i;jerr=j;kerr=k;  // error and grid point
        }
      }
    }
  }
  printf("maximum error on grid: (%d,%d,%d) %e\n",ierr,jerr,kerr,err);
  return err;
}

// update inner grid points

void update_grid() {
  int i,j,k;

  for (i=1;i<NMAX-1;i++) 
    for (j=1;j<NMAX-1;j++) 
      for (k=1;k<NMAX-1;k++) 
        u[i][j][k]=(ou[i-1][j][k]+ou[i+1][j][k]+
                    ou[i][j-1][k]+ou[i][j+1][k]+
                    ou[i][j][k-1]+ou[i][j][k+1])/6.0;

  for (i=1;i<NMAX-1;i++) 
    for (j=1;j<NMAX-1;j++) 
      for (k=1;k<NMAX-1;k++) 
        ou[i][j][k]=u[i][j][k];
}

// main program

int main() {
  int n;

  initialize_guess();          // initialize matrix
  initialize_guess(); 

  n=0;
  while (n < ITERMAX) {         // relaxation
   update_grid();
   n++;
  }

  print_maxerr_grid();         // compare with exact solution
  return 0;
}
