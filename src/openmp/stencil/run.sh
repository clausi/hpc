for iblock in 1 3 6 12 24 48 96;
do

for jblock in 1 3 6 12 24 48 96;
do

for kblock in 1 3 6 12 24 48 96;
do

for i in 12;
do
  gcc -O3 -fopenmp -funroll-loops stencil3domp.c -o stencil3domp -DIBLOCK=$iblock -DJBLOCK=$jblock -DKBLOCK=$kblock
  export OMP_NUM_THREADS=$i
  ./stencil3domp
done
done
done
done
