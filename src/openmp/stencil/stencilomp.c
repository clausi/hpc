//----------------------------------------------------------------------------
// Example code for solution of Laplace equation on a square
// (x,y)=([0,1],[0,1]). The Dirichlet boundary conditions
// are: u(x,y)=1 on the boundaries. This gives the trivial solution
// u(x,y)=1 on the square
//
// Omp version
//
//----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <omp.h>

#define NMAX 514             // number of grid point
//#define IBLOCK 64 
//#define JBLOCK 64 
#define ITERMAX 1000          // max. number of iterations

double u[NMAX][NMAX];         // discrete values on a grid
double ou[NMAX][NMAX];

// provide an initial guess for iteration

void initialize_guess() {
  int i,i1,j,j1;

  #pragma omp parallel for collapse(2)
  for (i=1;i<NMAX-1;i+=IBLOCK) {
    for (j=1;j<NMAX-1;j+=JBLOCK) {
      for (i1=0;i1<IBLOCK;i1++) {
        for (j1=0;j1<JBLOCK;j1++) {
          u[i+i1][j+j1]=0.0;
          ou[i+i1][j+j1]=0.0;
        }
      }
    }
  }

  #pragma omp parallel for
  for (i=0;i<NMAX;i++) {         // boundary conditions
    ou[i][0]      = u[i][0]  =     1.0;
    ou[i][NMAX-1] = u[i][NMAX-1]=  1.0;
    ou[0][i]      = u[0][i]     =  1.0;
    ou[NMAX-1][i] = u[NMAX-1][i]=  1.0;
  }

}

// ----------------------------------------------------
// print max absolute deviation of solution on the grid
// ----------------------------------------------------

double print_maxerr_grid() {
  double err=0.0,derr;
  int i,j,ierr=-1,jerr=-1;

  for (i=0;i<NMAX;i++) {
    for (j=0;j<NMAX;j++) {
      derr=fabs(u[i][j]-1.0);
      if (derr > err) {
        err=derr;ierr=i;jerr=j;  // error and grid point
      }
    }
  }
  printf("maximum error on grid: (%d,%d) %e\n",ierr,jerr,err);
  return err;
}

// update inner grid points

int update_grid() {
  int i,i1,j,j1,n=0,nt,np;

#pragma omp parallel private(i1,j,j1,n,nt)
  {
    #pragma omp master
    np=omp_get_num_threads();

    for (n=0; n < ITERMAX; n++) { 
      #pragma omp for collapse(2) schedule(guided)
      for (i=1;i<NMAX-1;i+=IBLOCK) {
        for (j=1;j<NMAX-1;j+=JBLOCK) {
          for (i1=0;i1<IBLOCK;i1++) {
            for (j1=0;j1<JBLOCK;j1++) {
              u[i+i1][j+j1]=0.25*(ou[i+i1-1][j+j1]+ou[i+i1+1][j+j1]
                             +ou[i+i1][j+j1-1]+ou[i+i1][j+j1+1]);
            }
          }
        }
      }
      #pragma omp for collapse(2)
      for (i=1;i<NMAX-1;i++) {
        for (j=1;j<NMAX-1;j++) {
          ou[i][j]=u[i][j];
        }
      }
    }
  }
  return np;
}

// main program

int main() {
  int n,np;
  double secs;

  initialize_guess();          // initialize matrix
  secs=omp_get_wtime();
  np=update_grid();
  secs=omp_get_wtime()-secs;
  printf("%d %10.3e %d %d \n",np,secs,IBLOCK,JBLOCK);

  //print_maxerr_grid();         // compare with exact solution
  return 0;
}
