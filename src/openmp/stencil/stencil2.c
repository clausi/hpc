//----------------------------------------------------------------------------
// Example code for solution of Laplace equation on a square
// (x,y)=([0,1],[0,1]). The Dirichlet boundary conditions
// are: u(x,y)=1 on the boundaries. This gives the trivial solution
// u(x,y)=1 on the square
//
// Serial version
//
//----------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#define NMAX 2050            // number of grid point
#define ITERMAX 1000         // max. number of iterations

double u[NMAX][NMAX];         // discrete values on a grid
double ou[NMAX][NMAX];


// provide an initial guess for iteration

void initialize_guess() {
  int i,j;

  #pragma omp parallel for private(j)
  for (i=1;i<NMAX-1;i++)         // inner points are 0
    for (j=1;j<NMAX-1;j++) {
      u[i][j]=0.0;
      ou[i][j]=0.0;
    }

  for (i=0;i<NMAX;i++) {         // boundary conditions
    ou[i][0]      = u[i][0]  =  1.0;
    ou[i][NMAX-1] = u[i][NMAX-1]=  1.0;
    ou[0][i]      = u[0][i]     =  1.0;
    ou[NMAX-1][i] = u[NMAX-1][i]=  1.0;
  }
}

// print max absolute deviation of solution on the grid

double print_maxerr_grid() {
  double err=0.0,derr;
  int i,j,ierr=-1,jerr=-1;

  for (i=0;i<NMAX;i++) {
    for (j=0;j<NMAX;j++) {
      derr=fabs(u[i][j]-1.0);
      if (derr > err) {
        err=derr;ierr=i;jerr=j;  // error and grid point
      }
    }
  }
  printf("maximum error on grid: (%d,%d) %e\n",ierr,jerr,err);
  return err;
}

// update inner grid points

void update_grid() {
  int i,j,n;
  #pragma omp parallel private(n)
  {
  n=0;
  while (n < ITERMAX) {
    #pragma omp for private(j)
    for (i=1;i<NMAX-1;i++) {
      for (j=1;j<NMAX-1;j++) 
        u[i][j]=0.25*(ou[i-1][j]+ou[i+1][j]+ou[i][j-1]+ou[i][j+1]);
    }

    #pragma omp for private(j)
    for (i=1;i<NMAX-1;i++) {
      for (j=1;j<NMAX-1;j++) 
        ou[i][j]=u[i][j];
    }
    n++;
  }
  }
}

// main program

int main() {
  int n;

  initialize_guess(); 

  update_grid();

  print_maxerr_grid(); 
  return 0;
}
