#include <stdio.h>
#include <math.h>
#include <omp.h>
#define NMAX 98                // block sizes 48, 24,12,6,3
#define ITERMAX 200           // max. number of iterations

double u[NMAX][NMAX][NMAX];   // discrete values on a grid
double ou[NMAX][NMAX][NMAX];

// provide an initial guess for iteration

void initialize_guess() {
  int i,j,k;

#pragma omp parallel for private(j,k)
  for (i=1;i<NMAX-1;i++) {        // inner points are 0
    for (j=1;j<NMAX-1;j++) {
      for (k=1;k<NMAX-1;k++) {
        u[i][j][k]=0.0;
        ou[i][j][k]=0.0;
      }
    }
  }
  
#pragma omp parallel for private(j)
  for (i=0;i<NMAX;i++) {         // u = 1 on surface
    for (j=0;j<NMAX;j++) {   
      ou[0][i][j] = u[0][i][j] = 1.0;
      ou[i][0][j] = u[i][0][j] = 1.0;
      ou[i][j][0] = u[i][j][0] = 1.0;
      ou[NMAX-1][i][j] = u[NMAX-1][i][j] = 1.0;
      ou[i][NMAX-1][j] = u[i][NMAX-1][j] = 1.0;
      ou[i][j][NMAX-1] = u[i][j][NMAX-1] = 1.0;
    }
  }
}

// print max absolute deviation of solution on the grid

double print_maxerr_grid() {
  double err=0.0,derr;
  int i,j,k,ierr=-1,jerr=-1,kerr=-1;
  for (i=0;i<NMAX;i++) {
    for (j=0;j<NMAX;j++) {
      for (k=0;k<NMAX;k++) {
        derr=fabs(u[i][j][k]-1.0);
        if (derr > err) {
          err=derr;ierr=i;jerr=j;kerr=k;  // error and grid point
        }
      }
    }
  }
  printf("maximum error on grid: (%d,%d,%d) %e\n",ierr,jerr,kerr,err);
  return err;
}

// update inner grid points

int update_grid() {
  int i,j,k,i1,j1,k1,n,np;
  
#pragma omp parallel private(n)
{
#pragma omp master
  np = omp_get_num_threads();
  for (n=0;n<ITERMAX;n++) {
#pragma omp for private(j,k)
  for (i=1;i<NMAX-1;i++) {
    for (j=1;j<NMAX-1;j++) {
      for (k=1;k<NMAX-1;k++) {
        u[i][j][k]=(ou[i-1][j][k]+ou[i+1][j][k]+
                    ou[i][j-1][k]+ou[i][j+1][k]+
                    ou[i][j][k-1]+ou[i][j][k+1])/6.0;
      }
    }
  }

#pragma omp for private(j,k)
  for (i=1;i<NMAX-1;i++) 
    for (j=1;j<NMAX-1;j++) 
      for (k=1;k<NMAX-1;k++) 
        ou[i][j][k]=u[i][j][k];
  }
}
  return np;
}

// main program

int main() {
  int np;
  double secs;

  initialize_guess();          // initialize matrix
  secs = omp_get_wtime();
  np=update_grid();
  secs = omp_get_wtime()-secs;
  printf("%d %10.3e\n",np,secs);
  //print_maxerr_grid();         // compare with exact solution
  return 0;
}
