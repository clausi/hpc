### Exercise Mandelbrot

* Make and run. Use "display" to show the image. Write down wall time
* Change OPTFLAGS in Makefile to include some more agressive optimizations. How
  much faster does it get?
* Create a copy of mandelbrot.c as mandelomp.c. Add instructions
  to Makefile so this program gets compiled
* In mandelomp.c, substitute calls to start_timer() and stop_timer() by
  calls to omp_get_wtime(). Compile and run (export OMP_NUM_THREADS=1)
* Parallelize mandelomp.c with a simple "parallel for". Run with 1, 2 and 4 
  threads. Is the image the same?
* Measure the scaling behaviour. (use run.sh)
* Modify the code in order to have a parallel section and a for worksharing
  construct. Is the image fine? Include a statement that tells us on how
  many threads we are running. 
