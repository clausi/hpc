#include <stdio.h>
#include <omp.h>

#define NMAX 8
double a[NMAX],b[NMAX];

void calc(double *v1, double *v2, int is, int n) {
  int i;
  if (is ==0) {
    is=1;
    n--;
  }
  for (i=is;i<is+n;i++) {
    v2[i]=0.5*(v1[i-1]+v1[i]);
  }
}

int main() {
  int i,iam,nt,ipoints,istart;

#pragma omp parallel private(iam,nt,ipoints,istart)
  {
    iam = omp_get_thread_num();
    nt =  omp_get_num_threads();
#pragma omp for
    for (i=0;i<NMAX;i++) {
      a[i]=i;
      b[i]=0;
    }
    iam = omp_get_thread_num();
    nt =  omp_get_num_threads();
    ipoints = NMAX / nt;    /* size of partition */
    istart = iam * ipoints;  /* starting array index */
    if (iam == nt-1)     /* last thread may do more */
      ipoints = NMAX - istart;
    printf("thread %d of %d: %d %d\n",iam,nt,istart,ipoints);
    calc(a,b, istart, ipoints);
#pragma omp for
    for (i=1;i<NMAX;i++) {
      printf("%02d %10.3e\n",i,b[i]);
    }
  }
  return 0;
}
