#include <stdio.h>

int main() {
  int i;
  double sum=0;
#pragma omp parallel for reduction(+:sum)
  for (i=0; i<1000; i++) { 
    sum = sum + 1;
  }
  printf("sum %10.3e\n",sum);
  return 0;
}
