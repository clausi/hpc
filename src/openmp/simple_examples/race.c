#include <stdio.h>

int main() {
  int i;
  double sum=0;
#pragma omp parallel for
  for (i=0; i<1000; i++) { 
    sum = sum + 1;
  }
  printf("sum %10.3e\n",sum);
  return 0;
}
