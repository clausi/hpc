#include <stdio.h>
#include <omp.h>

//note: try adding num_threads(2) to omp pragma

int main() {
  int iam,nt,nsh;
  
  printf("max threads: %2d\n",omp_get_max_threads());
  printf("num threads: %2d\n",omp_get_num_threads());

#pragma omp parallel private(iam,nt) shared(nsh) default(none)
  {
    iam = omp_get_thread_num();
    nt = omp_get_num_threads();
    if (iam==0) {
      printf("parallel - num threads: %2d\n",nt);
      nsh = nt;
    }
  }
  
  printf("%2d threads used\n",nsh);

  return 0;
}
