#include <stdio.h>
#include <omp.h>

int main() {
#pragma omp parallel 
  {
#pragma omp master
    printf("Running with %d threads\n",omp_get_num_threads()); 
    printf("Hello from thread %d!\n",omp_get_thread_num()); 
  }
  return 0;
}
