#include <stdio.h>
#include <omp.h>

#define NMAX 11
double a[NMAX],b[NMAX];

void calc(double *v1, double *v2, int is, int n) {
  int i;
  if (is ==0) {
    is=1;
    n--;
  }
  for (i=is;i<is+n;i++) {
    v2[i]=0.5*(v1[i-1]+v1[i]);
  }
}

int main() {
  int i,iam,nt,ipoints,istart;

  for (i=0;i<NMAX;i++) {
    a[i]=i;
    b[i]=0;
    printf("%02d %10.3e\n",i,a[i]);
  }

#pragma omp parallel private(iam,nt,ipoints,istart)
  {
    iam = omp_get_thread_num();
    nt =  omp_get_num_threads();
    ipoints = NMAX / nt;     /* cake slice size */
    istart = iam * ipoints;  /* where it starts */
    if (iam == nt-1)         /* last gets leftovers */
      ipoints = NMAX - istart;
    calc(a,b, istart, ipoints);
  }

  for (i=1;i<NMAX;i++) {
    printf("%02d %10.3e\n",i,b[i]);
  }
  return 0;
}
