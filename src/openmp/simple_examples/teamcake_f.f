      PROGRAM TEAMCAKE
!$    INCLUDE "omp_lib.h"
      PARAMETER (NMAX=11)
      REAL*8 a(NMAX), b(NMAX)
C
      write(*,*) 'Initialization'
      Do i=1,NMAX
        a(I)=I
        b(I)=0
        write(*,*) i,a(i)
      ENDDO
C
      write(*,*) 'Calculation'

!$OMP PARALLEL PRIVATE(iam,nt,ipoints,istart,i) 
      iam = omp_get_thread_num()
      nt =  omp_get_num_threads()
      ipoints = NMAX / nt
      istart = iam * ipoints
      if (iam .EQ. nt-1) then
        ipoints=NMAX-istart
      endif
C     avoid referencing a(0)
      if (iam .EQ. 0) then
        istart = 1
        ipoints=ipoints-1
      endif
      DO i=istart+1, istart+ipoints
        b(i) = 0.5*(a(i-1) + a(i))
        write(*,*) iam,i,b(i)
      ENDDO
!$OMP END PARALLEL
C
      write(*,*) 'Results'
      Do i=2,NMAX
        write(*,*) i,b(i)
      ENDDO
      END PROGRAM TEAMCAKE
