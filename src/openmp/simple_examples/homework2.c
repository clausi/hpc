#include <stdio.h>


int main() {
  int nt=0;

#pragma omp parallel reduction(+:nt)
  {
     nt=1;
  }

  printf("%2d threads\n",nt);
  return 0;
}
