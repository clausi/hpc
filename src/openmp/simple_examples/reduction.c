#include <stdio.h>
#include <omp.h>

#define NMAX 100
double a[NMAX];

int main() {
  int i,iam,nt,ipoints,istart;
  double psum=0,tsum=0;

  for (i=0;i<NMAX;i++) {
    a[i]=i+1;
  }

#pragma omp parallel private(iam,nt,ipoints,istart,psum)
  {
    iam = omp_get_thread_num();
    nt =  omp_get_num_threads();
    ipoints = NMAX / nt;     /* size of partition */
    istart = iam * ipoints;  /* starting array index */
    if (iam == nt-1)         /* last thread may do more */
      ipoints = NMAX - istart;
    for (i=istart;i < istart + ipoints;i++) {
      psum += a[i];
    }
#pragma omp critical
    tsum += psum;
  }
  printf("%10.3e\n",tsum);

  return 0;
}
