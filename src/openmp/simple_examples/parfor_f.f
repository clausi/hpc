      PROGRAM PARFOR
      PARAMETER (NMAX=5)
      REAL*8 a(NMAX), b(NMAX)
C
      write(*,*) 'Initialization'
      Do i=1,NMAX
        a(I)=I
        b(I)=0
        write(*,*) i,a(i)
      ENDDO
C
      write(*,*) 'Calculation'
!$OMP PARALLEL DO
      DO i=2,NMAX
        b(i) = 0.5*(a(i-1) + a(i))
        write(*,*) i,b(i)
      ENDDO
C
      write(*,*) 'Results'
      Do i=2,NMAX
        write(*,*) i,b(i)
      ENDDO
      END PROGRAM PARFOR
