#include <stdio.h>

#define NMAX 5
double a[NMAX],b[NMAX];

int main() {
  int i,n=NMAX;

  printf("initialization\n");
  // serial loop
  for (i=0; i<n; i++) {
    a[i] = i;
    b[i] = 0.0;
    printf("%d %10.3e\n",i,a[i]);
  }

  printf("calculation\n");
  // parallel loop
#pragma omp parallel for 
  for (i=1; i<n; i++) {
    b[i] = 0.5*(a[i-1] + a[i]);
    printf("%d %10.3e\n",i,b[i]);
  }

  printf("results\n");
  for (i=1; i<n; i++) {
    printf("%d %10.3e\n",i,b[i]);
  }
  return 0;
}
