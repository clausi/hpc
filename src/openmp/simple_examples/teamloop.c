#include <stdio.h>
#include <omp.h>

#define NMAX 101
double a[NMAX];


int main() {
  int i,iam;

  for (i=0;i<NMAX;i++) {
    a[i]=i;
  }

#pragma omp parallel private(iam)
  {
    iam = omp_get_thread_num();
    printf("iam %02d \n",iam);
  }

  return 0;
}
