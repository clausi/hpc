#include <stdio.h>
#include <math.h>
#include <omp.h>
/*
 * see http://mathworld.wolfram.com/FourierSeriesSquareWave.html
 * for Fourier series of step function
 */
#define NMAX 100
double a[NMAX];

int main() {
  int i,iam,nt,ipoints,istart;
  double psum=0,x=1.0;

#pragma omp parallel private(iam,nt,ipoints,istart,i)
  {
    iam = omp_get_thread_num();
    nt =  omp_get_num_threads();
    ipoints = NMAX / nt; 
    istart = iam * ipoints; 
    if (iam == nt-1) 
      ipoints = NMAX - istart;
    printf("iam %d istart %d ipoints %d\n",iam,istart,ipoints);

    for (i=istart;i < istart + ipoints;i++) {
      a[i]=sin((2*i+1)*x)/(2.0*i+1);
    }
//#pragma omp barrier
#pragma omp for schedule(dynamic) reduction(+:psum) 
    for (i=0; i < NMAX; i++) {
      psum += a[i];
    }
  }
  printf("%10.3e\n",4.0/M_PI*psum);

  return 0;
}
