#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mm_malloc.h>
#include "timing.h"
#include "sysinfo.h"

//minimal number of calculations to do
#define MIN_CALCS 1000000000
#define USE_DOUBLE_PRECISION 1

#ifdef USE_DOUBLE_PRECISION
  #define REAL double
#else
  #define REAL float
#endif

/*
 *   y = a*x + y
 */
void daxpy1(int n, REAL a, REAL *x, REAL *y) {
  int i;
#pragma omp parallel for
  for (i = 0; i < n; i++) {
    y[i] = a*x[i] + y[i];
  }
}
/*
 * main program
 */
int main(int argc, char *argv[] ) 
{
  REAL *x=NULL, *y=NULL;
  int i,n_iter;
  long n=1024;
  REAL a = 1.1;
  double secs = 0.0, gflops = 0.0;

  // read command line argument
  if (argc == 2) {
    if (strcmp(argv[1],"-h") == 0) {
       printf("W_Size Array_Length Iterations Seconds    Gflops    \n");
       return 0;
    }
    if (sscanf (argv[1], "%ld", &n)!=1) { 
      printf("usage: %s [nnn]. Note: %s -h prints header line.\n",
	     argv[0],argv[0]); 
      return -1;
    }
  }

  // adjust number of iterations 
  if (n < MIN_CALCS)
    n_iter = MIN_CALCS/n; 
  else
    n_iter = 1;

  // allocate memory
  if (2*n*sizeof(REAL) > get_freeram()) {
    printf("error: arrays will not fit in ram\n");
    return -2;
  }
  x = malloc(n*sizeof(REAL));
  y = malloc(n*sizeof(REAL));
  //printf("%p %p\n",x,y);
  if (x == NULL || y == NULL) {
    printf ("error allocating memory\n"); 
    return -3;
  }

  // initialize
#pragma omp parallel for
  for (i = 0; i < n; i++) {
      x[i] = i + 0.001;
      y[i] = 0.0;
  }

  // do calculations
  start_timer();
  for( i = 0; i < n_iter; i++) {
    daxpy1(n,a,x,y);
  }
  secs=stop_timer();;

  // print results  
  gflops = 2*n*n_iter*1.0e-9/secs;    
  printf("%6ld %12ld %10d %10f %10f\n",
         sizeof(REAL),n,n_iter,secs,gflops);
  return 0;
}

