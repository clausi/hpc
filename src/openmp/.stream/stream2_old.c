#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define GIGABYTE_DOUBLE 134217728L
#define NMAX 20*GIGABYTE_DOUBLE
#define NREPEAT 1

int main() {
  long int i;
  int n,nt;
  double scal = 0.5, secs = 0.0;
  double *a, *b, *c;

  a = malloc(NMAX*sizeof(double));
  b = malloc(NMAX*sizeof(double));
  c = malloc(NMAX*sizeof(double));

#pragma omp parallel private(n)
  {
#pragma omp master
    {
      nt =  omp_get_num_threads();
      printf("running on %d threads, repeating %d times\n",nt,NREPEAT);
    }

#pragma omp master
  printf("initializing ..\n");
#pragma omp for
  for (i=0;i<NMAX;i++) {
    a[i]=1.0;
    b[i]=2.0;
    c[i]=0.0;
  }

#pragma omp single
    secs=omp_get_wtime();     

    for (n=0;n<NREPEAT;n++) {
      #pragma omp for
      for (i=1;i<NMAX;i++) {
        c[i] = a[i] + scal * b[i];
      }
    }

#pragma omp single
    secs=omp_get_wtime()-secs;     

  }
  printf("time: %10.3e, last element: %10.3e\n",secs,c[NMAX-1]);
  free(a);
  free(b);
  free(c);
  return 0;
}
