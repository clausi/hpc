#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define GIGABYTE_DOUBLE 134217728L
// be carefull ...
#define NMAX 1*GIGABYTE_DOUBLE

int main() {
  long int i;
  int nt;
  double scal = 0.5, secs = 0.0, bw=NMAX*sizeof(double)*3.0;
  double *a, *b, *c;

  a = malloc(NMAX*sizeof(double));
  b = malloc(NMAX*sizeof(double));
  c = malloc(NMAX*sizeof(double));

  #pragma omp parallel
  {
    #pragma omp master
    {
      nt =  omp_get_num_threads();
      printf("running on %d threads\n",nt);
    }

    #pragma omp master
    printf("initializing ..\n");

    #pragma omp for
    for (i=0;i<NMAX;i++) {
      a[i]=1.0;
      b[i]=2.0;
      c[i]=0.0;
    }

    #pragma omp single
    secs=omp_get_wtime();     

    #pragma omp for
    for (i=1;i<NMAX;i++) {
      c[i] = a[i] + scal * b[i];
    }

    #pragma omp single
    secs=omp_get_wtime()-secs;     

  }
  printf("time: %10.3e, bandwidth: %10.3e\n",secs,bw/secs);
  free(a);
  free(b);
  free(c);
  return 0;
}
