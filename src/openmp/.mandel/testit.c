#include <math.h>
#include <stdio.h>
#include <complex.h>
#include <omp.h>

#define ImageHeight 1024
#define ImageWidth 1024
#define LIMIT 4096
#define SIMD_LEN 16

#pragma omp declare simd simdlen(SIMD_LEN)
int mandel(float cr, float ci)
{  
  float complex c = cr + I*ci;
  float complex z = c;
  int iters=0;
  for (iters=0; (cabsf(z) < 2.0f) && (iters < LIMIT); iters++) {
    z = z * z + c;
  }
  return iters;
}

float complex in_vals[ImageHeight][ImageWidth];
int count[ImageHeight][ImageWidth];

int main() {

  float x1=-2.0,x2=1.0,y1=-1.5,y2=1.5;
  float dx = (x2-x1)/ImageWidth, dy = (y2-y1)/ImageHeight;

  for (int y = 0;  y < ImageHeight; ++y) {
    for (int x =  0; x < ImageWidth; ++x) {
      in_vals[y][x]=x1+dx*x + I*(y1+dy*y);
    }
  }
#ifdef _OPENMP
  double t1=omp_get_wtime();
#endif
  #pragma omp parallel for
  for (int y = 0;  y < ImageHeight; ++y) {
    #pragma omp simd
    for (int x =  0; x < ImageWidth; ++x) {
       count[y][x] = mandel(creal(in_vals[y][x]),cimag(in_vals[y][x]));
    }
  }
#ifdef _OPENMP
  t1=omp_get_wtime()-t1;
  printf("%10.3e\n",t1);
#endif
  printf("%d\n",count[ImageHeight/2][ImageWidth/2]);
}
