#include "../../utilities/ppm.h"

#define REAL float
#define SIMD_LEN 32

double mandelbrot(IMAGE *img, int maxiters,
                  REAL x1, REAL x2, REAL y1, REAL y2);
