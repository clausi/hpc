#include "colorpixel.h"

//#pragma omp declare simd uniform(img,j,maxiters) linear(i) simdlen(SIMD_LEN)
#pragma omp declare simd uniform(img,maxiters)
void color_pixel(IMAGE *img, int i, int j, int niters, int maxiters) {
  img->data_rh[j*img->nx+i]=240.0-(niters*360.0)/maxiters;
  img->data_gs[j*img->nx+i]=1.0;
  img->data_bv[j*img->nx+i] = (niters == maxiters) ? 0.0 : 1.0;
}
