#include "../../utilities/ppm.h"
#include "mandel.h"

//#pragma omp declare simd uniform(img,j,maxiters) linear(i) simdlen(SIMD_LEN)
#pragma omp declare simd uniform(img,maxiters) 
void color_pixel(IMAGE *img, int i, int j, int niters, int maxiters);
