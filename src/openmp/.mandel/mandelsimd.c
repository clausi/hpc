#include <complex.h>
#include <stdio.h>
#include <omp.h>
#include "mandel.h"
#include "colorpixel.h"

/**
 * mandelbrot with simd/omp
 */
//#pragma omp declare simd uniform(nmax) simdlen(SIMD_LEN)
#pragma omp declare simd uniform(nmax)
int iterate(REAL cr, REAL ci, int nmax) {
  REAL zr=cr;
  REAL zi=ci;
  REAL tr2=zr*zr;
  REAL ti2=zi*zi;
  int count = 0;
  while ((++count < nmax) && (tr2+ti2 < 4.0)) {
	 zi = 2*zr*zi + ci;
         zr = tr2-ti2 + cr;
         tr2=zr*zr;
         ti2=zi*zi;
  }
  return count;
}
#pragma omp declare simd uniform(nmax)
int iteratec(REAL cr, REAL ci, int nmax) {
  REAL complex c=cr+I*ci;
  REAL complex z=c;
  int count = 0;
  while ((++count < nmax) && (creal(z)*creal(z)+cimag(z)*cimag(z) < 4.0)) {
    z = z*z +c;
  }
  return count;
}

double mandelbrot(IMAGE *img, int maxiters,
                  REAL x1, REAL x2, REAL y1, REAL y2) {
  REAL dx = (x2-x1)/img->nx, dy = (y2-y1)/img->ny;
  double flops=0;
#pragma omp parallel
  {
    #pragma omp master
    printf("running on %d threads\n",omp_get_num_threads());
#pragma omp for schedule(dynamic) reduction(+:flops)
    for (int j = 0; j < img->ny; ++j) {
#pragma omp simd safelen(SIMD_LEN) reduction(+:flops)
      for (int i = 0; i < img->nx; ++i) {
        REAL ci=y1+dy*j;
        REAL cr = x1+dx*i;
        int count = iterate(cr,ci,maxiters);
        color_pixel(img,i,j,count,maxiters);
        flops += 4+count*8;
      }
    }
  }
  img->cspace=TYPE_HSV;
  return flops;
}
