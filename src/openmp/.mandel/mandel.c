#include <stdio.h>
#include "mandel.h"
#include "../../utilities/timing.h"

/**
 * Calculate the mandelbrot set
 * standard: flops=mandelbrotX(img,4096,-2.0,1.0,1.5,-1.5);
 */
int main(void) {
  int i,j;
  double secs,flops,val;
  IMAGE *img=makeimage(2*1024,2*1024);
  start_timer();
  flops=mandelbrot(img,4096,-1.2,-0.7,0.5,0.0);
  secs=stop_timer();
  printf("%10.3e %10.3e %10.3e\n",
          secs,flops,1.0e-9*flops/secs); 
  saveimage(img,"mandel.ppm");
}
