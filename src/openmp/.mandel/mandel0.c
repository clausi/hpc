#include <complex.h>
#include "mandel.h"
#include "colorpixel.h"

int iterate(REAL cr, REAL ci, int nmax) {
  REAL zr=cr;
  REAL zi=ci;
  REAL tr2=zr*zr;
  REAL ti2=zi*zi;
  int count = 0;
  while ((++count < nmax) && (tr2+ti2 < 4.0)) {
         zi = 2*zr*zi + ci;
         zr = tr2-ti2 + cr;
         tr2=zr*zr;
         ti2=zi*zi;
  }
  return count;
}

/**
 * standard mandelbrot implementation
 */
double mandelbrot(IMAGE *img, int maxiters, 
                  REAL x1, REAL x2, REAL y1, REAL y2) { 
  int i,j,count;
  REAL dx = (x2-x1)/img->nx, dy = (y2-y1)/img->ny;
  double flops=0;
  REAL complex c,z;
  for (j = 0; j < img->ny; ++j) {
    for (i = 0; i < img->nx; ++i) {
      count = iterate(x1+dx*i,y1+dy*j,maxiters);
/*
      c = x1+dx*i + (y1+dy*j) * I;
      z = c;
      count = 0;
      while ((++count < maxiters) && 
         (creal(z)*creal(z)+cimag(z)*cimag(z) < 4.0)) {
          z = z*z+c;
      }
*/
      color_pixel(img,i,j,count,maxiters);
      flops += 4+count*8;
    }
  }
  img->cspace=TYPE_HSV;
  return flops;
}
