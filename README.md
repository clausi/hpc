# HPC course

## Repository for some content from my HPC course

Directories:
------------
* src - source code of example programs
* notes - class notes
* bin - handy shell scripts etc

## Contact me ###

* Claus Denk - obtain mail at [SISIUS](https://investigacion.us.es/sisius/sis_showpub.php?idpers=3552)
